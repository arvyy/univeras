CREATE TABLE User (
	id integer primary key,
	reputation integer,
	createDate date,
	name varchar(50),
	accessDate date,
	views integer,
	upvotes integer,
	downvotes integer
);

CREATE TABLE Badge (
	id integer primary key autoincrement,
	name varchar(50) unique,
	tagbased boolean
);

CREATE TABLE Tag (
	id integer primary key,
	name varchar(50) unique,
	count integer
);

CREATE TABLE BadgeAcquisition (
	id integer primary key,
	acquireDate date
);

CREATE TABLE Post(
	id integer primary key, 
	type varchar(1), 
	createdate date, 
	score integer, 
	body varchar(2000), 
	title varchar(100), 
	viewcount integer);

CREATE TABLE Comment(
	id integer primary key,
	text varchar(200),
	score integer,
	createdate date
);
