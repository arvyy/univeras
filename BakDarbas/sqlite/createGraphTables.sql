drop table if exists Nodes;
drop table if exists Edges;

create table Nodes (
	id integer primary key autoincrement,
	type char(20) not null
);

create table Edges (
    id integer primary key autoincrement,
    node_from int,
    node_to int,
    type char(20) not null
);

create index node_from_index on Edges(node_from);
create index node_to_index on Edges(node_to);
