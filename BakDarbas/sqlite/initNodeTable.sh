#!/bin/bash

echo "
ALTER TABLE $1
    ADD gsql_node_id int;

DROP TRIGGER IF EXISTS create_node_for_$1 ;
CREATE TRIGGER create_node_for_$1
AFTER INSERT ON $1
FOR EACH ROW
BEGIN
    INSERT INTO nodes (id,type) VALUES (NULL, '$1');
    UPDATE $1
        SET gsql_node_id = (select last_insert_rowid())
        WHERE NEW.id = id;
END;

DROP TRIGGER IF EXISTS delete_node_for_$1 ;
CREATE TRIGGER delete_node_for_$1
AFTER DELETE ON $1
FOR EACH ROW
BEGIN
	DELETE FROM nodes
	WHERE OLD.gsql_node_id = nodes.id;
	DELETE FROM edges
	WHERE node_from = OLD.gsql_nodes_id OR node_to = OLD.gsql_nodes_id;
END;
"
