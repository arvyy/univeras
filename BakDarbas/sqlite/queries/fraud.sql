select user1.name, user2.name, count(*)
matching
	(User:user1)-['ask']->(Post:)-['answer_to']->(Post:)<-['ask']-(User:user2)
group by user1.name, user2.name having count(*) > 5
order by count(*) desc;
