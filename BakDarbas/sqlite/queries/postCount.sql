select u.name, t.name, count(*)
matching graph(nodes2, edges2)
	(User:u)-['BadgeAcquisition']->(Badge:)-['BadgeTag']->(Tag:t),
	(User:u)-['ask']->(Post:)-['tagged']->(Tag:t)
group by u.name, t.name
order by count(*) desc;
