select p.title, count(*)
matching (Post:p)-['similar']->(Post:p2)
group by p.title
order by count(*) asc;
