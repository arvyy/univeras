#/bin/bash

ds=../datasets/aviation.stackexchange.com

# delete old
rm -f ./SO.db
rm -rf ./inserts

# create graph tables
echo 'Create graph tables'
sqlite3 ./SO.db < ./createGraphTables.sql

# create SO tables
echo 'Create SO tables'
sqlite3 ./SO.db < ./initSOTables.sql
bash initNodeTable.sh User | sqlite3 ./SO.db
bash initNodeTable.sh Badge | sqlite3 ./SO.db
bash initNodeTable.sh Tag | sqlite3 ./SO.db
bash initNodeTable.sh Comment | sqlite3 ./SO.db
bash initNodeTable.sh Post | sqlite3 ./SO.db
bash initEdgeTable.sh BadgeAcquisition | sqlite3 ./SO.db

# load data (nodes)
mkdir inserts
mkdir inserts/nodes
mkdir inserts/edges

echo 'Load data (nodes)'
echo '  users'
guile -s ../guile/sodata/users.scm $ds/Users.xml >> ./inserts/nodes/users.sql
echo '  posts'
guile -s ../guile/sodata/posts.scm $ds/Posts.xml >> ./inserts/nodes/posts.sql
guile -s ../guile/sodata/posts.scm $ds/Posts2.xml >> ./inserts/nodes/posts.sql
echo '  comments'
guile -s ../guile/sodata/comments.scm $ds/Comments.xml >> ./inserts/nodes/comments.sql
echo '  tags'
guile -s ../guile/sodata/tags.scm $ds/Tags.xml >> ./inserts/nodes/tags.sql
echo '  badges'
guile -s ../guile/sodata/badges.scm $ds/Badges.xml >> ./inserts/nodes/badges.sql
echo 'Load data (links)'
echo '  badges'
guile -s ../guile/sodata/badgeLinks.scm $ds/Badges.xml >> ./inserts/edges/badgesLinks.sql
echo '  comments'
guile -s ../guile/sodata/commentLinks.scm $ds/Comments.xml >> ./inserts/edges/commentsLinks.sql
echo '  posts'
guile -s ../guile/sodata/postLinks.scm $ds/Posts.xml >> ./inserts/edges/postsLinks.sql
guile -s ../guile/sodata/postLinks.scm $ds/Posts2.xml >> ./inserts/edges/postsLinks.sql
guile -s ../guile/sodata/similarPostLinks.scm $ds/PostLinks.xml >> ./inserts/edges/postsLinks.sql
guile -s ../guile/sodata/similarPostLinks.scm $ds/PostLinks2.xml >> ./inserts/edges/postsLinks.sql

echo 'Execute inserts'
for i in inserts/nodes/*
do
	echo "Doing inserts for $i"
	sqlite3 ./SO.db < $i;
done

for i in inserts/edges/*
do
	echo "Doing inserts for $i"
	sqlite3 ./SO.db < $i
done
