(define-module
  (amazondata querybuilder))

(use-modules
  (ice-9 match))

(define (parse-users data)
  (define users-hash (make-hash-table))
  (for-each
	(lambda (e)
	  (match e
			 ((id asin title group similar ('reviews reviews))
			  (for-each
				(lambda (review)
				  (match review
						 ((date ('customer name) rating)
						  (hash-set! users-hash name #t))))
				reviews))))
	data)
  (hash-map->list (lambda (key value) key)
				  users-hash))

(define (parse-products data)
  (map (lambda (e)
		 (match e
				((id asin title group . _)
				 (list id asin title))))
	   data))

(define (parse-groups data)
  (define h (make-hash-table))
  (for-each (lambda (e)
		 (match e
				((id asin title ('group group) . _)
				 (hash-set! h group #t))))
	   data)
  (hash-map->list (lambda (key value) key)
				  h))

(define (parse-similar-links data)
  (define links/superlist
	(map (lambda (e)
		 (match e
				((id ('asin asin) title group ('similar similar-lst) . _)
				 (map (lambda (s)
						(list asin s)) 
					  similar-lst)))) 
		 data))
  (apply append links/superlist))

(define (parse-review-links data)
  (define links/superlist
	(map (lambda (e)
		   (match e
				  ((id ('asin asin) title group similar ('reviews reviews))
				   (map (lambda (r)
						  (match r 
								 ((('date date) ('customer customer) ('rating rating))
								  `((customer ,customer) (product ,asin) (date ,date) (rating ,rating)))))
						reviews))))
		 data))
  (apply append links/superlist))

(define (parse-group-links data)
  (map 
	(lambda (e)
	  (match e
			 ((id ('asin asin) title ('group group) . _)
			  `(,asin ,group)))) 
	data))

(define (make-user-inserts users)
  (map (lambda (u)
		 (format #f "INSERT INTO Customer (id) VALUES ('~a');" u))
	   users))

(define (make-group-inserts groups)
  (map (lambda (group)
		 (format #f "INSERT INTO ProductGroup (name) VALUES ('~a');" group))
	   groups))

(define (make-product-inserts products)
  (map (lambda (p)
		 (match p
				((id ('asin asin) ('title title))
					(format #f "INSERT INTO Product (asin, title) values ('~a', '~a');" asin title))))
	   products))

(define (make-group-links-inserts group-links)
  (map 
	(lambda (link)
	  (match link
			 ((asin group)
			  (format #f 
					(string-append
					  "INSERT INTO Edges (node_from, node_to, type) " 
					  "SELECT p.gsql_node_id, g.gsql_node_id, 'belongs' "
					  "FROM Product p, ProductGroup g "
					  "WHERE p.asin = '~a' AND g.name = '~a';")
					   asin group))))
	  group-links))

(define (make-similar-links-inserts similar-links)
  (map
	(lambda (link)
	  (match link
			 ((p1 p2)
			  (format #f 
					  (string-append
						"INSERT INTO Edges (node_from, node_to, type) "	
						"SELECT pa.gsql_node_id, pb.gsql_node_id, 'similar' "
						"FROM Product pa, Product pb "
						"WHERE pa.asin = '~a' AND pb.asin = '~a';")
					   p1 p2))))
	similar-links))

(define (make-review-links-inserts review-links)
  (map
	(lambda (review)
	  (match review
			 ((('customer c)
			   ('product p)
			   ('date d)
			   ('rating r))
			  (format #f
					  (string-append
						"INSERT INTO Edges (node_from, node_to, type) "
						"SELECT c.gsql_node_id, p.gsql_node_id, 'Review' "
						"FROM Customer c, Product p "
						"WHERE c.id = '~a' AND p.asin = '~a';"
						"INSERT INTO Review (reviewDate, rating, gsql_edge_id) "
						"VALUES ('~a', ~a, last_insert_rowid()); ")
					   c p d r))))
	review-links))


(export parse-users parse-products parse-similar-links parse-groups parse-group-links parse-review-links make-user-inserts make-group-inserts make-product-inserts make-group-links-inserts make-similar-links-inserts make-review-links-inserts)
