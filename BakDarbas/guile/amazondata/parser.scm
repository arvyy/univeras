(define-module
  (amazondata parser))

(use-modules
  (ice-9 format)
  (ice-9 textual-ports)
  (ice-9 regex))

(define (parse-data file start end)
  (parse-data* '() (open-file file "r") start end))

(define (parse-data* data port start end)
  (define l1 (get-line port))
  (cond
	((eof-object? l1) (reverse data))
	((string-every char-whitespace? l1) (parse-data* data port start end))
	(else (let* ((id (match-id l1))
				 (asin (match-asin (get-line port))))
			(define l (get-line port))
			(cond
			  ((string-match "\\s*discontinued product" l) (parse-data* data port start end))
			  (else (let* ((title (match-title l))
						   (group (match-group (get-line port)))
						   (skip-sales (get-line port))
						   (similar (match-similar (get-line port)))
						   (skip-cat (skip-categories port))
						   (reviews (get-reviews port)))
					  (define entry 
						`((id ,id)
						  (asin ,asin)
						  (title ,title)
						  (group ,group)
						  (similar ,similar)
						  (reviews ,reviews)))
					  (cond
						((< id start)
						 (parse-data* data port start end))
						((>= id end)
						 (reverse data))
						(else (parse-data* (cons entry data) port start end)) ))))))))

(define (match-id line)
  (define m (string-match "^\\s*Id:\\s*([0-9]+)\\s*$" line))
  (define start (match:start m 1))
  (define end (match:end m 1))
  (string->number (substring line start end)))

(define (match-asin line)
  (define m (string-match "^\\s*ASIN:\\s*([a-zA-Z0-9]+)\\s*$" line))
  (define start (match:start m 1))
  (define end (match:end m 1))
  (substring line start end))

(define (match-title line)
  (define m (string-match "^\\s*title:\\s*(.+)\\s*$" line))
  (define start (match:start m 1))
  (define end (match:end m 1))
  (substring line start end))

(define (match-group line)
  (define m (string-match "^\\s*group:\\s*(.+)\\s*$" line))
  (define start (match:start m 1))
  (define end (match:end m 1))
  (substring line start end))

(define (match-similar line)
  (define m (string-match "\\s*similar:\\s*([0-9]+)\\s*" line))
  (define count (string->number (substring line
										   (match:start m 1)
										   (match:end m 1))))
  (let it ((i count)
		   (similar '())
		   (line (substring line (match:end m 0))))
	(cond
	  ((= i 0) (reverse similar))
	  (else (let* ((m (string-match "\\s*([a-zA-Z0-9]+)\\s*" line))
				   (s (substring line (match:start m 1) (match:end m 1)))
				   (next-line (substring line (match:end m 0))))
			  (it (- i 1)
				  (cons s similar)
				  next-line))))))

(define (skip-categories port)
  (define l (get-line port))
  (define m (string-match "\\s*categories:\\s*([0-9]+)" l))
  (define cat-count (string->number (substring l (match:start m 1) (match:end m 1))))
  (let it ((i cat-count))
	(if (= i 0) #t
		(begin
		  (get-line port)
		  (it (- i 1))))))

(define (get-reviews port)
  (define l (get-line port))
  (define m (string-match "^\\s*reviews:\\s*total:\\s*[0-9]+\\s*downloaded:\\s*([0-9]+).*$" l))
  (define rev-count (string->number (substring l (match:start m 1) (match:end m 1))))
  (let it ((reviews '())
		   (i rev-count))
	(cond 
	  ((= i 0) (reverse reviews))
	  (else (let ()
			  (define l (get-line port))
			  (define m (string-match "\\s*([0-9]{4}-[0-9]{1,2}-[0-9]{1,2})\\s*cutomer:\\s*([a-zA-Z0-9]+)\\s*rating:\\s*([0-5])" l))
			  (define date (substring l (match:start m 1) (match:end m 1)))
			  (define customer (substring l (match:start m 2) (match:end m 2)))
			  (define rating (string->number (substring l (match:start m 3) (match:end m 3))))
			  (define review `((date ,date) (customer ,customer) (rating ,rating)))
			  (it (cons review reviews) (- i 1)))))))

(export parse-data)
