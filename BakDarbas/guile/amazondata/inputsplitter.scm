(use-modules
  (ice-9 textual-ports)
  (ice-9 format)
  (ice-9 regex))

(define input-file (cadr (command-line)))
(define output-file (caddr (command-line)))
(define in (open-file input-file "r"))
(let it ((i 0)
		 (output (open-output-file (string-append output-file ".0"))))
  (define line (get-line in))
  (cond
	((eof-object? line) #t)
	(else (let ()
			(define m (string-match "^\\s*Id:\\s*([0-9]+)\\s*$" line))
			(define use-new-file?
			  (and m (= (floor-remainder (string->number (substring line 
																	(match:start m 1)
																	(match:end m 1)))
										 1000)
						0)))
			(define-values
			  (i* output*)
			  (if use-new-file?
				  (values (+ 1 i)
						  (begin
							(close-port output)
							(open-output-file (format #f "~a.~a" output-file (+ 1 i)))))
				  (values i output)))
			(put-string output* line)
			(put-string output* "\n")
			(it i* output*)))))
