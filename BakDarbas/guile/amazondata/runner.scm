(use-modules
  (ice-9 match)
  (amazondata parser)
  (amazondata querybuilder))

(define-values 
  (input-file output-folder start end)
  (match (command-line)
		 ((script in out start end)
		  (values in out (string->number start) (string->number end)))))

(define data (parse-data input-file start end))
(mkdir output-folder)

(with-output-to-port
  (open-output-file (string-append output-folder "/users.sql" )) 
  (lambda ()
	(for-each
	  (lambda (sql)
		(display sql)
		(newline))
	  (make-user-inserts (parse-users data)))
	))
(with-output-to-port
  (open-output-file (string-append output-folder "/products.sql" )) 
  (lambda () 
	(for-each
	  (lambda (sql)
		(display sql)
		(newline))
	  (make-product-inserts (parse-products data)))
	))
(with-output-to-port
  (open-output-file (string-append output-folder "/groups.sql" )) 
  (lambda () 
	(for-each
	  (lambda (sql)
		(display sql)
		(newline))
	  (make-group-inserts (parse-groups data)))))
(with-output-to-port
  (open-output-file (string-append output-folder "/group-links.sql" )) 
  (lambda ()
	(for-each
	  (lambda (sql)
		(display sql)
		(newline))
	  (make-group-links-inserts (parse-group-links data)))
	))
(with-output-to-port
  (open-output-file (string-append output-folder "/similar-links.sql" )) 
  (lambda ()
	(for-each
	  (lambda (sql)
		(display sql)
		(newline))
	  (make-similar-links-inserts (parse-similar-links data)))
	))
(with-output-to-port
  (open-output-file (string-append output-folder "/review-links.sql" )) 
  (lambda ()
	(for-each
	  (lambda (sql)
		(display sql)
		(newline))
	  (make-review-links-inserts (parse-review-links data)))
	))
