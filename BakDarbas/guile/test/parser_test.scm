(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules
  (gsql lexer)
  (gsql parser)
  (srfi srfi-64))

(test-begin "Parser")

(test-group "Parse simple"
            (define parsed (parse (tokenize "matching ()-[]-()")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE #f (GEN "gsql_0"))
                           (EDGE #f (GEN "gsql_1") ANY)
                           (NODE #f (GEN "gsql_2")))) graph))

(test-group "Parse simple from custom table"
            (define parsed (parse (tokenize "matching graph(nodes2, edges2) ()-[]-()")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE #f (GEN "gsql_0"))
                           (EDGE #f (GEN "gsql_1") ANY)
                           (NODE #f (GEN "gsql_2")))) graph)
			(test-equal (vector-ref parsed 3) (cons "nodes2" "edges2")))

(test-group "Parse typed node"
            (define parsed (parse (tokenize "matching (Person:)-[]-()")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE "Person" (GEN "gsql_0"))
                           (EDGE #f (GEN "gsql_1") ANY)
                           (NODE #f (GEN "gsql_2")))) graph))

(test-group "Parse typed named node"
            (define parsed (parse (tokenize "matching (Person:p)-[]-()")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE "Person" "p")
                           (EDGE #f (GEN "gsql_0") ANY)
                           (NODE #f (GEN "gsql_1")))) graph))

(test-group "Parse untyped named node"
            (define parsed (parse (tokenize "matching (p)-[]-()")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE #f "p")
                           (EDGE #f (GEN "gsql_0") ANY)
                           (NODE #f (GEN "gsql_1")))) graph))

(test-group "Parse typed edge"
            (define parsed (parse (tokenize "matching ()-[Friend:]-()")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE #f (GEN "gsql_0"))
                           (EDGE "Friend" (GEN "gsql_1") ANY)
                           (NODE #f (GEN "gsql_2")))) graph))

(test-group "Parse labeled edge"
			(define parsed (parse (tokenize "matching ()-['friend']-()")))
			(define graph (vector-ref parsed 0))
			(test-equal '(((NODE #f (GEN "gsql_0"))
						   (EDGE (LABEL "friend") (GEN "gsql_1") ANY)
						   (NODE #f (GEN "gsql_2")))) graph))

(test-group "Parse typed named edge"
            (define parsed (parse (tokenize "matching ()-[Friend:f]-()")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE #f (GEN "gsql_0"))
                           (EDGE "Friend" "f" ANY)
                           (NODE #f (GEN "gsql_1")))) graph))

(test-group "Parse left edge"
            (define parsed (parse (tokenize "matching ()<-[]-()")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE #f (GEN "gsql_0"))
                           (EDGE #f (GEN "gsql_1") LEFT)
                           (NODE #f (GEN "gsql_2")))) graph))

(test-group "Parse right edge"
            (define parsed (parse (tokenize "matching ()-[]->()")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE #f (GEN "gsql_0"))
                           (EDGE #f (GEN "gsql_1") RIGHT)
                           (NODE #f (GEN "gsql_2")))) graph))

(test-group "Parse alternative any edge"
            (define parsed (parse (tokenize "matching ()<-[]->()")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE #f (GEN "gsql_0"))
                           (EDGE #f (GEN "gsql_1") ANY)
                           (NODE #f (GEN "gsql_2")))) graph))

(test-group "Parse longer chain"
            (define parsed (parse (tokenize "matching (a)-[]-()-[]-(c)")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE #f "a")
                           (EDGE #f (GEN "gsql_0") ANY)
                           (NODE #f (GEN "gsql_1")))
                          ((NODE #f (GEN "gsql_1"))
                           (EDGE #f (GEN "gsql_2") ANY)
                           (NODE #f "c"))) graph))

(test-group "Parse multipl chains"
            (define parsed (parse (tokenize "matching (a)-[]-(b), (b)-[]-(c)")))
            (define graph (vector-ref parsed 0))
            (test-equal '(((NODE #f "a")
                           (EDGE #f (GEN "gsql_0") ANY)
                           (NODE #f "b"))
                          ((NODE #f "b")
                           (EDGE #f (GEN "gsql_1") ANY)
                           (NODE #f "c"))) graph))

(test-end "Parser")
