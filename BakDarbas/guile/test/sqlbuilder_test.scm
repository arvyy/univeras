;(add-to-load-path (string-append (dirname (current-filename)) "/.."))
;
;(use-modules
;  (gsql lexer)
;  (gsql parser)
;  (gsql sqlbuilder)
;  (srfi srfi-64))
;
;(test-begin "Sql builder")
;
;(test-group "Simple chain"
;            (define input "matching (a)-[]-(b)")
;            (define tokens (tokenize input))
;            (define parsed (parse tokens))
;            (define triplets (vector-ref parsed 0))
;            (define sql (build-sql triplets "nodes" "edges" "gsql_node_id" "gsql_edge_id"))
;            (test-equal "FROM nodes AS b, edges AS gsql_0, nodes AS a WHERE ((gsql_0.from = b.id AND gsql_0.to = a.id) OR (gsql_0.from = a.id AND gsql_0.to = b.id))" sql))
;
;(test-end "Sql builder")
