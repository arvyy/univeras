(add-to-load-path (string-append (dirname (current-filename)) "/.."))

(use-modules
  (gsql lexer)
  (srfi srfi-64))

(test-begin "Lexer")

(test-group "Id"
			(define input "test1")
			(define tokens (tokenize input))
			(test-equal '((ID "test1")) tokens))

(test-group "Empty token"
            (define input "")
            (define tokens (tokenize input))
            (test-equal '() tokens))

(test-group "Comment"
            (test-equal '(OPEN_PAREN) (tokenize "(--)"))
            (test-equal '(OPEN_PAREN CLOSE_PAREN) (tokenize "(--\n)"))
            (test-equal '(OPEN_PAREN CLOSE_PAREN) (tokenize "(/*.**/)")))

(test-group "Simple terminal token"
            (define input "(")
            (test-equal '(OPEN_PAREN) (tokenize input)))

(test-group "Simple terminal token with space"
            (define input "  (")
            (test-equal '(OPEN_PAREN) (tokenize input)))

(test-group "All terminal non-keyword tokens"
            (define input "()<>-.[]:,")
            (test-equal '(OPEN_PAREN CLOSE_PAREN LT GT MINUS DOT OPEN_SQ_PAREN CLOSE_SQ_PAREN COLON COMMA) (tokenize input)))

(test-group "All terminal keyword tokens"
            (define input "AS WHERE MATCHING GRAPH as where matching graph")
            (test-equal '(AS WHERE MATCHING GRAPH AS WHERE MATCHING GRAPH) (tokenize input)))

(test-group "Parse quoted text"
            (define input "'test'")
            (test-equal '((QUOTED_TEXT "test")) (tokenize input)))

(test-group "Parse double quoted text"
            (define input "\"test\"")
            (test-equal '((DOUBLE_QUOTED_TEXT "test")) (tokenize input)))

(test-group "Convert tokens back to string"
            (define input "() AS 'test'")
            (define tokens (tokenize input))
            (define strings (map token->string tokens))
            (test-equal '("(" ")" " AS " "'test'") strings))

(test-end "Lexer")
