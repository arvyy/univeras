(use-modules
  (ice-9 textual-ports )
  (gsql converter))

(define input (get-string-all (current-input-port)))
(put-string (current-output-port) (convert input))
