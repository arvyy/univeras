(define-module
  (gsql sqlbuilder))

(use-modules
  (srfi srfi-1)
  (ice-9 match)
  (ice-9 format))

(define (build-sql triplets nodes-table edges-table nodes-fk edges-fk)
  (define from-clauses (build-from-clauses triplets nodes-table edges-table))
  (define where-clauses (build-where-clauses triplets nodes-fk edges-fk))
  (define from-statement
    (format #f "FROM ~a" (string-join from-clauses ", ")))
  (define where-statement
    (format #f "WHERE ~a" (string-join where-clauses " AND ")))
  (string-append from-statement " " where-statement))

(define (string-join strings sep)
  (reduce (lambda (a b)
            (string-append a sep b)) 
          "" 
          strings))

(define (build-where-clauses triplets nodes-fk edges-fk)
  (define (node->where-clauses node)
    (match node
           (('NODE (and (not #f)
                        table)
                    (and (not ('GEN _))
                         alias))
            (list (format #f "gsql_~a.id = ~a.~a" alias alias nodes-fk)
                  (format #f "gsql_~a.type = '~a'" alias table)))
           (('NODE (and (not #f)
                        table)
                    ('GEN alias))
            (list (format #f "~a.type = '~a'" alias table)))
           (_ '())))
  (define (edge->where-clauses edge)
    (match edge
           (('EDGE (and (not #f)
						(not ('LABEL _))
                        table)
                    (and (not ('GEN _))
                         alias)
					_)
            (list (format #f "gsql_~a.id = ~a.~a" alias alias edges-fk)
                  (format #f "gsql_~a.type = '~a'" alias table)))
		   (('EDGE ('LABEL label) ('GEN alias) _)
			(list (format #f "~a.type = '~a'" alias label)))
           (_ '())))
  (define (join-left-dir n1 e n2)
    (format #f "~a.node_from = ~a.id AND ~a.node_to = ~a.id" e n2 e n1))
  (define (join-right-dir n1 e n2)
    (format #f "~a.node_from = ~a.id AND ~a.node_to = ~a.id" e n1 e n2))
  (define (join-both-dir n1 e n2)
    (format #f "((~a) OR (~a))" 
            (join-left-dir n1 e n2)
            (join-right-dir n1 e n2)))
  (define (graph-object-alias name)
	(match name
		   (('GEN name) name)
		   (name (format #f "gsql_~a" name))))
  (define (triplet->join-clause triplet)
    (match triplet
      ((('NODE _ n1)
        ('EDGE _ e dir)
        ('NODE _ n2))
	   (let ((n1* (graph-object-alias n1))
			 (e* (graph-object-alias e))
			 (n2* (graph-object-alias n2)))
		 (cond
		   ((equal? dir 'LEFT) (join-left-dir n1* e* n2*))
		   ((equal? dir 'ANY) (join-both-dir n1* e* n2*))
		   ((equal? dir 'RIGHT) (join-right-dir n1* e* n2*))))
       )))
  (define (triplet->where-clauses triplet)
    (match triplet
           ((node1 edge node2)
            (append (list (triplet->join-clause triplet))
                    (node->where-clauses node1)
                    (edge->where-clauses edge)
                    (node->where-clauses node2)))))
  (define where-clauses/superlist
    (map triplet->where-clauses triplets))
  (apply append where-clauses/superlist))

(define (build-from-clauses triplets nodes-table edges-table)
  (define node-tables (make-hash-table))
  (define (node->from-clauses node)
    (match node
           (('NODE _ ('GEN alias)) 
			(cond
			  ((hash-ref node-tables alias) (list))
			  (else (begin
					  (hash-set! node-tables alias #t)
					  (list (format #f "~a AS ~a" nodes-table alias))))))
           (('NODE #f alias) 
			(cond
			  ((hash-ref node-tables alias) (list))
			  (else (begin
					  (hash-set! node-tables alias #t)
					  (list (format #f "~a AS ~a" nodes-table alias))))))
           (('NODE table alias) 
			(cond
			  ((hash-ref node-tables alias) (list))
			  (else (begin
					  (hash-set! node-tables alias #t)
					  (list (format #f "~a AS gsql_~a" nodes-table alias)
                                      (format #f "~a AS ~a" table alias) )))))))
  (define (edge->from-clauses edge)
    (match edge
           (('EDGE _ ('GEN alias) _) (list (format #f "~a AS ~a" edges-table alias)))
           (('EDGE table alias _) (list (format #f "~a AS gsql_~a" edges-table alias)
                                        (format #f "~a AS ~a" table alias)))))
  (define (triplet->from-clauses triplet)
    (match triplet
           ((node1 edge node2)
            (append (node->from-clauses node1)
                    (edge->from-clauses edge)
                    (node->from-clauses node2)))))
  (define from-clauses/superlist
    (map triplet->from-clauses triplets))
  (apply append from-clauses/superlist))

(export build-sql)
