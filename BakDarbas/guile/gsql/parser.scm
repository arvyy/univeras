(define-module
  (gsql parser))

(use-modules
  (ice-9 format)
  (ice-9 match))

; TODO refactor duplication
(define (parse tokens)
  (match tokens
		 (('MATCHING 'GRAPH 'OPEN_PAREN ('ID nodestbl) 'COMMA ('ID edgestbl) 'CLOSE_PAREN . rest)
		  (match (parse-graphs rest)
				 (#(graphs rest)
				  (match rest
						 (('WHERE . rest*) (vector graphs rest* #t (cons nodestbl edgestbl)))
						 (_ (vector graphs rest #f (cons nodestbl edgestbl)))))))
		 (('MATCHING . rest)
		  (match (parse-graphs rest)
				 (#(graphs rest)
				  (match rest
						 (('WHERE . rest*) (vector graphs rest* #t #f))
						 (_ (vector graphs rest #f #f))))))

		 (_ #f)))

(define (parse-graphs input)
  (let it ((next-node? #t)
           (graphs '())
           (graph '())
           (input input))
    (cond
      (next-node? (match (parse-node input)
                         (#f (throw 'PARSER_ERROR "Expected node"))
                         (#(node rest) (it #f graphs (cons node graph) rest))))
      ((parse-edge input) => (lambda (parsed)
                               (match parsed
                                      (#(edge rest) (it #t graphs (cons edge graph) rest)))))
      (else (match input
                   (('COMMA . rest)
                    (it #t (cons (reverse graph) graphs) '() rest))
                   (rest (let* ((graphs (reverse (cons (reverse graph) graphs)))
                                (graphs (graphs/gen-names graphs))
                                (triplets (graphs->triplets graphs)))
                           (vector triplets rest))))))))

(define (parse-node input)
  (match input
         (('OPEN_PAREN 'CLOSE_PAREN . rest) (vector '(NODE #f #f) rest))
         (('OPEN_PAREN ('ID colname) 'COLON 'CLOSE_PAREN . rest) (vector `(NODE ,colname #f) rest))
         (('OPEN_PAREN ('ID colname) 'COLON ('ID aliasname) 'CLOSE_PAREN . rest) (vector `(NODE ,colname ,aliasname) rest))
         (('OPEN_PAREN ('ID aliasname) 'CLOSE_PAREN . rest) (vector `(NODE #f ,aliasname) rest))))


(define (parse-edge input)
  (define-values (left-dir input*) 
    (match input
           (('LT 'MINUS . rest) (values 'LEFT rest))
           (('MINUS . rest) (values 'ANY rest))
           (_ (values #f #f))))
  (if left-dir
      (parse-edge/after-left left-dir input*)
      #f))

(define (parse-edge/after-left left-dir input)
  (define-values (edge-def input*)
    (match input
           (('OPEN_SQ_PAREN 'CLOSE_SQ_PAREN . rest)
            (values '(EDGE #f #f) rest))
           (('OPEN_SQ_PAREN ('ID colname) 'COLON 'CLOSE_SQ_PAREN . rest)
            (values `(EDGE ,colname #f) rest))
           (('OPEN_SQ_PAREN ('ID colname) 'COLON ('ID aliasname) 'CLOSE_SQ_PAREN . rest)
            (values `(EDGE ,colname ,aliasname) rest))
           (('OPEN_SQ_PAREN ('QUOTED_TEXT label) 'CLOSE_SQ_PAREN . rest)
            (values `(EDGE (LABEL ,label) #f) rest))
           (_ (throw 'PARSER_ERROR "Bad edge definition: ~a" input))))
  (parse-edge/after-edge-def left-dir edge-def input*))

(define (parse-edge/after-edge-def left-dir edge-def input)
  (define-values (right-dir input*)
    (match input
           (('MINUS 'GT . rest) (values 'RIGHT rest))
           (('MINUS . rest) (values 'ANY rest))
           (_ (throw 'PARSER_ERROR "Bad edge definition"))))
  (define dir
    (match (cons left-dir right-dir)
           (('LEFT . 'ANY) 'LEFT)
           (('LEFT . 'RIGHT) 'ANY)
           (('ANY . 'ANY) 'ANY)
           (('ANY . 'RIGHT) 'RIGHT)))
  (define edge
    (match edge-def
           (('EDGE col alias)
            (list 'EDGE col alias dir))))
  (vector edge input*))

(define (graphs->triplets graphs)
  (define triplets/superlist (map graph->triplets graphs))
  (apply append triplets/superlist))

(define (graphs/gen-names graphs)
  (let it ((graphs graphs)
           (i 0)
           (named-graphs '()))
    (cond
      ((null? graphs) (reverse named-graphs))
      (else (let ()
              (define-values (named-graph new-i) (graph/gen-names (car graphs) i))
              (it (cdr graphs) new-i (cons named-graph named-graphs)))))))

(define (graph/gen-names graph i)
  (let it ((i i)
           (graph graph)
           (named-graph '()))
    (match graph
           ((('NODE table #f) . rest) (it (+ 1 i) (cdr graph) (cons `(NODE ,table (GEN ,(format #f "gsql_~a" i))) named-graph)))
           ((('EDGE table #f dir) . rest) (it (+ 1 i) (cdr graph) (cons `(EDGE ,table (GEN ,(format #f "gsql_~a" i)) ,dir) named-graph)))
           ((obj . rest) (it i (cdr graph) (cons obj named-graph)))
           (() (values (reverse named-graph) i)))))

(define (graph->triplets graph)
  (define (graph->triplets* triplets graph)
    (match graph
           ((node1 edge node2 . rest) (graph->triplets* (cons (list node1 edge node2) triplets) (cons node2 rest)))
           (_ (reverse triplets))))
  (graph->triplets* '() graph))

(export parse)
