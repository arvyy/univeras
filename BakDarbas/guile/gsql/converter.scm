(define-module
  (gsql converter))

(use-modules
  (ice-9 match)
  (gsql lexer)
  (gsql parser)
  (gsql sqlbuilder))

(define nodes-table (make-parameter "nodes"))
(define edges-table (make-parameter "edges"))
(define nodes-fk (make-parameter "gsql_node_id"))
(define edges-fk (make-parameter "gsql_edge_id"))

(define (convert str)
  (define tokens (tokenize str))
  (let it ((processed '())
           (tokens tokens))
    (cond
      ((null? tokens) (join-processed-units (reverse processed)))
      ((parse tokens) => (lambda (parsed)
                           (match parsed
                                  (#(triplets rest has-where? graph-meta)
                                   (let* ((nodes-tbl (if graph-meta (car graph-meta) (nodes-table)))
										  (edges-tbl (if graph-meta (cdr graph-meta) (edges-table)))
										  (sql (build-sql triplets nodes-tbl edges-tbl (nodes-fk) (edges-fk)))
                                          (sql (if has-where? (string-append sql " AND ") sql))
                                          (new-processed (cons `(SQL_SPLICE ,sql) processed)))
                                        (it new-processed rest))))))
      (else (it (cons (car tokens) processed) 
                (cdr tokens)))))) 

(define (join-processed-units processed)
  (define (to-string unit)
    (match unit
           (('SQL_SPLICE sql) sql)
           (token (token->string token))))
  (define strings 
    (map to-string processed))
  (apply string-append strings))

(export convert nodes-table nodes-fk edges-table edges-fk)
