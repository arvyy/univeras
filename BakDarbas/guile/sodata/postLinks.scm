(use-modules
  (ice-9 match)
  (ice-9 regex)
  (ice-9 format)
  (sxml simple)
  (sxml match))

(define (parse-tags str)
  (define (parse-tags* tags str)
	(cond
	  ((= (string-length str) 0) tags)
	  (else (let* ((m (string-match "<([^>]*)>" str))
				   (tag (substring str (match:start m 1) (match:end m 1)))
				   (rest-string (substring str (match:end m 0))))
			 	(parse-tags* (cons tag tags) rest-string) ))))
  (parse-tags* '() str))

(define (xml->insert xml)
  (cond 
	((string? xml) "")
	(else (sxml-match xml
					  ;question
					  ((row (@ (PostTypeId "1")
							   (OwnerUserId ,ownerUserId)
							   (Id ,id)
							   (AcceptedAnswerId (,answerId #f))
							   (Tags ,tags)
							   ))
					   (let ()
						 (define selectedAnswerLink
						   (if answerId
							   (format #f
									   (string-append
										 "INSERT INTO Edges(node_from, node_to, type) "
										 "SELECT q.gsql_node_id, a.gsql_node_id, 'selected_answer' "
										 "FROM Post q, Post a "
										 "WHERE q.id = '~a' AND a.id = '~a';\n")
									   id answerId) ""))
						 (define authorLink
						   (format #f
								   (string-append
									 "INSERT INTO Edges(node_from, node_to, type) "
									 "SELECT u.gsql_node_id, q.gsql_node_id, 'ask' "
									 "FROM User u, Post q "
									 "WHERE q.id = ~a AND u.id = ~a;\n"
									 )
									id ownerUserId))
						 (define tagLinks
						   (map (lambda (tag)
								  (format #f
										  (string-append
											"INSERT INTO Edges(node_from, node_to, type) "
											"SELECT q.gsql_node_Id, t.gsql_node_id, 'tagged' "
											"FROM Post q, Tag t "
											"WHERE q.id = ~a AND t.name = '~a';\n"
											)
										  id tag))
								(parse-tags tags)))
						 (define statements
						   (append (list authorLink selectedAnswerLink) tagLinks))
						 (apply string-append statements)))
					  ; answer
					  ((row (@ (PostTypeId "2")
							   (OwnerUserId ,ownerUserId)
							   (ParentId ,questionId)
							   (Id ,id)))
					   (let ()
						 (define askedBy (format #f 
							   (string-append
									 "INSERT INTO Edges(node_from, node_to, type) "
									 "SELECT u.gsql_node_id, q.gsql_node_id, 'answer' "
									 "FROM User u, Post q "
									 "WHERE q.id = ~a AND u.id = ~a;\n"
									 )
							   id ownerUserId))
						 (define answerTo (format #f 
							   (string-append
									 "INSERT INTO Edges(node_from, node_to, type) "
									 "SELECT a.gsql_node_id, q.gsql_node_id, 'answer_to' "
									 "FROM Post a, Post q "
									 "WHERE a.id = ~a AND q.id = ~a;\n"
									 )
							   id questionId))
						 (string-append askedBy answerTo)))
					  (,any "")))))

(define (output-data xmldata)
  (sxml-match xmldata
		 ((*TOP* (*PI* ,any ...) (posts ,rows ...))
		  (display "BEGIN;")
		  (newline)
		  (for-each
			(lambda (row)
			  (display (xml->insert row))
			  (newline))
			(list rows ...))
		  (display "END;")
		  (newline))))

(define file-name (cadr (command-line)))
(define in (open-file file-name "r"))
(output-data (xml->sxml in  #:declare-namespaces? #f #:trim-whitespace? #t))
