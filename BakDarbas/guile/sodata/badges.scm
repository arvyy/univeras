(use-modules
  (ice-9 match)
  (sxml simple)
  (sxml match))

(define (xml->insert xml)
  (cond 
	((string? xml) "")
	(else (sxml-match xml
					  ((row (@ (Id ,id)
							   (UserId ,userId)
							   (Name ,name)
							   (Date ,date)
							   (Class ,class)
							   (TagBased ,tagBased)))
					   (format #f 
							   (string-append 
								 "INSERT OR IGNORE INTO Badge(name, tagBased) "
								 "VALUES ('~a',~a);\n")
							   name (if tagBased 1 0)))
					  (,any (error (format #f "Unmatched: ~a" any)))))))

(define (output-data xmldata)
  (sxml-match xmldata
		 ((*TOP* (*PI* ,any ...) (badges ,rows ...))
		  (display "BEGIN;")
		  (newline)
		  (for-each
			(lambda (row)
			  (display (xml->insert row))
			  (newline))
			(list rows ...))
		  (display "END;")
		  (newline))))

(define file-name (cadr (command-line)))
(define in (open-file file-name "r"))
(output-data (xml->sxml in  #:declare-namespaces? #f #:trim-whitespace? #t))
