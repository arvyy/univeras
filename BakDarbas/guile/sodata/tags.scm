(use-modules
  (ice-9 match)
  (sxml simple)
  (sxml match))

(define (xml->insert xml)
  (cond 
	((string? xml) "")
	(else (sxml-match xml
					  ((row (@ (Id ,id)
							   (TagName ,tagname)
							   (Count ,count)))
					   (format #f 
							   (string-append 
								 "INSERT INTO Tag(id, name, count) "
								 "VALUES (~a,'~a',~a);")
							   id tagname count))))))

(define (output-data xmldata)
  (sxml-match xmldata
		 ((*TOP* (*PI* ,any ...) (tags ,rows ...))
		  (display "BEGIN;")
		  (newline)
		  (for-each
			(lambda (row)
			  (display (xml->insert row))
			  (newline))
			(list rows ...))
		  (display "END;")
		  (newline))))

(define file-name (cadr (command-line)))
(define in (open-file file-name "r"))
(output-data (xml->sxml in  #:declare-namespaces? #f #:trim-whitespace? #t))
