(use-modules
  (ice-9 match)
  (ice-9 regex)
  (sxml simple)
  (sxml match))


(define (string-replace-substring str substr replacement)
  (regexp-substitute/global #f substr str 'pre replacement 'post))

(define (xml->insert xml)
  (cond 
	((string? xml) "")
	(else (sxml-match xml
					  ((row (@ (Id ,id)
							   (Text ,text)
							   (Score ,score)
							   (CreationDate ,date)
							   ))
					   (format #f 
							   (string-append 
								 "INSERT INTO Comment(id, text, score, createdate) "
								 "VALUES (~a,'~a',~a,'~a');\n")
							   id 
							   (string-replace-substring text "'" "''")
							   score date))
					  (,any (error (format #f "Unmatched: ~a" any)))
					  ))))

(define (output-data xmldata)
  (sxml-match xmldata
		 ((*TOP* (*PI* ,any ...) (comments ,rows ...))
		  (display "BEGIN;")
		  (newline)
		  (for-each
			(lambda (row)
			  (display (xml->insert row))
			  (newline))
			(list rows ...))
		  (display "END;")
		  (newline))))

(define file-name (cadr (command-line)))
(define in (open-file file-name "r"))
(output-data (xml->sxml in  #:declare-namespaces? #f #:trim-whitespace? #t))
