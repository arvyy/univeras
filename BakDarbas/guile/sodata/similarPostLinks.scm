(use-modules
  (ice-9 match)
  (sxml simple)
  (sxml match))

(define (xml->insert xml)
  (cond 
	((string? xml) "")
	(else (sxml-match xml
					  ((row (@ (PostId ,postId)
							   (RelatedPostId ,relatedPostId)))
					   (format #f 
							   (string-append 
								 "INSERT INTO Edges(node_from, node_to, type) "
								 "SELECT p1.gsql_node_id, p2.gsql_node_id, 'similar' "
								 "FROM Post p1, Post p2 "
								 "WHERE p1.id = ~a AND p2.id = ~a;\n"
								 )
							   postId relatedPostId))
					  (,any "")))))

(define (output-data xmldata)
  (sxml-match xmldata
		 ((*TOP* (*PI* ,any ...) (postlinks ,rows ...))
		  (display "BEGIN;")
		  (newline)
		  (for-each
			(lambda (row)
			  (display (xml->insert row))
			  (newline))
			(list rows ...))
		  (display "END;")
		  (newline))))

(define file-name (cadr (command-line)))
(define in (open-file file-name "r"))
(output-data (xml->sxml in  #:declare-namespaces? #f #:trim-whitespace? #t))
