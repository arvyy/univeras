(use-modules
  (ice-9 match)
  (sxml simple)
  (sxml match))

(define (xml->insert xml)
  (cond 
	((string? xml) "")
	(else (sxml-match xml
					  ((row (@ (Id ,id)
							   (PostId ,postId)
							   (UserId (,userId -2))
							   ))
					   (let ()
						 (define post-link
						   (format #f 
							   (string-append 
								 "INSERT INTO Edges(node_from, node_to, type) "
								 "SELECT c.gsql_node_id, p.gsql_node_id, 'comment_on' "
								 "FROM Comment c, Post p "
								 "WHERE c.id = ~a AND p.id = ~a;\n"
								 )
							   id postId))
						 (define user-link
						   (format #f 
							   (string-append 
								 "INSERT INTO Edges(node_from, node_to, type) "
								 "SELECT u.gsql_node_id, p.gsql_node_id, 'comment_by' "
								 "FROM User u, Post p "
								 "WHERE p.id = ~a AND u.id = ~a;\n"
								 )
							   id userId))
						 (string-append post-link user-link)))))))

(define (output-data xmldata)
  (sxml-match xmldata
		 ((*TOP* (*PI* ,any ...) (comments ,rows ...))
		  (display "BEGIN;")
		  (newline)
		  (for-each
			(lambda (row)
			  (display (xml->insert row))
			  (newline))
			(list rows ...))
		  (display "END;")
		  (newline))))

(define file-name (cadr (command-line)))
(define in (open-file file-name "r"))
(output-data (xml->sxml in  #:declare-namespaces? #f #:trim-whitespace? #t))
