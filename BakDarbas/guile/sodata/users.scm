(use-modules
  (ice-9 match)
  (ice-9 regex)
  (sxml simple)
  (sxml match))

(define (string-replace-substring str substr replacement)
  (regexp-substitute/global #f substr str 'pre replacement 'post))

(define (xml->insert xml)
  (cond 
	((string? xml) "")
	(else (sxml-match xml
					  ((row (@ (Id ,id)
							   (Reputation ,rep)
							   (CreationDate ,create-date)
							   (DisplayName ,name)
							   (LastAccessDate ,access-date)
							   (Views ,views)
							   (UpVotes ,upvotes)
							   (DownVotes ,downvotes)))
					   (format #f 
							   (string-append 
								 "INSERT INTO User(id, reputation, createDate, name, accessDate, views, upvotes, downvotes) "
								 "VALUES (~a,~a,'~a','~a','~a',~a,~a,~a);")
							   id rep create-date (string-replace-substring name "'" "''") access-date views upvotes downvotes))))))

(define (output-data xmldata)
  (sxml-match xmldata
		 ((*TOP* (*PI* ,any ...) (users ,rows ...))
		  (display "BEGIN;")
		  (newline)
		  (for-each
			(lambda (row)
			  (display (xml->insert row))
			  (newline))
			(list rows ...))
		  (display "END;")
		  (newline))))

(define file-name (cadr (command-line)))
(define in (open-file file-name "r"))
(output-data (xml->sxml in  #:declare-namespaces? #f #:trim-whitespace? #t))
