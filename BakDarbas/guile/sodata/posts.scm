(use-modules
  (ice-9 format)
  (ice-9 match)
  (ice-9 regex)
  (sxml simple)
  (sxml match))

(define (string-replace-substring str substr replacement)
  (regexp-substitute/global #f substr str 'pre replacement 'post))

(define (xml->insert xml)
  (cond 
	((string? xml) "")
	(else (sxml-match xml
					  ;question
					  ((row (@ (PostTypeId "1")
							   (Id ,id)
							   (CreationDate ,createDate)
							   (Score ,score)
							   (ViewCount ,viewcount)
							   (Body ,body)
							   (Title (,title ""))
							   ))
					   (format #f 
							   (string-append 
								 "INSERT INTO Post(id,type,createdate,score,viewcount,body,title) "
								 "VALUES (~a,'Q','~a',~a,~a,'~a','~a');\n")
							   id createDate score viewcount 
							   (string-replace-substring body "'" "''") 
							   (string-replace-substring title "'" "''")
							   ))
					  ; answer
					  ((row (@ (PostTypeId "2")
							   (Id ,id)
							   (CreationDate ,createDate)
							   (Score ,score)
							   (Body ,body)))
					   (format #f 
							   (string-append
								 "INSERT INTO Post (id, type, createdate, score, body) "
								 "VALUES (~a, 'A', '~a', ~a, '~a');\n"
								 )
							   id createDate score 
							   (string-replace-substring body "'" "''")
							   
							   ))
					  (,any "")
					  ))))

(define (output-data xmldata)
  (sxml-match xmldata
		 ((*TOP* (*PI* ,any ...) (posts ,rows ...))
		  (display "BEGIN;")
		  (newline)
		  (for-each
			(lambda (row)
			  (display (xml->insert row))
			  (newline))
			(list rows ...))
		  (display "END;")
		  (newline))))

(define file-name (cadr (command-line)))
(define in (open-file file-name "r"))
(output-data (xml->sxml in  #:declare-namespaces? #f #:trim-whitespace? #t))
