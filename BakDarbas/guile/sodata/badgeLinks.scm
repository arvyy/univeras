(use-modules
  (ice-9 match)
  (sxml simple)
  (sxml match))

(define (xml->insert xml)
  (cond 
	((string? xml) "")
	(else (sxml-match xml
					  ((row (@ (Id ,id)
							   (UserId ,userId)
							   (Name ,name)
							   (Date ,date)
							   (Class ,class)
							   (TagBased ,tagBased)))
					   (let ()
						 (define user-link (format #f 
												   (string-append
													 "INSERT INTO Edges(node_from, node_to, type) "
													 "SELECT u.gsql_node_id, b.gsql_node_id, 'BadgeAcquisition' "
													 "FROM User u, Badge b "
													 "WHERE u.id = ~a AND b.name = '~a';\n"

													 "INSERT INTO BadgeAcquisition(gsql_edge_id, acquireDate) "
													 "VALUES(last_insert_rowid(), '~a');\n")
												   userId name date))
						 (define tag-link 
						   (format #f 
								   (string-append 
									 "INSERT INTO Edges(node_from, node_to, type) "
									 "SELECT b.gsql_node_id, t.gsql_node_id, 'BadgeTag' "
									 "FROM Badge b, Tag t "
									 "WHERE b.name = '~a' AND  t.name = '~a';")
								   name name))
						 (string-append user-link (if (equal? "True" tagBased) tag-link ""))))
					  (,any (error (format #f "Unmatched: ~a" any)))
					  ))))

(define (output-data xmldata)
  (sxml-match xmldata
		 ((*TOP* (*PI* ,any ...) (badges ,rows ...))
		  (display "BEGIN;")
		  (newline)
		  (for-each
			(lambda (row)
			  (display (xml->insert row))
			  (newline))
			(list rows ...))
		  (display "END;")
		  (newline))))

(define file-name (cadr (command-line)))
(define in (open-file file-name "r"))
(output-data (xml->sxml in  #:declare-namespaces? #f #:trim-whitespace? #t))
