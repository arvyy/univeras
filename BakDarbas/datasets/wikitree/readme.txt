Downloaded From: http://proj.ise.bgu.ac.il/sns

Please Cite:

Fire, Michael, and Yuval Elovici. "Data Mining of Online Genealogy Datasets for Revealing Lifespan Patterns in Human Population." arXiv preprint arXiv:1311.4276 (2013).�

Bibtex:
@article{fire2013data,
  title={Data Mining of Online Genealogy Datasets for Revealing Lifespan Patterns in Human Population},
  author={Fire, Michael and Elovici, Yuval},
  journal={arXiv preprint arXiv:1311.4276},
  year={2013}
}
