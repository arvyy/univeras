drop table Darbuotojas cascade;
drop table Tipas cascade;
drop table Role cascade;
drop table Dokumentas cascade;

drop table VadovasBriauna cascade;
drop table DarbMatoDokumentaBriauna cascade;
drop table DarbMatoTipaBriauna cascade;
drop table RoleMatoTipaBriauna cascade;
drop table TuriTipaBriauna cascade;
drop table TuriRoleBriauna cascade;

create table Darbuotojas (
	id int primary key not null,
	vardas char(20) not null
);

create table Tipas (
	id int primary key not null,
	pavadinimas char(20) not null
);

create table Role (
	id int primary key not null,
	pavadinimas char(20) not null
);

create table Dokumentas (
	id int primary key not null,
	pavadinimas char(20) not null
);

create table VadovasBriauna (
	id int primary key not null,
	virsune_is int not null references Darbuotojas,
	virsune_i int not null references Darbuotojas
);

create table DarbMatoDokumentaBriauna (
	id int primary key not null,
	virsune_is int not null references Darbuotojas,
	virsune_i int not null references Dokumentas
);

create table DarbMatoTipaBriauna (
	id int primary key not null,
	virsune_is int not null references Darbuotojas,
	virsune_i int not null references Tipas
);
create table TuriTipaBriauna (
	id int primary key not null,
	virsune_is int not null references Dokumentas,
	virsune_i int not null references Tipas
);
create table RoleMatoTipaBriauna (
	id int primary key not null,
	virsune_is int not null references Role,
	virsune_i int not null references Tipas
);
create table TuriRoleBriauna (
	id int primary key not null,
	virsune_is int not null references Darbuotojas,
	virsune_i int not null references Role
);
