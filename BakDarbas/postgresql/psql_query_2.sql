select dok.* from (

	with darbuotojas as (
		select d.* from Darbuotojas as d
	   	where d.vardas = 'D1'
	),

	tipai as (
		select t.* from Tipas as t, DarbMatoTipaBriauna as m, darbuotojas
		where m.virsune_is = darbuotojas.id and m.virsune_i = t.id
		union
		select t.* from Tipas as t, TuriRoleBriauna as tr, Role as r, RoleMatoTipaBriauna as rm, darbuotojas
		where tr.virsune_is = darbuotojas.id and tr.virsune_i = r.id and rm.virsune_is = r.id and rm.virsune_i = t.id
	)

	select d.* from Dokumentas as d, darbuotojas, DarbMatoDokumentaBriauna as m
	where m.virsune_is = darbuotojas.id and m.virsune_i = d.id
	union
	select d.* from Dokumentas as d, tipai, TuriTipaBriauna as turi
	where turi.virsune_is = d.id and turi.virsune_i = tipai.id
) as dok
where dok.pavadinimas like '%DOC%';
