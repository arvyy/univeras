module LM (move) where

import qualified Data.Map as Map
import Data.List
import Data.Char
import Data.Bool

move ::  String -> Either String (Maybe [String])

---

data JsonValue = JsonStringValue String | JsonObjectValue (Map.Map String JsonValue) | JsonNullValue deriving Show
parseJson :: String -> Either String (JsonValue , String)

parseString :: String -> Either String (String , String)
parseString ('"' : str) = 
	case a ([] , str) of
		Left l -> Left l
		Right (acc , rest) -> Right ((reverse acc) , rest)
	where
		a :: (String , String)-> Either String (String , String) -- (likes , sukauptas) -> (likes , sukauptas)
		a (_, []) = Left "Neuzdarytas stringas"
		a (acc , ('"' : rest) ) = Right (acc , rest)
		a (acc , (char : rest) ) = a ((char : acc) , rest)
parseString _ = Left "Str turi prasidet kabutemis"

parseJson ('n':'u':'l':'l':rest) = 
	Right (JsonNullValue , rest)
parseJson ('"':str) = 
	case parseString ('"' : str) of
		Left l -> Left l
		Right (acc, rest) -> Right (JsonStringValue acc , rest)
parseJson ('{':'}':str) = Right (JsonObjectValue Map.empty , str)
parseJson ('{':str) = parseJsonInner [] (',':str)
	where
		parseJsonInner :: [(String, JsonValue)] -> String -> Either String (JsonValue , String)
		parseJsonInner items ('}':rest) = Right (JsonObjectValue (Map.fromList items), rest)
		parseJsonInner _ [] = Left "Neterminuotas objektas"
		parseJsonInner items (',':rest) = 
			case parseObjectPair rest of
				Left l -> Left l
				Right ((key , value) , rest') -> parseJsonInner ((key , value):items) rest'
		parseJsonInner _ str = Left ("Blogas JSON objektas: " ++ str)

parseJson _ = Left "Klaida skaitant json"

parseObjectPair :: String -> Either String ((String , JsonValue) , String)
parseObjectPair str = 
	case (parseString (removeSpaces str)) of
		Left l -> Left l
		Right (key , ':':str') -> 
			case parseJson (removeSpaces str') of
				Left l -> Left l
				Right (json , rest) -> Right ((key , json) , rest)
		Right (key , _) -> Left "Nera dvitaskio"
			
	where
		removeSpaces :: String -> String
		removeSpaces (' ' : rest) = removeSpaces rest
		removeSpaces s = s

buildBattleFields :: JsonValue -> Either String (Map.Map (Integer , String , String) Bool ) -- (Map (zaidejas , x , y) hit? )
buildBattleFields jsonvalue = buildBattleFieldsInner Map.empty False jsonvalue 1
	where
		getCoords :: Integer -> Map.Map String JsonValue -> Either String (Integer , String , String)
		getCoords player jsonob = 
			case Map.lookup "coord" jsonob of
				Nothing -> Left "Truksta 'coord' reiksmes"
				Just (JsonObjectValue coord) -> 
					case Map.lookup "1" coord of
						Just (JsonStringValue coord1) -> 
							case Map.lookup "2" coord of
								Just (JsonStringValue coord2) -> Right (player , coord1 , coord2)
								_ -> Left "Bloga arba nerasta coord antra reiksme"
						_ -> Left "Bloga arba nerasta coord pirma reiksme"
				Just _ -> Left "'coord' turetu but objektas"
		
		buildBattleFieldsInner :: (Map.Map (Integer , String , String) Bool) -> Bool -> JsonValue -> Integer -> Either String (Map.Map (Integer , String , String) Bool )
		buildBattleFieldsInner battlefields lastHit JsonNullValue player = Right battlefields
		buildBattleFieldsInner _ _ (JsonStringValue _) _ = Left "netiketa reiksme"
		buildBattleFieldsInner battleFields lastHit (JsonObjectValue val) player = 
			case coords of
				Left l -> Left l
				Right coords' ->
					if Map.member coords' battleFields 
						then Left "I ta pati langeli taikyta kelis kartus"
						else case newHit of
							Right b -> (buildBattleFieldsInner (Map.insert coords' lastHit battleFields) b newVal playerNext)
							Left l -> Left l
	
			where 
				coords :: Either String (Integer, String, String)
				coords = getCoords player val

				playerNext :: Integer
				playerNext = mod (player + 1) 2

				newHit :: Either String Bool
				newHit = case Map.lookup "result" val of
					Just (JsonStringValue "HIT") -> Right True
					Just (JsonStringValue "MISS") -> Right False
					Just JsonNullValue -> Right False	
					r -> Left ("Bloga arba nerasta result reiksme: " ++ (show r))

				newVal :: JsonValue
				newVal = Map.findWithDefault (JsonNullValue) "prev" val

move str =
	case parseJson str of
		Right (json , "") -> 
			case buildBattleFields json of
				Left l -> Left l
				Right map -> findMove map 0
					where
						findMove :: Map.Map (Integer , String , String) Bool -> Integer -> Either String (Maybe [String])
						findMove battleFields it = 
							if it >= 100 then Right Nothing
							else if (Map.member (0, x, y) battleFields)
								then  findMove battleFields (1 + it)
								else Right (Just [x , y])
							where 
								x :: String
								x = [(chr ((ord 'A') + (fromIntegral (mod it 10))))]
								y :: String
								y = show (1 + (div it 10))
		Right (json , rest )-> Left "Pasaliniai simboliai"
		Left l -> Left l
