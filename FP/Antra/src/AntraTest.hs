module Main where
 
import Antra
import Test.Hspec
 
main :: IO ()
main = hspec $ do
 
  describe "Test" $ do
    it "Siusti atsaka" $ do
      makeResponse ["A", "1"] ["J", "10"] "null" `shouldBe` "{\"coord\":{\"1\":\"A\",\"2\":\"1\"},\"result\":\"HIT\",\"prev\":null}"
