{-# LANGUAGE OverloadedStrings #-}
module Antra where

import qualified Data.Map as Map
import Data.List
import Data.Char
import Data.Bool

import Network.Wreq
import Network.Wreq.Types (Postable)
import Control.Lens
import Control.Monad
import Data.Aeson.Lens (members)
import qualified Data.ByteString.Lazy.Char8 as C

import System.Environment

move ::  String -> Either String (Maybe String)
firstMove :: String

---

data JsonValue = JsonStringValue String | JsonObjectValue (Map.Map String JsonValue) | JsonNullValue deriving Show
parseJson :: String -> Either String (JsonValue , String)

parseString :: String -> Either String (String , String)
parseString ('"' : str) = 
	case a ([] , str) of
		Left l -> Left l
		Right (acc , rest) -> Right ((reverse acc) , rest)
	where
		a :: (String , String)-> Either String (String , String) -- (likes , sukauptas) -> (likes , sukauptas)
		a (_, []) = Left "Neuzdarytas stringas"
		a (acc , ('"' : rest) ) = Right (acc , rest)
		a (acc , (char : rest) ) = a ((char : acc) , rest)
parseString _ = Left "Str turi prasidet kabutemis"

parseJson ('n':'u':'l':'l':rest) = 
	Right (JsonNullValue , rest)
parseJson ('"':str) = 
	case parseString ('"' : str) of
		Left l -> Left l
		Right (acc, rest) -> Right (JsonStringValue acc , rest)
parseJson ('{':'}':str) = Right (JsonObjectValue Map.empty , str)
parseJson ('{':str) = parseJsonInner [] (',':str)
	where
		parseJsonInner :: [(String, JsonValue)] -> String -> Either String (JsonValue , String)
		parseJsonInner items ('}':rest) = Right (JsonObjectValue (Map.fromList items), rest)
		parseJsonInner _ [] = Left "Neterminuotas objektas"
		parseJsonInner items (',':rest) = 
			case parseObjectPair rest of
				Left l -> Left l
				Right ((key , value) , rest') -> parseJsonInner ((key , value):items) rest'
		parseJsonInner _ str = Left ("Blogas JSON objektas: " ++ str)

parseJson _ = Left "Klaida skaitant json"

parseObjectPair :: String -> Either String ((String , JsonValue) , String)
parseObjectPair str = 
	case (parseString (removeSpaces str)) of
		Left l -> Left l
		Right (key , ':':str') -> 
			case parseJson (removeSpaces str') of
				Left l -> Left l
				Right (json , rest) -> Right ((key , json) , rest)
		Right (key , _) -> Left "Nera dvitaskio"
			
	where
		removeSpaces :: String -> String
		removeSpaces (' ' : rest) = removeSpaces rest
		removeSpaces s = s

buildBattleFields :: JsonValue -> Either String (Map.Map (Integer , String , String) Bool ) -- (Map (zaidejas , x , y) hit? )
buildBattleFields jsonvalue = buildBattleFieldsInner Map.empty False jsonvalue 1
	where
		getCoords :: Integer -> Map.Map String JsonValue -> Either String (Integer , String , String)
		getCoords player jsonob = 
			case Map.lookup "coord" jsonob of
				Nothing -> Left "Truksta 'coord' reiksmes"
				Just (JsonObjectValue coord) -> 
					case Map.lookup "1" coord of
						Just (JsonStringValue coord1) -> 
							case Map.lookup "2" coord of
								Just (JsonStringValue coord2) -> Right (player , coord1 , coord2)
								_ -> Left "Bloga arba nerasta coord antra reiksme"
						_ -> Left "Bloga arba nerasta coord pirma reiksme"
				Just _ -> Left "'coord' turetu but objektas"
		
		buildBattleFieldsInner :: (Map.Map (Integer , String , String) Bool) -> Bool -> JsonValue -> Integer -> Either String (Map.Map (Integer , String , String) Bool )
		buildBattleFieldsInner battlefields lastHit JsonNullValue player = Right battlefields
		buildBattleFieldsInner _ _ (JsonStringValue _) _ = Left "netiketa reiksme"
		buildBattleFieldsInner battleFields lastHit (JsonObjectValue val) player = 
			case coords of
				Left l -> Left l
				Right coords' ->
					if Map.member coords' battleFields 
						then Left "I ta pati langeli taikyta kelis kartus"
						else case newHit of
							Right b -> (buildBattleFieldsInner (Map.insert coords' lastHit battleFields) b newVal playerNext)
							Left l -> Left l
	
			where 
				coords :: Either String (Integer, String, String)
				coords = getCoords player val

				playerNext :: Integer
				playerNext = mod (player + 1) 2

				newHit :: Either String Bool
				newHit = case Map.lookup "result" val of
					Just (JsonStringValue "HIT") -> Right True
					Just (JsonStringValue "MISS") -> Right False
					Just JsonNullValue -> Right False	
					r -> Left ("Bloga arba nerasta result reiksme: " ++ (show r))

				newVal :: JsonValue
				newVal = Map.findWithDefault (JsonNullValue) "prev" val

firstMove = "{\"coord\":{\"1\":\"C\",\"2\":\"5\"}}"

makeResponse :: [String] -> [String] -> String -> String

move str =
	case parseJson str of
		Right (json , "") -> 
			case buildBattleFields json of
				Left l -> Left l
				Right map -> findMove map 0
					where
						findMove :: Map.Map (Integer , String , String) Bool -> Integer -> Either String (Maybe String)
						findMove battleFields it = 
							if it >= 100 then Right Nothing
							else if (Map.member (0, x, y) battleFields)
								then  findMove battleFields (1 + it)
								else case getOpponentTarget json of
									Just [ox,oy] -> Right (Just (makeResponse [x,y] [ox, oy] str))
									_ -> Left "Nerastas prieso target"
							where 
								x :: String
								x = [(chr ((ord 'A') + (fromIntegral (mod it 10))))]
								y :: String
								y = show (1 + (div it 10))
								getOpponentTarget :: JsonValue -> Maybe [String]
								getOpponentTarget json = 
									case json of 
										JsonObjectValue map ->
											case Map.lookup "coord" map of
												Just (JsonObjectValue coordmap) ->
													case Map.lookup "1" coordmap of
														Just (JsonStringValue coordx) ->
															case Map.lookup "2" coordmap of
																Just (JsonStringValue coordy) -> Just [coordx, coordy]
																_ -> Nothing
														_ -> Nothing
												_ -> Nothing
										_ -> Nothing
		Right (json , rest )-> Left "Pasaliniai simboliai"
		Left l -> Left l

makeResponse [x, y] [opp_x, opp_y] requestJson =
	"{\"coord\":{\"1\":\"" ++ x ++ "\",\"2\":\"" ++ y ++ "\"},\"result\":\"" ++ opponent_result ++ "\",\"prev\":" ++ requestJson ++ "}"
	where
		opponent_result :: String
		opponent_result = 
			case (opp_x, opp_y) of
				("J","10") -> "HIT"
				("J","9") -> "HIT"
				("J","8") -> "HIT"
				("J","7") -> "HIT"

				("H","10") -> "HIT"
				("H","9") -> "HIT"
				("H","8") -> "HIT"

				("F","10") -> "HIT"
				("F","9") -> "HIT"
				("F","8") -> "HIT"

				("H","6") -> "HIT"
				("H","5") -> "HIT"

				("F","6") -> "HIT"
				("F","5") -> "HIT"

				("D","10") -> "HIT"
				("D","9") -> "HIT"

				("D", "7") -> "HIT"
				("D", "5") -> "HIT"
				("J", "5") -> "HIT"
				("J", "3") -> "HIT"
				_ -> "MISS"
	

address :: String
address = "http://battleship.haskell.lt/game/"

httpAwait :: String -> String -> IO()
httpPost :: String -> String -> String -> IO()


httpAwait game player = 
	do
	let opts = defaults & header "Accept" .~ ["application/json+nolists"]
	r <- getWith opts (address ++ game ++ "/player/" ++ player)
	case move (C.unpack (r ^. responseBody)) of
		Right (Just str) -> httpPost str game player
		_ -> return()

httpPost str game player =
	do
	let opts = defaults & header "Content-Type" .~ ["application/json+nolists"]
	postWith opts (address ++ game ++ "/player/" ++ player) ((C.pack str))
	httpAwait game player

