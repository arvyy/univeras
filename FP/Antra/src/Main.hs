module Main where

import Antra
import System.Environment

main :: IO ()
main = 
	do
	args <- getArgs
	case args of
		id:pl:[] -> 
			if (pl == "A")
			then (httpPost firstMove id "A")
			else (httpAwait id "B")
		_ -> return ()

