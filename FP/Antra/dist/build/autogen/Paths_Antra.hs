{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -fno-warn-implicit-prelude #-}
module Paths_Antra (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/arvydas/.cabal/bin"
libdir     = "/home/arvydas/.cabal/lib/x86_64-linux-ghc-8.0.1/Antra-0.1.0.0-4ixvp46mpXgDG5FIzwiuZk"
dynlibdir  = "/home/arvydas/.cabal/lib/x86_64-linux-ghc-8.0.1"
datadir    = "/home/arvydas/.cabal/share/x86_64-linux-ghc-8.0.1/Antra-0.1.0.0"
libexecdir = "/home/arvydas/.cabal/libexec"
sysconfdir = "/home/arvydas/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "Antra_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "Antra_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "Antra_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "Antra_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "Antra_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "Antra_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
