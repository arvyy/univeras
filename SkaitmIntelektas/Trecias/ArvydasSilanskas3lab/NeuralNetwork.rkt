#lang racket

; activation function. Fn - function, dfn - its derirative
(define-struct activation (fn dfn))

;activation using sigmoid
(define sigmoid-a (let ([sigmoid (lambda (x)
                                   (/ 1 (+ 1 (exp (* x -1)))))])
                    (activation (lambda(x)
                                  (sigmoid x))
                                (lambda(x)
                                  (*(sigmoid x) (- 1 (sigmoid x)))))))

; activation using relu
(define relu-a (activation (lambda(x) (if (< x 0)
                                          (* 0.2 x)
                                          x))
                           (lambda(x) (if (< x 0)
                                          0.2
                                          1))))
; neuron. Bias is implicit first weight
(define-struct neuron (weights wd) #:transparent )

; neuron output before applying activation function
(define (neuron-out-unactivated neuron inputs)
  (foldl (lambda(w in result)
           (+ result (* w in)))
         0
         (neuron-weights neuron)
         (cons 1 inputs)))

; neuron output with activation function applied
(define (neuron-out neuron inputs activation-fn)
  (activation-fn (neuron-out-unactivated neuron inputs)))

; neuron layer
(define-struct layer (neurons) #:transparent )

; list of layer's neurons' output, before activation function
(define (layer-out-unactivated layer inputs)
  (map (lambda(neuron)
         (neuron-out-unactivated neuron inputs))
       (layer-neurons layer)))

; list of layer's neurons' output with activation function applied
(define (layer-out layer inputs activation-fn)
  (map (lambda(neuron)
         (neuron-out neuron inputs activation-fn))
       (layer-neurons layer)))

; neural network
(define-struct neural-network (layers activation) #:transparent)

; neural network output 
(define (neural-network-out nn inputs)
  (let pass ([layers (neural-network-layers nn)]
             [inputs inputs])
    (if (empty? layers) inputs
        (pass (rest layers)
              (layer-out (first layers)
                         inputs
                         (activation-fn (neural-network-activation nn)))))))

; calculating derirative for the neuron in the last (output) layer
; out-unactivated -- neuron's output before applying activation function
; target-- teaching data / desired result
; activation -- activation fn and its derirative
(define (d-lastlayer out-unactivated target activation)
  (let* ([out ((activation-fn activation) out-unactivated)]
         [result (* (- out target) ((activation-dfn activation) out-unactivated))])
    result))

; calculating derirative for the neuron in the inner (hidden) layer
; neuron-index -- place of the neuron in its layer. Needed, because weights are stored in the next layer's neurons.
; out-unactivated -- neuron's output before applying activation function
; d-nextlayer -- deriratives of the next layer
; activation -- activation fn and its derirative
(define (d-innerlayer neuron-index out-unactivated d-nextlayer nextlayer activation)
  (define mp (map (lambda (neur d)
                    (let* ([w (list-ref (neuron-weights neur) 
                                        (add1 neuron-index))]
                           [result (* d w)])
                      result))
                  (layer-neurons nextlayer)
                  d-nextlayer))
  (* (foldl + 0 mp) 
     ((activation-dfn activation) out-unactivated)))

; maps list of layers into list of layer deriratives, where each layer derirative is a list of its neuron deriratives 
(define (backpropagation layers inputs targets activation)
  (let* ([output-unactivated (layer-out-unactivated (first layers) inputs)]
        [output (layer-out (first layers) inputs (activation-fn activation))])
    (if (empty? (rest layers)) 
        (list (map (lambda (out target) (d-lastlayer out target activation)) output-unactivated targets))
        (let ([next-layer-d (backpropagation (rest layers) output targets activation)])
          (cons (map (lambda(index out)
                       (d-innerlayer index 
                                     out 
                                     (first next-layer-d)
                                     (first (rest layers))
                                     activation)) 
                     (range (length output-unactivated)) 
                     output-unactivated) 
                next-layer-d)))))

; calculates new weights for the layer.
(define (transform-layer _layer input derirative train-speed momentum)
  (layer (map (lambda(n d)
                (define weight-ds (map (lambda(wd-prev i)
                                  (+ (* (- train-speed) i d) (* momentum wd-prev)))
                                (neuron-wd n)
                                (cons 1 input)))
                (neuron (map (lambda (w wd) (+ w wd)) (neuron-weights n) weight-ds) weight-ds)
                #;(neuron (map (lambda(w i)
                               (+ w (* (- train-speed) i d))) 
                             (neuron-weights n) 
                             (cons 1 input)))) 
              (layer-neurons _layer)
              derirative)))

; calculates new weights for all layers
(define (update-layers layers inputs deriratives train-speed momentum activation-fn)
  (if (empty? layers) '()
      (cons (transform-layer (first layers)
                             inputs
                             (first deriratives)
                             train-speed
                             momentum) 
            (update-layers (rest layers)
                           (layer-out (first layers)
                                      inputs
                                      activation-fn)
                           (rest deriratives)
                           train-speed
                           momentum
                           activation-fn))))

; performs network update for single input vector
(define (train-neural-network-iteration network inputs target train-speed momentum)
  (let* ([layers (neural-network-layers network)]
         [activation (neural-network-activation network)]
         [deriratives (backpropagation layers inputs target activation)]
         [new-layers (update-layers layers inputs deriratives train-speed momentum (activation-fn activation))])
    (neural-network new-layers (neural-network-activation network))))

; performs network update for each input in teaching-data
(define (train-neural-network-epoch network teaching-data train-speed momentum)
  (let train ([network network]
              [data teaching-data])
    (if (empty? data) network
        (train (train-neural-network-iteration network (car (first data)) (cdr (first data)) train-speed momentum) (rest data)))))

; Trains network for `iterations` amount of epochs
(define (train-neural-network network data train-speed momentum max-iterations target-error)
  (let it ([i 0] [network network])
    (define err (neural-network-avg-error network data))
    (if (or (>= i max-iterations) (< err target-error)) (values network i err)
        (begin
          ;(println (~a "Iteration nr: " i))
          ;(println (~a "Error: " (neural-network-err network data)))
        (it (add1 i) (train-neural-network-epoch network data train-speed momentum))))))

; creates a network. Neuron count list -- a list of integers, each telling how many neurons in that layer
(define (create-neural-network inputs-length neuron-count-list activation)
  (let _create ([inputs-l inputs-length] [n-count neuron-count-list] [layers '()])
    (if (empty? n-count) (neural-network (reverse layers) activation)
        (_create (first n-count)
                 (rest n-count)
                 (cons (layer (build-list (first n-count)
                                          (lambda (n)
                                            (neuron (build-list (add1 inputs-l)
                                                                (lambda(n2) (- (/ (random 50) 25) 1)))
                                                    (make-list (add1 inputs-l) 0)))))
                       layers)))
    ))

(define (neural-network-correct-rate network data)
  (let it ([count 0] [correct 0] [data data])
    (if (empty? data) (/ correct count)
        (let* ([input (car (first data))]
               [target (cdr (first data))]
               [output (neural-network-out network input)]
               [max_value (apply max output)]
               [output-sel (map (lambda (x) (if (= x max_value) 1 0)) output)]
               [correct? (equal? target output-sel)])
          (it (add1 count) (if correct? (add1 correct) correct) (rest data))))))

#;(define (neural-network-error network data)
  (define (output-error out target)
    (define max_value (apply max out))
    (foldl (lambda (o t result) (+ result (expt (- t (if (= o max_value) 1 0)) 2))) 0 out target))
  (let it ([count 0] [sum 0] [data data])
    (if (empty? data) (/ sum count)
        (let ([out (neural-network-out network (car (first data)))]
              [target (cdr (first data))])
          (it (add1 count) (+ sum (output-error out target)) (rest data))))))

(define (neural-network-avg-error network data)
  (define (calc-error target out)
    (foldl (lambda (t out result) (+ result (expt (- out t) 2))) 0 target out))
  (let it ([error 0]
           [data data]
           [i 0])
    (if (empty? data)
        (/ error i)
        (it (+ error (calc-error (neural-network-out network (car (first data))) (cdr (first data)))) (rest data) (add1 i)))))

;test
(define (test-case act)
(define nn (create-neural-network 2 (list 2 1) act))
(define data (list (cons (list 1 0) (list 1))
                   (cons (list 0 0) (list 0))
                   (cons (list 0 1) (list 1))
                   (cons (list 1 1) (list 0))
                   (cons (list 0 1) (list 1))
                   (cons (list 1 0) (list 1))
                   (cons (list 1 1) (list 0))
                   (cons (list 0 0) (list 0))))
(define trained-nn (train-neural-network nn data 10000 0.3 0.8))
(println (~a (neural-network-out trained-nn (list 0 0))))
(println (~a (neural-network-out trained-nn (list 0 1))))
(println (~a (neural-network-out trained-nn (list 1 0))))
(println (~a (neural-network-out trained-nn (list 1 1))))
(println (~a trained-nn)))

;(test-case sigmoid-a)
;outputs
;0->2 * 10^(-29)
;1->5 * 10^(-21)
;2->2 * 10^(-31)

;(test-case relu-a)
;outputs
;0 -> ~164
;1 -> ~164
;2 -> ~0

(provide (all-defined-out))
