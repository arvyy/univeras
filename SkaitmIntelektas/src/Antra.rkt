#lang racket
(require "./Neuron.rkt")

(define iteration_count 10000)
(define n 1)

(define test1 '(((0 0)(0 1)(1 0)(1 1)) ; ivestys
                (0 0 0 1))) ; atitinkamai norimos klases

(define test2 '(((0 0 0)(0 0 1)(0 1 0)(0 1 1)(1 0 0)(1 0 1)(1 1 0)(1 1 1))
                (0 0 0 0 0 0 0 1)))

(define (train init_neuron input target min_error)
  (define it 0)
  (define trained_neuron (neuron-train init_neuron 
                                       input 
                                       target 
                                       iteration_count 
                                       min_error 
                                       n
                                       #:epoch-listener (lambda(i e n)
                                                          (set! it i))))
  (display "Iteration Count: ")
  (displayln it)
  (display "Bias: ")
  (displayln (neuron-bias trained_neuron))
  (displayln "Weights: ")
  (for-each (lambda(w)
              (display w)
              (display " ")) 
            (neuron-weights trained_neuron))
  (displayln "")
  )

(define (train-step input target)
  (train (neuron unitStep (make-list (length (car input)) 0) 0) input target 0))

(define (train-sigm input target)
  (train (neuron (sigmoid 10) (make-list (length (car input)) 0) 0) input target 0.00001))


(displayln "x1 AND x2 su laiptine funkcija")
(train-step (car test1) (cadr test1))
(displayln "")

(displayln "x1 AND x2 su sigmoidine funkcija")
(train-sigm (car test1) (cadr test1))
(displayln "")


(displayln "x1 AND x2 AND x3 su laiptine funkcija")
(train-step (car test2) (cadr test2))
(displayln "")

(displayln "x1 AND x2 AND x3 su sigmoidine funkcija")
(train-sigm (car test2) (cadr test2))
(displayln "")
