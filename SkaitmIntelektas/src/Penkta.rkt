#lang racket

(require csv-reading)
(require "SOM.rkt")

(define (read-data file)
  (define in (open-input-file file))
  (define rez (csv-map (lambda(strings)
                         (list (string->number (first strings))
                               (string->number (second strings))
                               (string->number (third strings)))) in))
  (close-input-port in)
  rez)

(define (export-som-rezult size som-out out-error filename)
  (call-with-output-file filename #:exists 'replace
    (lambda (out)
      (displayln (~a "<html><head></head><body>" out-error "<table>") out)
      (for-each (lambda(i)
                  (displayln "<tr>" out)
                  (for-each (lambda(j)
                              (display "<td style=\"border: solid gray;\">" out)
                              (display (string-join (map number->string (set->list (hash-ref som-out (cons i j))))) out)
                              (displayln "</td>" out)) (range size))
                  (displayln "</tr>" out)) (range size))
      (displayln "</table></body></html>" out))))

(define (build-som-set out size)
  (define set (make-hash))
  (for-each (lambda(x)
              (for-each (lambda(y)(hash-set! set (cons x y) (mutable-set))) (range size))) (range size))
  (for-each (lambda(o)
              (define i (som-result-i o))
              (define c (if (< i 50) 0 (if (< i 100) 1 2)))
              (set-add! (hash-ref set (cons (som-result-x o) (som-result-y o))) c)) out)
  set)

(define (test-som size inputs epoch outputfile)
  (let* ([neurons (create-neurons size size 3)]
         [trained-neurons (teach-neurons neurons inputs epoch)]
         [out (neurons-output trained-neurons inputs)]
         [out-error (neurons-output-error trained-neurons inputs)]
         [som-set (build-som-set out size)]
         [filename (~a outputfile "_size" size "_ep" epoch ".html")])
    (println out)
    (println som-set)
    (export-som-rezult size som-set out-error filename)))


(define inputs (read-data "../Penktas/iris.data"))
(test-som 5 inputs 100 "../Penktas/rez")
(test-som 5 inputs 1 "../Penktas/rez")
(test-som 10 inputs 100 "../Penktas/rez")
(test-som 10 inputs 1 "../Penktas/rez")

