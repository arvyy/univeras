#lang racket

(define-struct neuron (x y weights) #:transparent )

(define-struct som-result (x y i) #:transparent )

(define (create-neurons x y inputs-length)
  (define (rand-weights)
    (build-list inputs-length (lambda(i)(/ (random 100) 100))))
  (flatten (build-list x (lambda(i)(build-list y (lambda(j)(neuron i j (rand-weights))))))))

; euklido atstumas
(define (eukl-dist v1 v2)
  (sqrt (foldl (lambda(i j result) (+ result (expt (- i j) 2))) 0 v1 v2)))

; ciampiono pozicijos radimas
(define (find-champ-pos neurons input)
  (let it ([smallest -1] [neurons neurons] [best (cons -1 -1)])
    (if (empty? neurons) best
        (let* ([n (first neurons)]
               [dist (eukl-dist (neuron-weights n) input)]
               [is-new-smallest (or (= smallest -1) (< dist smallest))]
               [new-smallest (if is-new-smallest dist smallest)]
               [new-best (if is-new-smallest (cons (neuron-x n) (neuron-y n)) best)]
               )
          (it new-smallest (rest neurons) new-best)))))

; mokymo parametras
(define (a t) (/ 1 (add1 t)))

(define (o size max-it-number it-number)
  (* (/ size 2) (exp (/ (- it-number) max-it-number))))

; kaimynystes funkcija
(define (gauss-h champ-pos neur it-number max-it-number network-width)
  (let* ([c_vec (list (car champ-pos) (cdr champ-pos))]
         [n_vec (list (neuron-x neur) (neuron-y neur))]
         [dist (eukl-dist c_vec n_vec)]
         [o (o (/ network-width 2) max-it-number it-number)]
         [a (a it-number)]
         [nom (- (expt dist 2))]
         [denom (* 2 (expt o 2))])
    (* a (exp (/ nom denom)))))

; neurono atnaujinimas
(define (update-neuron neur input champ-pos it-nr max-it-number network-width)
  (define h (gauss-h champ-pos neur it-nr max-it-number network-width))
  (define (update-weight w i)(+ w (* h (- i w))))
  (neuron (neuron-x neur)
          (neuron-y neur)
          (map update-weight (neuron-weights neur) input)))

; visu neuronu vienos iteracijos atnaujinimas
(define (teach-neurons-it neurons input it-nr max-it-number network-width)
  (let ([champ-pos (find-champ-pos neurons input)])
    (map (lambda(neur) 
           (update-neuron neur input champ-pos it-nr max-it-number network-width)) neurons)))

(define (teach-neurons neurons inputs epoch-count)
  (define max-it-number (* epoch-count (length inputs)))
  (define network-width (add1 (foldl (lambda(n result)
                                       (if (> (neuron-x n) result) (neuron-x n) result)) 0 neurons)))
  (let it ([neurons neurons]
           [current_inputs (shuffle inputs)]
           [it-nr 0])
    (define current_inputs* (if (empty? current_inputs) (shuffle inputs) current_inputs))
    (if (>= it-nr max-it-number) neurons
        (it (teach-neurons-it neurons (first current_inputs*) it-nr max-it-number network-width) (rest current_inputs*) (add1 it-nr)))))

(define (neurons-output neurons inputs)
  (map (lambda(in index)
         (let ([champ-pos (find-champ-pos neurons in)])
           (som-result (car champ-pos) (cdr champ-pos) index))) inputs (range (length inputs))))

(define (neurons-output-error neurons inputs)
  (define (find-by-pos pos)
    (let it ([neurons neurons])
      (define n (first neurons))
      (if (and (= (neuron-x n) (car pos))
               (= (neuron-y n) (cdr pos)))
          n
          (it (rest neurons)))))
  (define (output-err in)
    (let* ([champ-pos (find-champ-pos neurons in)]
           [champ-neur (find-by-pos champ-pos)]
           [dist (eukl-dist (neuron-weights champ-neur) in)])
      ;(println (~a "In: " in " Neur: " champ-neur " Dist: " dist))
      dist
      ))
  ( / (foldl (lambda (in result)
               (println result)
               (+ result (output-err in))) 0 inputs) (length inputs)))

(define (test-gauss)
  (define champ-pos (cons 2 2))
  (for-each (lambda(i)
              (define n (neuron 2 (+ 2 i) (list)))
              (for-each (lambda(j)
                          (println (~a "Neuron: " n ", It: " (add1 j) ", H: " (gauss-h champ-pos n (add1 j))))) (range 5))) (range 5)))

;(test-gauss)

(provide (all-defined-out))