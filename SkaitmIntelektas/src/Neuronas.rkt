#lang racket

; slenkstine funkcija
(define (unitStep x)
  (if (< x 0) 0 1))

; sigmoidine funkcija
(define (sigmoid b)
  (lambda (x)
    (/ 1
       (+ 1
          (exp (* x
                  (- 0 b)))))))

; ivestys -- sarasas poru (reiksme ir svoris). Bias -- slenkstis.
(define (neuron func inputs bias)
  (func (apply +
                 (map
                  (lambda (x)(* (car x) (cdr x)))
                  (cons (cons 1 bias) inputs)))))

; sarasa reiksmiu ir sarasa svoriu sujungia i viena sarasa atitinkamu poru
(define (createInputs values weights)
  (define (iter v w res)
    (if (empty? v)
        res
        (iter
         (rest v)
         (rest w)
         (cons (cons (first v) (first w)) res))))
  (iter values weights (list)))

; ivestys
(define testCase1 (list -0.2 0.5))
(define testCase2 (list 0.2 -0.5))
(define testCase3 (list 0.8 -0.8))
(define testCase4 (list 0.8 0.8))

; parinkti svoriai
(define weights (list 1 0))
(define bias -0.5)

; sigmoidine funkcija su b=50
(define sigmoid50 (sigmoid 50))

; atspausidnimo funkcija
(define (printTest testCase)
  (writeln testCase)
  (write 'step= )
  (writeln (neuron unitStep (createInputs testCase weights) bias))
  (write 'sigmoid= )
  (writeln (neuron sigmoid50 (createInputs testCase weights) bias))
  (writeln '~~~)
)

(printTest testCase1)
(printTest testCase2)
(printTest testCase3)
(printTest testCase4)