#lang racket
(require "./Neuron.rkt")

(define input '((0 0 0)(0 0 1)(0 1 0)(0 1 1)(1 0 0)(1 0 1)(1 1 0)(1 1 1)))
(define target '(0 0 0 0 0 0 0 1))

(define iteration_count 10000)
(define min_error 0)
(define ns '(0.1 0.5 1))
(define init_neuron (neuron (sigmoid 2) '(0 0 0) 0))

(define (getErrData n)
  (let ([errors '()])
	(neuron-train init_neuron 
				  input 
				  target 
				  iteration_count 
				  min_error 
				  n
				  #:epoch-listener (lambda(i e n)
									 (set! errors (cons e errors))))
	(reverse errors)))

(define data (apply map (lambda (i j k)
						  (list i j k)) 
					(map (lambda(n)
						   (getErrData n))
						 ns)))

(call-with-output-file "AntraGraphData.csv" #:exists 'replace
					   (lambda (out)
						 (displayln "error-0.1;error-0.5;error-1" out)
						 (for-each (lambda(pair)
									 (displayln (string-append (number->string (first pair)) 
															   ";" 
															   (number->string (second pair))
															   ";"
															   (number->string (third pair)))
												out))
								   data)))
