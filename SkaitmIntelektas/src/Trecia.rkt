#lang racket

(require "./NeuralNetwork.rkt")

(define (read-train-data file digit-class train-data test-data)

  (define (read-list in number)
	(let it ([number number] [result '()])
	  (if (>= 0 number) result
		(it (sub1 number) (cons (/ (read-byte in) 256) result)))))
  (define in (open-input-file file #:mode 'binary))
  (define target (build-list 10 (lambda (x) (if (= x digit-class) 1 0))))
  (define bytes-per-entry (* 28 28))
  (define-values (train test) (let it ([number 500] [train train-data] [test test-data])
                   (if (>= 0 number) (values train test)
                       (it (sub1 number) (cons (cons (read-list in bytes-per-entry) target) train)
                           (cons (cons (read-list in bytes-per-entry) target) test)))))
  (close-input-port in)
  (values train test))

(define-values (train-data test-data) (let it ([i 0] [train '()] [test '()])
			   (if (> i 9) (values (shuffle train) (shuffle test))
                               (let-values ([(train test) (read-train-data (string-append "../Trecias/data" (number->string i)) i train test)])
                                 (it (add1 i) train test)))))

(define (perform-train-case neurons-count learning-speed momentum activation max-iterations target-error)
  (define network (create-neural-network (* 28 28) neurons-count activation))
  (define start-time (current-seconds))
  (define-values (trained-network iterations error) (train-neural-network network train-data learning-speed momentum max-iterations target-error))
  (define train-duration (- (current-seconds) start-time))
  (define acc-train (neural-network-correct-rate trained-network train-data))
  (define acc (neural-network-correct-rate trained-network test-data))
  (define avg-out-sum (neural-network-out-sum trained-network test-data))
  ;(define err (neural-network-error trained-network train-data))
  (println (~a "Neurons by layer: " neurons-count
                          ", Iterations: " iterations
                          ", Error: " error 
                          ", Learning speed: " learning-speed
                          ", Momentum: " momentum
                          ", Accuracy (train): " acc-train
                          ", Accuracy (test): " acc
                          ", Avg. out sum: " avg-out-sum
                          ", Train duration: " train-duration)))


;(perform-train-case (list 10 10) 0.3 0.7 sigmoid-a 0 0.01)
(perform-train-case (list 10 10) 0.3 0.7 sigmoid-a 1 0.01)
;(perform-train-case (list 10 10) 0.3 0.7 sigmoid-a 10 0.01)
(perform-train-case (list 16 10) 0.3 0.7 sigmoid-a 5 0.01)
;(perform-train-case (list 10 10) 0.01 0.9 sigmoid-a 10 0.01)