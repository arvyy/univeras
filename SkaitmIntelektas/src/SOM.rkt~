#lang racket

(define-struct neuron (x y weights) #:transparent )

(define (create-neurons x y inputs-length)
  (define (rand-weights)
	(build-list inputs-length (lambda(i)(rnd))))
  (map (lambda(x y) (neuron x y (rand-weights))) (range x) (range y)))

(define (eukl-dist v1 v2)
  (sqrt (foldl (lambda(i j)
				 (+ (expt i 2) (expt j 2))) 0 v1 v2)))

(define (find-champ-pos neurons input)
  (let it ([smallest -1] [neurons neurons] [best (pair -1 -1)])
	(if (empty? neurons) best
	  (let* ([n (first neurons)]
			 [dist (eukl-dist (neuron-weights n) input)]
			 [is-new-smallest (or (= smallest -1) (< dist smallest))]
			 [new-smallest (if is-new-smallest dist smallest)]
			 [new-best (if is-new-smallest (pair (neuron-x n) (neuron-y n)) best)]
			 )
	  	(it new-smallest (rest neurons) new-best)))))

; mokymo parametras
(define (a t) (/ 1 t))

; atstumo funkcija
(define (o_fact o_0 lam)
  (lambda(t)
	(* o_0 (exp (/ (- t) (lam))))))

(define o (o_fact 1 1))

(define (gauss-h champ-pos neur it-number)
  (exp (/ (- (expt (eukl-dist (list (first champ-pos) (second champ-pos)) (list (neuron-x neur) (neuron-y neur))) 2)) (* 2 (expt (o it-number) 2)))))


(define (teach-neurons-it neurons input it-nr)
  (let ([champ-pos (find-champ-pos neurons input)])
  	(map (lambda(neur) 
		   (neuron (neuron-x neur) 
				   (neuron-y neur) 
				   (map (lambda(w i)
						  (+ w (* (- i w) (a it-number) (gauss-h champ-pos neur it-number)))) (neuron-weights neur) input))) neurons)))

(define (teach-neurons-epoch neurons inputs )
  (let it ([neurons neurons]
		   [inputs inputs]
		   [it-nr 0])
	(if (empty? inputs) neurons
	  	(it (teach-neurons-it neurons (first inputs)) (rest inputs) (add1 it-nr)))))
