#lang racket
(require "./Neuron.rkt")

(struct neuron-layer (neurons)
  #:methods gen:neuron-output
  [(define (neuron-calc layer inputs)
     (map (lambda(n)
            (neuron-calc n inputs)) 
          (neuron-layer-neurons layer)))])

(struct neuron-network (layers)
  #:methods gen:neuron-output
  [(define (neuron-calc nn inputs)
     (let nn-calc ([inputs inputs]
                   [layers (neuron-network-layers nn)])
       (if (empty? layers) 
           inputs
           (nn-calc (neuron-calc (first layers) inputs) (rest layers)))))])

(define (create-neuron-network inputs-length neuron-count-list fn)
  (let _create ([inputs-l inputs-length] [n-count neuron-count-list] [layers '()])
    (if (empty? n-count) (neuron-network layers)
        (_create (first n-count)
                 (rest n-count)
                 (cons (neuron-layer (build-list (first n-count)
                                                 (lambda (n)
                                                   (neuron fn (build-list inputs-l
                                                                          (lambda(n2) 0))
                                                           0))))
                       layers)))
    ))

#;(define (neuron-network-error nn input target)
  (foldl (lambda (in tar result)
           (+ result (expt (- in tar) 2)))
         (neuron-calc nn input) target))

#;(define (nn-error output target)
  (foldl (lambda (out tar result)
           (+ result (* 0.5 (expt (- out tar) 2))))
         output target))

