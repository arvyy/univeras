#lang racket

; slenkstine funkcija
(define (unitStep x)
  (if (< x 0)
      0
      1))

; sigmoidine funkcija
(define (sigmoid b)
  (lambda (x)
    (/ 1
       (+ 1
          (exp (* x
                  (- 0 b)))))))

; neuronas - aktyvacijos funkcija, svoriai, slenkstis
(struct neuron (func weights bias) #:transparent )

; klases skaiciavimo funkcija
; neur -- neuronas
; inputs -- ivestys
(define (neuron-calc neur inputs)
  (define (inner _w _in sum)
    (if (empty? _w)
        sum
        (inner (rest _w)
               (rest _in)
               (+ sum
                  (* (first _w)
                     (first _in))))))
  ((neuron-func neur) (inner (neuron-weights neur)
                             inputs
                             (neuron-bias neur))))

; mokymo iteracija - grazina nauja neurona po pokycio su vienu iejimo vektorium
; neur -- ivesties neuronas
; input -- ivesties vektorius
; target -- norima klase
(define (neuron-train-iteration neur input target change-mult)
  (define (new-weight weight c t x)
    (+ weight (* change-mult (- t c) x)))
  (define (new-weights weights c t xs processed-weights)
    (if (empty? weights)
        processed-weights
        (new-weights (rest weights)
                     c
                     t
                     (rest xs)
                     (cons (new-weight (first weights)
                                       c
                                       t
                                       (first xs))
                           processed-weights))))
  (define current (neuron-calc neur input))
  (if (eq? current target)
      neur
      (neuron (neuron-func neur)
              (reverse (new-weights (neuron-weights neur)
                                    current
                                    target
                                    input '()))
              (new-weight (neuron-bias neur)
                          current
                          target
                          1)))
  )

; mokymo epocha - grazina nauja neurona po pokycio su visais iejimo vektoriais
; neur -- ivesties neuronas
; inputs -- vektoriu aibe
; taget-values -- norimos klases vektoriu aibems
(define (neuron-train-epoch neur inputs target-values change-mult)
  (if (empty? inputs)
      neur
      (neuron-train-epoch (neuron-train-iteration neur
                                                  (first inputs)
                                                  (first target-values)
                                                  change-mult)
                          (rest inputs)
                          (rest target-values)
                          change-mult)))

; apskaiciuoja neurono paklaida nuo norimu reiksmiu.
; neur -- neuronas
; inputs -- sarasas ivesties vektoriu
; target-values -- norimos klases vektoriu aibems
(define (neuron-error neur inputs target-values)
  (define (inner-sum in t partial-sum)
    (if (empty? in)
        partial-sum
        (inner-sum (rest in)
                   (rest t)
                   (+ partial-sum
                      (expt (- (first t)
                               (neuron-calc neur (first in)))
                            2)))))
  (* 0.5
     (inner-sum inputs
                target-values
                0)))

; apmoko neurona
; neur -- pradinis neuronas (pradiniai svoriai)
; inputs -- ivesties vektoriu sarasas
; target-values -- norimos reiksmes
; max-iterations -- maksimalus epochu skaicius
; min-error -- minimali paklaida
; change-mult -- apmokymo greitis (n)
(define (neuron-train neur inputs target-values max-iterations min-error change-mult #:epoch-listener [listener void])
  (define (neuron-train-inner _neur iteration)
    (define error (neuron-error _neur inputs target-values))
    (listener iteration error _neur)
    (if (or (< max-iterations iteration) (<= error min-error))
        _neur
        (neuron-train-inner (neuron-train-epoch _neur
                                                inputs
                                                target-values
                                                change-mult)
                            (+ iteration 1))))
  (neuron-train-inner neur 0))

(provide (struct-out neuron) unitStep sigmoid neuron-train)
