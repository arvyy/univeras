<CsoundSynthesizer>
<CsOptions>
</CsOptions>
<CsInstruments>

sr = 44100
ksmps = 128
nchnls = 1
0dbfs = 1.0

instr 1

kbase = 440
kexponent = p4 / 12
kchange pow 2, kexponent
kfreq = kbase * kchange

kamplfreq = 0.5 / p3
kampl oscil p5, kamplfreq, 3, 0.25
kampl = (kampl + p5) / 2

asiglow oscil kampl, kfreq, 2
asighigh oscil kampl, kfreq, 1

asig ntrpol asiglow, asighigh, p4, -9, 24


outs asig
endin

</CsInstruments>
<CsScore>
;garsas aukstai
f1 0 16384 10 1 0.25 0.0125
;garsas zemai
f2 0 16384 10 0.5 1 1 0.01

;amplitudes banga
f3 0 16384 10 1

;instr, pradzia, trukme, nata, garsumas
i	1	0		0.5		3	.6
i	.	0.5		.		5	.
i	.	1		.		7	.
i	.	1.5		.		3	.
i	.	2		0.33	10	.
i	.	2.33	0.17	8	.
i	.	2.5		0.33	7	.
i	.	2.83	1.17	2	.

i	.	4		0.5		2	0.4
i	.	4.5		0.5		3	.
i	.	5		0.33	5	0.5
i	.	5.33	0.17	2	.
i	.	5.5		0.33	6	0.6
i	.	5.83	0.17	2	.
i	.	6		0.5		7	0.7
i	.	6.5		0.33	3	0.5
i	.	6.83	1.17	0	0.4

i	.	8		0.5		-2	.
i	.	8.5		0.5		0	.
i	.	9		0.5		2	.
i	.	9.5		0.5		-2	.
i	.	10		0.25	3	0.5
i	.	10.25	0.5		5	0.4
i	.	10.75	1.25	7	0.5
i	.	12		0.25	7	0.6
i	.	12.25	0.5		10	0.5
i	.	12.75	1.25	12	0.6
i	.	14		0.25	12	0.7
i	.	14.25	0.5		15	.
i	.	14.75	0.25	10	.
i	.	15		1		15	.
	
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
