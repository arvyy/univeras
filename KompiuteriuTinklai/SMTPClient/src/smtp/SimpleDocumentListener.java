package smtp;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

@FunctionalInterface
public interface SimpleDocumentListener extends DocumentListener {
    
	void textUpdate(DocumentEvent e);

    @Override
    default void insertUpdate(DocumentEvent e) {
        textUpdate(e);
    }
    @Override
    default void removeUpdate(DocumentEvent e) {
        textUpdate(e);
    }
    @Override
    default void changedUpdate(DocumentEvent e) {
        textUpdate(e);
    }
}
