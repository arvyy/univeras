package smtp;

import java.awt.Dimension;

import javax.swing.*;

@SuppressWarnings("serial")
public class SMTPClientView extends JFrame {
	private JTextField to, subject;
	private JTextArea message;
	private JButton send;
	
	public SMTPClientView() {
		JPanel form = new JPanel();
		Box hbox;
		BoxLayout layout = new BoxLayout(form, BoxLayout.PAGE_AXIS);
		form.setLayout(layout);
		form.setPreferredSize(new Dimension(400, 300));
		
		hbox = Box.createHorizontalBox();
		hbox.add(Box.createHorizontalStrut(8));
		hbox.add(new JLabel("to"));
		hbox.add(Box.createHorizontalStrut(4));
		hbox.add(to = new JTextField());
		to.setMaximumSize(new Dimension(Integer.MAX_VALUE, to.getPreferredSize().height) );
		hbox.add(Box.createHorizontalStrut(8));
		form.add(hbox);
		
		form.add(Box.createVerticalStrut(16));
		
		hbox = Box.createHorizontalBox();
		hbox.add(Box.createHorizontalStrut(8));
		hbox.add(new JLabel("subject"));
		hbox.add(Box.createHorizontalStrut(4));
		hbox.add(subject = new JTextField());
		subject.setMaximumSize(new Dimension(Integer.MAX_VALUE, subject.getPreferredSize().height) );
		hbox.add(Box.createHorizontalStrut(8));
		form.add(hbox);
		
		form.add(Box.createVerticalStrut(16));
		
		hbox = Box.createHorizontalBox();
		hbox.add(Box.createHorizontalStrut(8));
		JScrollPane pane = new JScrollPane(message = new JTextArea());
		pane.setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));
		hbox.add(pane);
		hbox.add(Box.createHorizontalStrut(8));
		form.add(hbox);
		
		form.add(Box.createVerticalStrut(16));
		
		hbox = Box.createHorizontalBox();
		hbox.add(Box.createHorizontalStrut(8));
		hbox.add(send = new JButton("send"));
		hbox.add(Box.createGlue());
		form.add(hbox);

		form.add(Box.createVerticalStrut(16));
		
		add(form);
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public void showErrorMessage(String message) {
		JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	public void showSuccessMessage(String message) {
		JOptionPane.showMessageDialog(this, message, "Success", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public JTextField getTo() {
		return to;
	}
	
	public JTextField getSubject() {
		return subject;
	}

	public JTextArea getMessage() {
		return message;
	}
	
	public JButton getSend() {
		return send;
	}
	
}
