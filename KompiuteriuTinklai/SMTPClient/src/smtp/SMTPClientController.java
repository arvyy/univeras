package smtp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Base64;
import javax.net.ssl.SSLSocketFactory;

public class SMTPClientController {
	
	private SMTPClientView view;
	
	private final String fromHost = "mif.stud.vu.lt",
						 fromName = "arvydas.silanskas@" + fromHost,
						 username = "s1510773",
						 password64 = "TGFpc3Zhbm9yaXMxLg==",
						 smtpServer = "smtps.vu.lt";
	private final int smtpServerPort = 465;
	
	public SMTPClientController(SMTPClientView view) {
		this.view = view;
		view.getSend().addActionListener(l -> sendEmail());
		SimpleDocumentListener sdl = e -> fieldChanged();
		view.getTo().getDocument().addDocumentListener(sdl);
		view.getMessage().getDocument().addDocumentListener(sdl);
		fieldChanged();
	}
	
	public SMTPClientController(){}
	
	private void fieldChanged() {
		boolean disable = view.getTo().getText().isEmpty() ||
				view.getMessage().getText().isEmpty();
		view.getSend().setEnabled(!disable);
	}
	
	private void sendEmail() {
		String to = view.getTo().getText();
		String message = view.getMessage().getText();
		String subject = view.getSubject().getText();
		sendEmail(to, subject, message);
	}
	
	public void sendEmail(String to, String subject, String message) {
		new Thread(()->{
			try {
				String lines[] = message.split("\n");
				for (int i = 0; i < lines.length; i++) {
					if (lines[i].startsWith("."))
						lines[i] = ".".concat(lines[i]);
				}
				Socket socket = SSLSocketFactory.getDefault().createSocket(smtpServer, smtpServerPort);
				PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				System.out.println(in.readLine());
				out.println(String.format("helo %s", fromHost));
				System.out.println(in.readLine());
				out.println("AUTH LOGIN");
				System.out.println(in.readLine());
				out.println(Base64.getEncoder().encodeToString(username.getBytes()));
				System.out.println(in.readLine());
				out.println(password64);
				System.out.println(in.readLine());
				out.println(String.format("mail from: %s", fromName));
				System.out.println(in.readLine());
				out.println(String.format("rcpt to: %s", to));
				System.out.println(in.readLine());
				out.println("data");
				System.out.println(in.readLine());
				out.println(String.format("FROM: %s", fromName));
				out.println(String.format("SUBJECT: %s", subject));
				out.println(String.format("TO: %s", to));
				out.println();
				for (String line : lines) {
					out.println(line);
				}
				out.println(".");
				System.out.println(in.readLine());
				socket.close();
				view.showSuccessMessage("Mail successfully sent");
				view.getMessage().setText("");
				view.getSubject().setText("");
				view.getTo().setText("");
			} catch (Exception e) {
				e.printStackTrace();
				view.showErrorMessage(e.getMessage());
			}
		}).start();
	}
	
}
