package smtp;

import javax.swing.SwingUtilities;

public class SMTPClientLauncher {
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new SMTPClientController(new SMTPClientView());
		});
	}
	
}
