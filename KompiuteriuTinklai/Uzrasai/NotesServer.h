#ifndef NOTESSTRUCTS_H
#define NOTESSTRUCTS_H

#define MAX_MESSAGES_COUNT 10
#define MAX_MESSAGES_LENGTH 1000
#define MAX_PASSWORD_LENGTH 20
#define MAX_NAME_LENGTH 20

struct User {
	char name[20];
	char password[20];
	char messages[1000][10];
	int messagesCount;
};


struct User* handleLogin(char* reqB, int reqL, char* respB, int* respL);
void handleCreateUser(char* reqB, int reqL, char* respB, int* respL);
void handleGetNotes(struct User* user, char* reqB, int reqL, char* respB, int* respL);
void handleDeleteNote(struct User* user, char* reqB, int reqL, char* respB, int* respL);
void handleCreateNote(struct User* user, char* reqB, int reqL, char* respB, int* respL);
void handleLogout(struct User** user, char* reqB, int reqL, char* respB, int* respL);
void invalidRequest(char* respB, int* respL);
void handleClientConnection(int socket_desc);

#endif

