#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include "NotesServer.h"
#include "NotesMessages.h"
#include "DBRepository.h"

#define BUFFER_L 11000


int main(int argc, char *argv[]) {
	int sockfd, newsockfd, portno;
	socklen_t clilen;
	char buffer[BUFFER_L];
	struct sockaddr_in serv_addr, cli_addr;
	if (argc < 2) {
		printf("Usage: %s port\n", argv[0]);
		return 1;
	}
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		printf("Failure to create socket\n");
		return 1;
	}
	bzero((char *) &serv_addr, sizeof(serv_addr));
	portno = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if (bind(sockfd, (struct sockaddr *) &serv_addr,
				sizeof(serv_addr)) < 0) {
		printf("Failure to bind socket\n");
		return 1;
	}
	printf("Listening...\n");
	listen(sockfd,5);
	clilen = sizeof(cli_addr);
	while (1) {
		newsockfd = accept(sockfd, 
				(struct sockaddr *) &cli_addr, 
				&clilen);
		printf("Client(%d) connected\n", newsockfd);
		int childId = fork();
		if (childId == -1) {
			//fork klaida
			return 1;
		}
		if (childId == 0) {
			//vaikas
			handleClientConnection(newsockfd);
			close(newsockfd);
			printf("Client(%d) disconnected\n", newsockfd);
			return 0;
		}
	}
	close(sockfd);
}

void invalidRequest(char* responseBuffer, int* length) {
	*responseBuffer = BAD_REQUEST;
	*length = 1;
}

void handleCreateUser(
		char* requestBuffer, 
		int requestLength, 
		char* responseBuffer,
		int* responseLength)
{
	char* name = strtok(requestBuffer + 1, "|");
	char* password = strtok(NULL, "\0");
	if (name == NULL || password == NULL) {
		invalidRequest(responseBuffer, responseLength);
	} else {
		createUser(name, password, responseBuffer);
		*responseLength = 1;
	}

}

struct User* handleLogin(
		char* requestBuffer, 
		int requestLength, 
		char* responseBuffer,
		int* responseLength)
{
	char* name = strtok(requestBuffer + 1, "|\0");
	char* password = strtok(NULL, "|\0");
	if (name == NULL || password == NULL) {
		invalidRequest(responseBuffer, responseLength);
		return NULL;
	} else {
		*responseLength = 1;
		printf("logging in: %s, %s\n", name, password);
		return login(name, password, responseBuffer);
	}
}

void handleGetNotes(
		struct User* user,
		char* requestBuffer, 
		int requestLength, 
		char* responseBuffer,
		int* responseLength)
{
	*responseBuffer = SUCCESS;
	int l = 1;
	for (int i = 0; i < user->messagesCount; i++) {
		int ml = strlen(user->messages[i]);
		strcpy((responseBuffer + l), user->messages[i]);
		*(responseBuffer + l + ml) = '|';	
		l += ml + 1;
	}
	if (l != 1) {
		//nutrinam trailinanti "|"
		*(responseBuffer + l) = '\0';
		l -= 1;
	}
	*responseLength = l;
}

void handleCreateNote(
		struct User* user,
		char* requestBuffer, 
		int requestLength, 
		char* responseBuffer,
		int* responseLength)
{
	//trinam visus "|"
	
	if (requestLength < 2) {
		invalidRequest(responseBuffer, responseLength);
		return;
	}

	int i = 1;
	while (*(requestBuffer + i)) {
		if (*(requestBuffer + i) == '|')
			*(requestBuffer + i) = '/';
		i++;
	}
	createNote(user, requestBuffer + 1, responseBuffer);
	*responseLength = 1;
}


void handleDeleteNote(
		struct User* user,
		char* requestBuffer, 
		int requestLength, 
		char* responseBuffer,
		int* responseLength)
{
	*responseLength = 1;
	deleteNote(user, *(requestBuffer + 1), responseBuffer);	
}


void handleLogout(
		struct User** user,
		char* requestBuffer, 
		int requestLength, 
		char* responseBuffer,
		int* responseLength)
{
	*responseBuffer = SUCCESS;
	*responseLength = 1;
	if (*user != NULL) {
		free(*user);
		*user = NULL;
	}
}



void handleClientConnection(int sock_desc) {
	int reqL;
	char reqB[BUFFER_L];
	int _respL = 0;
	int* respL = &_respL;
	char respB[BUFFER_L];
	struct User* user = NULL;
	int exit = 0;
	while (1) {
		bzero(reqB, BUFFER_L);
		bzero(respB, BUFFER_L);
		*respL = 0;
		reqL = read(sock_desc, reqB, BUFFER_L);
		if (reqL < 0) return;
		switch (*reqB) {
			case LOGIN : if (user != NULL) free(user);
						 user = handleLogin(reqB, reqL, respB, respL);
						 break;
			case CREATE_USER : handleCreateUser(reqB, reqL, respB, respL);
							   break;
			case GET_NOTES : handleGetNotes(user, reqB, reqL, respB, respL);
							 break;
			case DELETE_NOTE : handleDeleteNote(user, reqB, reqL, respB, respL);
							   break;
			case CREATE_NOTE : handleCreateNote(user, reqB, reqL, respB, respL);
							   break;
			case LOGOUT : handleLogout(&user, reqB, reqL, respB, respL);
						  break;
			case EXIT : exit = 1;
						respB[0] = SUCCESS;
						*respL = 1;
						break;
			default : invalidRequest(respB, respL);
					  break;
		}
		write(sock_desc, respB, *respL);
		if (exit)
			break;
	}
	if (user != NULL) {
		free(user);
	}
}
