#ifndef DBREPOSITORY_H
#define DBREPOSITORY_H

#include "NotesServer.h"

#define USER_FILE_LOCATION "./users/"

void createUser(char* name, char* password, char* responseCode);
struct User* login(char* name, char* password, char* responseCode);
void createNote(struct User* user, char* text, char* responseCode);
void deleteNote(struct User* user, int note_id, char* responseCode);

#endif
