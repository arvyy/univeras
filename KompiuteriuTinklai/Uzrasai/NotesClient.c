#include <netinet/in.h>
#include <stdio.h>
#include <sys/socket.h>
#include<netdb.h> //hostent
#include<arpa/inet.h>
#include <stdlib.h>
#include "NotesMessages.h"
#include <strings.h>
#include <unistd.h>
#include <string.h>


void handleCreateUser(int socket_desc);
void handleLogin(int* loggedIn, int socket_desc);
void handleLogout(int* loggedIn, int socket_desc);
void handleExit(int socket_desc);
void handleNewNote(int* loggedIn, int socket_desc);
void handleReadAllNotes(int* loggedIn, int socket_desc);
void handleDeleteNote(int* loggedIn, int socket_desc);
void printMessage(int respCode);


int main(int argc, char *args[]) {
	int socket_desc;
	struct hostent* he;
	struct in_addr** address_list;
	struct sockaddr_in server;

	if (argc < 3) {
		printf("Use: %s hostname port\n", args[0]);
		return 1;
	}
	he = gethostbyname(args[1]);
	socket_desc = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_desc == -1) {
		printf("Startup fail\n");
		return 1;
	}
	address_list = (struct in_addr**) he->h_addr_list;
	struct in_addr** i;
	for (i = address_list - 1; *(i + 1) != NULL; i++);
	server.sin_addr = **i;
	server.sin_family = AF_INET;
	server.sin_port = htons(atoi(args[2]));
	if (connect (socket_desc, (struct sockaddr*) &server, sizeof(server)) < 0) {
		printf("Connection error\n");
		return 1;
	}
	printf("Connected\n");
	int exit = 0;
	char choice;
	int loggedIn = 0;
	while (!exit) {
		printf("---\n");
		printf("Your actions:\n");
		if (loggedIn) {
			printf("(r)ead all notes\n(d)elete note\n(w)rite new note\n(l)og out\n");
		} else {
			printf("(c)reate user\n(l)ogin\n");
		}
		printf("(e)xit\n\n\nAction: ");
		scanf(" %c", &choice); getchar();
		switch(choice) {
			case 'c' : handleCreateUser(socket_desc); 
					   break; 
			case 'l': if (!loggedIn) handleLogin(&loggedIn, socket_desc);
						  else handleLogout(&loggedIn, socket_desc);
						  break;
			case 'e' : handleExit(socket_desc);
					   exit = 1;
					   break;
			case 'w' : handleNewNote(&loggedIn, socket_desc);
					   break;
			case 'd' : handleDeleteNote(&loggedIn, socket_desc);
					   break;
			case 'r' : handleReadAllNotes(&loggedIn, socket_desc);
					   break;
			default : printf("Unknown command\n");
					  break;
		}
		printf("\n");
	}
	close(socket_desc);
}

void handleDeleteNote(int* loggedIn, int socket_desc) {
	if (!*loggedIn) {
		printf("Log in first!\n");
		return;
	}
	int selection;
	printf("Number to delete: ");
	scanf(" %d", &selection);
	char sel = selection;
	char reqB[2];
	reqB[0] = DELETE_NOTE;
	reqB[1] = sel;
	write(socket_desc, reqB, 2);
	read(socket_desc, reqB, 2);
	if (reqB[0] != SUCCESS) {
		printMessage(reqB[0]);
	}
}	

void handleReadAllNotes(int* loggedIn, int socket_desc) {
	if (!*loggedIn) {
		printf("Log in first!\n");
		return;
	}
	char reqB[1];
	*reqB = GET_NOTES;
	write(socket_desc, reqB, 1);
	char respB[11000];
	bzero(respB, 11000);
	read(socket_desc, respB, 11000);
	if (*respB == SUCCESS) {
		char* line;
		for (int i = 0; 1; i++) {
			if (i == 0) {
				line = strtok(respB + 1, "|\0");
			} else {
				line = strtok(NULL, "|\0");
			}
			if (line == NULL) break;
			printf("%d. %s\n", i, line);
		}
	} else  {
		printMessage(respB[0]);
		return;
	}
}

void handleNewNote(int* loggedIn, int socket_desc) {
	if (!*loggedIn) {
		printf("Log in first!\n");
		return;
	}
	char note[1000];
	printf("Text: ");
	gets(note);
	char reqB[1001];
	reqB[0] = CREATE_NOTE;
	strcpy(reqB + 1, note);
	write(socket_desc, reqB, 2 + strlen(note));
	char respB[1];
	read(socket_desc, respB, 1);
	if (respB[0] != SUCCESS) {
		printMessage(respB[0]);
	}
}

void handleExit(int socket_desc) {
	char buffer[1];
	buffer[0] = EXIT;
	write(socket_desc, buffer, 1);
	read(socket_desc, buffer, 1);
}


void handleLogout(int* loggedIn, int socket_desc) {
	*loggedIn = 0;
	char buffer[50];
	buffer[0] = LOGOUT;
	write(socket_desc, buffer, 1);
	read(socket_desc, buffer, 50);
}

void handleLogin(int* loggedIn, int socket_desc) {
	char requestBuffer[50];
	*requestBuffer = LOGIN;
	printf("user name: ");
	scanf(" %s", requestBuffer + 1);
	int name_l = strlen(requestBuffer + 1);
	if (name_l < 3 || name_l > 19) {
		printf("username must be > 3 and < 19 char long\n");
		return;
	}
	*(requestBuffer + 1 + name_l) = '|';
	printf("user password: ");
	scanf(" %s", requestBuffer + 2 + name_l);
	int pw_l = strlen(requestBuffer + 2 + name_l);
	if (pw_l < 3 || pw_l > 19) {
		printf("password must be > 3 and < 19 char long\n");
		return;
	}
	write (socket_desc, requestBuffer, 3 + name_l + pw_l);
	char respB[10];
	int respL = read(socket_desc, respB, 10);
	if (respL >= 1 && respB[0] == SUCCESS) {
		printf("Successfully logged in\n");
		*loggedIn = 1;
	} else {
		printMessage(respB[0]);
	}
}

void handleCreateUser(int socket_desc) {
	char buffer[50];
	*buffer = CREATE_USER;
	printf("new user name: ");
	scanf(" %s", buffer + 1);
	int name_l = strlen(buffer + 1);
	if (name_l < 3 || name_l > 19) {
		printf("username must be > 3 and < 19 char long\n");
		return;
	}
	*(buffer + 1 + name_l) = '|';
	printf("new user password: ");
	scanf(" %s", buffer + 2 + name_l);
	int pw_l = strlen(buffer + 2 + name_l);
	if (pw_l < 3 || pw_l > 19) {
		printf("password must be > 3 and < 19 char long\n");
		return;
	}
	write (socket_desc, buffer, 3 + name_l + pw_l);
	int respL = read (socket_desc, buffer, 50);
	if (!(respL >= 1 && buffer[0] == SUCCESS)) {
		printMessage(buffer[0]);
	} else {
		printf("User created");
	}
}


void printMessage(int respCode) {
	char* m = NULL;
	switch (respCode) {
		case BAD_REQUEST : m = "Bad request\n";
						   break;
		case NOTE_INDEX_OUT_OF_BOUNDS : m = "Note index out of bounds\n";
										break;
		case USER_NOT_FOUND : m = "User note found\n";
							  break;
		case BAD_PASSWORD : m = "Password doesn't match\n";
							break;
		case USERNAME_TAKEN : m = "User with such name already exists\n";
							  break;
		case MESSAGES_LIMIT_REACHED : m = "Max message limit reached\n";
									  break;
		default : break;
	}
	if (m) {
		printf(m);
	}
}
