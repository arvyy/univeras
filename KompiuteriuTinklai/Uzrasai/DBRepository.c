#include "DBRepository.h"
#include "NotesServer.h"
#include "NotesMessages.h"
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <unistd.h>


void lineFix (char* buffer) {
	buffer[strcspn(buffer, "\n")] = 0;
}

void saveChanges(struct User* user) {
	char filename[50];
	strcpy(filename, USER_FILE_LOCATION);
	strcat(filename, user->name);
	FILE* file = fopen(filename, "w+");
	fprintf(file, "%s\n", user->password);
	for (int i = 0; i < user->messagesCount; i++) {
		fprintf(file, "%s\n", user->messages[i]);
	}
	fclose(file);
}

void createUser(char* name, char* password, char* respCode) {
	FILE* file;
	char filename[50];
	strcpy(filename, USER_FILE_LOCATION);
	strcat(filename, name);
	file = fopen(filename, "r");
	if (file != NULL) {
		fclose(file);
		*respCode = USERNAME_TAKEN;
		return;
	}
	file = fopen(filename, "w");
	fputs(password, file);
	fclose(file);
	*respCode = SUCCESS;
}

struct User* login(char* name, char* password, char* respCode) {
	FILE* file = NULL;
	char filename[50];
	strcpy(filename, USER_FILE_LOCATION);
	strcat(filename, name);
	file = fopen(filename, "r");
	printf("file opened\n");
	if (file == NULL) {
		*respCode = USER_NOT_FOUND;
		printf("not found\n");
		return NULL;
	}
	char buffer[1100];
	//read password
   	fgets(buffer, 19, file);
	lineFix(buffer);
	struct User* user = NULL;
	if (strcmp(password, buffer) == 0) {
		//equal
		user = (struct User*) malloc(sizeof (struct User));
		user->messagesCount = 0;
		strcpy(user->name, name);
		strcpy(user->password, password);
		while (fgets(buffer, 900, file) != NULL) {
			lineFix(buffer);
			int* count = &(user->messagesCount);
			strcpy(user->messages[*count], buffer);
			(*count)++;
		}
		*respCode = SUCCESS;
	} else {
		//not equal
		*respCode = BAD_PASSWORD;
	}
	fclose(file);
	return user;
}
void createNote(struct User* user, char* text, char* responseCode) {
	if (user->messagesCount >= MAX_MESSAGES_COUNT) {
		*responseCode = MESSAGES_LIMIT_REACHED;
		return;
	}
	strcpy(user->messages[(user->messagesCount)++], text);
	saveChanges(user);		
	*responseCode = SUCCESS;
}
void deleteNote(struct User* user, int note_id, char* responseCode) {
	if (user->messagesCount <= note_id || note_id < 0) {
		*responseCode = NOTE_INDEX_OUT_OF_BOUNDS;
		return;
	}
	strcpy(user->messages[note_id], user->messages[user->messagesCount - 1]);
	user->messagesCount -= 1;
	saveChanges(user);
	*responseCode = SUCCESS;
}


