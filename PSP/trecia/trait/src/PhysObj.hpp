#ifndef PHYS_OBJ_HPP
#define PHYS_OBJ_HPP

#include <map>
#include <string>

namespace PSP {
	
	struct Vec {
		Vec();
		Vec(float x, float y);
		float x;
		float y;
	};

	class PhysObj {
		public:
			Vec getSpeed();
			Vec getPos() ;
			void setSpeed(Vec v) ;
			void setPos(Vec v) ;
			void update(float delta) ;
			virtual void update(Vec& pos, Vec& speed, float delta);
		private:
			Vec speed;
			Vec pos;
	};
	
	template<typename T>
	class PhysObjAcc : public T {
		public:
			Vec getAcc() {return acc;}
			void setAcc(Vec v) {acc = v;}
			void update(Vec& pos, Vec& speed, float delta) {
				speed.x += delta * acc.x;
				speed.y += delta * acc.y;
				T::update(pos, speed, delta);
			}
			void update(float delta) {
				T::update(delta);
			}
		private:
			Vec acc;
	};

	template<typename T>
		class PhysObjAirRes : public T {
			public:
				float getResistance() {return res;}
				void setResistance(float f) {res = f;}
				void update(Vec& pos, Vec& speed, float delta) {
					speed.x -= speed.x * res;	
					speed.y -= speed.y * res;	
					T::update(pos, speed, delta);
				}
				void update(float delta) {
					T::update(delta);
				}
			private:
				float res;
		};

}

#endif
