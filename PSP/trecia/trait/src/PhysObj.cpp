#include "PhysObj.hpp"

using namespace PSP;
using namespace std;

Vec::Vec(float _x, float _y) : x(_x), y(_y) {}
Vec::Vec() {}

/******************
* PHYS OBJ
******************/
Vec PhysObj::getSpeed() { return speed; }

Vec PhysObj::getPos() { return pos; }

void PhysObj::setSpeed(Vec v) { speed = v; }

void PhysObj::setPos(Vec v) { pos = v; }

void PhysObj::update(float delta) {
	update(pos, speed, delta);
	pos.x += speed.x * delta;
	pos.y += speed.y * delta;
}

void PhysObj::update(Vec& pos, Vec& speed, float delta){}
