#include <iostream>
#include "PhysObj.hpp"

using namespace std;
using namespace PSP;

int main() {
	auto *obj = new PhysObjAcc<PhysObjAirRes<PhysObj>>();
	obj->setSpeed(Vec(2.0f, 0.0f));
	obj->setAcc(Vec(0.0f, 1.0f));
	obj->setResistance(0.01f);

	for (int i = 0; i < 10; i++) {
		obj->update(1.0f);
		cout << "POS [" << obj->getPos().x << "; " << obj->getPos().y << "]" << endl; 
	}
}
