#ifndef PHYS_OBJ_HPP
#define PHYS_OBJ_HPP

namespace PSP {
	
	struct Vec {
		Vec();
		Vec(float x, float y);
		float x;
		float y;
	};
	
	class PhysObj {
		public:
			virtual Vec getSpeed() = 0;
			virtual Vec getPos() = 0;
			virtual void setSpeed(Vec v) = 0;
			virtual void setPos(Vec v) = 0;
			virtual void update(float delta) = 0;
	};

	class PhysObjImpl : public PhysObj {
		public:
			Vec getSpeed() override ;
			Vec getPos() override ;
			void setSpeed(Vec v) override ;
			void setPos(Vec v) override ;
			void update(float delta) override ;
		private:
			Vec speed;
			Vec pos;
	};

	class PhysObjDecor : public PhysObj {
		public:
			PhysObjDecor(PhysObj* physObj);
			template<typename T>
				inline T* getDecor() {
					T* t = dynamic_cast<T*>(this);
					if (t != nullptr) return t;
					PhysObjDecor* decor = dynamic_cast<PhysObjDecor*>(physObj);
					if (decor == nullptr) return nullptr;
					return decor->getDecor<T>();
				}
			template<typename T>
				inline PhysObj* removeDecor() {
					PhysObjDecor* current = this;
					PhysObjDecor* last = nullptr;
					while (current != nullptr) {
						T* t = dynamic_cast<T*>(current);
						if (t != nullptr) {
							break;
						}
						last = current;
						current = dynamic_cast<PhysObjDecor*>(current->physObj);
					}
					if (current == nullptr) return this;
					if (current == this) return physObj;
					last->physObj = current->physObj;
					return this;
				}
			Vec getSpeed() override ;
			Vec getPos() override ;
			void setSpeed(Vec v) override ;
			void setPos(Vec v) override ;
			void update(float delta) override ;
			PhysObj* getBase();
		protected:
			PhysObj* physObj;
	};

	class PhysObjAcc : public PhysObjDecor {
		public:
			using PhysObjDecor::PhysObjDecor;
			Vec getAcc();
			void setAcc(Vec v);
			void update(float delta) override ;
		private:
			Vec acc;
	};

	class PhysObjAirRes : public PhysObjDecor {
		public:
			using PhysObjDecor::PhysObjDecor;
			float getResistance();
			void setResistance(float f);
			void update(float delta) override ;
		private:
			float res;
	};

	class PhysObjBound : public PhysObjDecor {
		public:
			using PhysObjDecor::PhysObjDecor;
			int getMinX();
			void setMinX(int min);
			void update(float delta) override ;
		private:
			int minX;
	};

}

#endif
