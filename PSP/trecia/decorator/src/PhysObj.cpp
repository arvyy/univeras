#include "PhysObj.hpp"
#include <iostream>

using namespace PSP;

Vec::Vec(float _x, float _y) : x(_x), y(_y) {}
Vec::Vec() {}

/******************
* PHYS OBJ
******************/
Vec PhysObjImpl::getSpeed() { return speed; }

Vec PhysObjImpl::getPos() { return pos; }

void PhysObjImpl::setSpeed(Vec v) { speed = v; }

void PhysObjImpl::setPos(Vec v) { pos = v; }

void PhysObjImpl::update(float delta) {
	pos.x += speed.x * delta;
	pos.y += speed.y * delta;
}

/******************
* PHYS OBJ DECOR
******************/
PhysObjDecor::PhysObjDecor(PhysObj* _physObj) : physObj(_physObj) {}

Vec PhysObjDecor::getSpeed() { return physObj->getSpeed(); }

Vec PhysObjDecor::getPos() { return physObj->getPos(); }

void PhysObjDecor::setSpeed(Vec v) { physObj->setSpeed(v); }

void PhysObjDecor::setPos(Vec v) { physObj->setPos(v); }

void PhysObjDecor::update(float delta) {
	physObj->update(delta);
}

PhysObj* PhysObjDecor::getBase() {
	PhysObjDecor *decor = dynamic_cast<PhysObjDecor*>(physObj);
	if (decor == nullptr) return physObj;
	return decor->getBase();
}

/******************
* PHYS OBJ ACC
******************/
Vec PhysObjAcc::getAcc() { return acc; }

void PhysObjAcc::setAcc(Vec v) { acc = v; }

void PhysObjAcc::update(float delta) {
	Vec speed = getSpeed();
	speed.x += delta * acc.x;
	speed.y += delta * acc.y;
	setSpeed(speed);
	physObj->update(delta);
}

/******************
* PHYS OBJ AIR RES
******************/
float PhysObjAirRes::getResistance() { return res; }

void PhysObjAirRes::setResistance(float f) { res = f; }

void PhysObjAirRes::update(float delta) {
	Vec speed = getSpeed();
	speed.x -= speed.x * res;	
	speed.y -= speed.y * res;	
	setSpeed(speed);
	physObj->update(delta);
}

//PhysObjBound
int PhysObjBound::getMinX() { return minX; }

void PhysObjBound::setMinX(int x) {minX = x;}

void PhysObjBound::update(float delta) {
	Vec basePos = getBase()->getPos();
	if (basePos.x < minX) {
		basePos.x = minX;
		getBase()->setPos(basePos);
	}
	physObj->update(delta);
}
