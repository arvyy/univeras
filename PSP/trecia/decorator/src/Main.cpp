#include <iostream>
#include "PhysObj.hpp"

using namespace std;
using namespace PSP;

void test(PhysObj* obj) {
	for (int i = 0; i < 10; i++) {
		obj->update(1.0f);
		cout << "POS [" << obj->getPos().x << "; " << obj->getPos().y << "]" << endl; 
	}
}

int main() {

	PhysObjDecor *obj = new PhysObjBound(new PhysObjAirRes(new PhysObjAcc (new PhysObjImpl())));
	obj->setSpeed(Vec(-1.0f, 2.0f));
	obj->getDecor<PhysObjAcc>()->setAcc(Vec(0.0f, 1.0f));
	obj->getDecor<PhysObjAirRes>()->setResistance(0.01f);
	obj->getDecor<PhysObjBound>()->setMinX(-5);

	cout << "1:" << endl;
	test(obj);

	obj->update(0.1f);
	PhysObj* physObj = obj->removeDecor<PhysObjBound>();
	obj = dynamic_cast<PhysObjDecor*>(physObj);
	obj->update(0.1f);

	obj->setPos(Vec(0.0f, 0.0f));
	obj->setSpeed(Vec(-1.0f, 2.0f));

	cout << "2:" << endl;
	test(obj);
}
