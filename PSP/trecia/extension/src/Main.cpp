#include <iostream>
#include "PhysObj.hpp"

using namespace std;
using namespace PSP;

void test(PhysObj* obj) {
	for (int i = 0; i < 10; i++) {
		float delta = 1.0f;
		obj->update(delta);
		cout << "POS [" << obj->getPos().x << "; " << obj->getPos().y << "]" << endl; 
	}
}

int main() {
	PhysObj *obj = new PhysObj();
	obj->setSpeed(Vec(1.0f, 2.0f));
	PhysObjAcc *acc = new PhysObjAcc();
	acc->setAcc(Vec(0.0f, 1.0f));
	acc->physObj = obj;
	obj->addExtension("acc", acc);
	PhysObjAirRes *res = new PhysObjAirRes();
	res->setResistance(0.01f);
	res->physObj = obj;
	obj->addExtension("airRes", res);

	cout << "1:" << endl;
	test(obj);

	obj->removeExtension("acc");
	obj->setPos(Vec(0.0f, 0.0f));
	obj->setSpeed(Vec(1.0f, 2.0f));

	cout << "2:" << endl;
	test(obj);
}
