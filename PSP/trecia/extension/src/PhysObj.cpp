#include "PhysObj.hpp"

using namespace PSP;
using namespace std;

Vec::Vec(float _x, float _y) : x(_x), y(_y) {}
Vec::Vec() {}

/******************
* PHYS OBJ
******************/
Vec PhysObj::getSpeed() { return speed; }

Vec PhysObj::getPos() { return pos; }

void PhysObj::setSpeed(Vec v) { speed = v; }

void PhysObj::setPos(Vec v) { pos = v; }

void PhysObj::update(float delta) {
	pos.x += speed.x * delta;
	pos.y += speed.y * delta;
	
	for (auto& pair : extensions) {
		PhysObjExtension *e = pair.second;
		PhysObjUpdateExtension *up = dynamic_cast<PhysObjUpdateExtension*>(e);
		if (up != nullptr) {
			up->update(delta);
		}
	}	
}

void PhysObj::addExtension(string name, PhysObjExtension* ext) {
	extensions[name] = ext;
}

PhysObjExtension* PhysObj::getExtension(string name) {
	return extensions[name];
}

PhysObjExtension* PhysObj::removeExtension(string name) {
	auto e = extensions[name];
	extensions[name] = nullptr;
	return e;
}

/******************
* PHYS OBJ ACC
******************/
Vec PhysObjAcc::getAcc() { return acc; }

void PhysObjAcc::setAcc(Vec v) { acc = v; }

void PhysObjAcc::update(float delta) {
	Vec speed = physObj->getSpeed();
	speed.x += delta * acc.x;
	speed.y += delta * acc.y;
	physObj->setSpeed(speed);
}

/******************
* PHYS OBJ AIR RES
******************/
float PhysObjAirRes::getResistance() { return res; }

void PhysObjAirRes::setResistance(float f) { res = f; }

void PhysObjAirRes::update(float delta) {
	Vec speed = physObj->getSpeed();
	speed.x -= speed.x * res;	
	speed.y -= speed.y * res;	
	physObj->setSpeed(speed);
}
