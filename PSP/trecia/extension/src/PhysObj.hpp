#ifndef PHYS_OBJ_HPP
#define PHYS_OBJ_HPP

#include <map>
#include <string>

namespace PSP {
	
	struct Vec {
		Vec();
		Vec(float x, float y);
		float x;
		float y;
	};

	class PhysObjExtension;

	class PhysObj {
		public:
			Vec getSpeed();
			Vec getPos() ;
			void setSpeed(Vec v) ;
			void setPos(Vec v) ;
			void update(float delta) ;

			void addExtension(std::string name, PhysObjExtension* ext);
			PhysObjExtension* getExtension(std::string name);
			PhysObjExtension* removeExtension(std::string name);
		private:
			Vec speed;
			Vec pos;

			std::map<std::string, PhysObjExtension*> extensions;
	};
	
	//inner update extension

	class PhysObjExtension {
		public:
			virtual ~PhysObjExtension(){}
			PhysObj* physObj;
	};

	class PhysObjUpdateExtension : public PhysObjExtension {
		public:
			virtual void update(float delta) = 0;
	};

	class PhysObjAcc : public PhysObjUpdateExtension {
		public:
			Vec getAcc();
			void setAcc(Vec v);
			void update(float delta) override;
		private:
			Vec acc;
	};

	class PhysObjAirRes : public PhysObjUpdateExtension  {
		public:
			float getResistance();
			void setResistance(float f);
			void update(float delta) override;
		private:
			float res;
	};

}

#endif
