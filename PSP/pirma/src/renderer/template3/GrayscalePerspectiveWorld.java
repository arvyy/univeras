package renderer.template3;

import java.awt.Color;
import java.awt.image.BufferedImage;

import renderer.commons.Camera;
import renderer.commons.Rect3D;

public class GrayscalePerspectiveWorld extends PerspectiveWorld {

	private GrayPostprocessingApplier gray;
	
	public GrayscalePerspectiveWorld(int width, int height) {
		super(width, height);
		gray = new GrayPostprocessingApplier();
	}


	@Override
	protected void applyPostprocessing(BufferedImage image) {
		gray.makeGray(image);
	}
}
