package renderer.template3;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import renderer.commons.Camera;
import renderer.commons.Rect3D;

public class GrayscaleOrthoWorld extends OrthoWorld {

	private GrayPostprocessingApplier gray;
	
	public GrayscaleOrthoWorld(int width, int height) {
		super(width, height);
		gray = new GrayPostprocessingApplier();
	}

	
	@Override
	protected void applyPostprocessing(BufferedImage image) {
		gray.makeGray(image);
	}
	
	
}
