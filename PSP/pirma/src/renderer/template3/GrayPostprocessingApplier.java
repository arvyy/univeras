package renderer.template3;

import java.awt.Color;
import java.awt.image.BufferedImage;

public class GrayPostprocessingApplier {

	void makeGray(BufferedImage image) {{
				for (int x = 0; x < image.getWidth(); x++)
					for (int y = 0; y < image.getHeight(); y++) {
						int rgb = image.getRGB(x, y);
						int r = (rgb >> 16) & 0xF;
						int g = (rgb >> 8) & 0xF;
						int b = rgb & 0xF;
						float val = (float)(r+g+b) / 15 / 3;
						image.setRGB(x, y, new Color(val, val, val).getRGB());
					}
			}
	}
	
}
