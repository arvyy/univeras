package renderer.template3;

import java.awt.Color;

import javax.swing.*;

import renderer.commons.Rect3D;
import renderer.commons.Utils;

public class TemplateLauncher {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			JFrame frame = new JFrame("World template");
			frame.setSize(800, 400);
			JPanel panel = new JPanel();
			World world = new OrthoWorld(400, 400);
			for (Rect3D r : Utils.createTestcaseRectList())
				world.addRectangle(r);
			panel.add(world);
			world = new PerspectiveWorld(400, 400);
			for (Rect3D r : Utils.createTestcaseRectList())
				world.addRectangle(r);
			panel.add(world);
			world = new GrayscaleOrthoWorld(400, 400);
			for (Rect3D r : Utils.createTestcaseRectList())
				world.addRectangle(r);
			panel.add(world);
			world = new GrayscalePerspectiveWorld(400, 400);
			for (Rect3D r : Utils.createTestcaseRectList())
				world.addRectangle(r);
			panel.add(world);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JScrollPane scroll= new JScrollPane(panel);
			frame.add(scroll);
			frame.setVisible(true);
		});
	}
	
}
