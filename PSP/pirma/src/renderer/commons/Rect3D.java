package renderer.commons;

import java.awt.Color;

public class Rect3D {

	private float x[], y[], z[];
	
	private Color color;

	public float[] getX() {
		return x;
	}

	public void setX(float[] x) {
		this.x = x;
	}

	public float[] getY() {
		return y;
	}

	public void setY(float[] y) {
		this.y = y;
	}

	public float[] getZ() {
		return z;
	}

	public void setZ(float[] z) {
		this.z = z;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
}
