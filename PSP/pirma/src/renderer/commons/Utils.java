package renderer.commons;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Utils {

	public static List<Rect3D> createTestcaseRectList() {
		ArrayList<Rect3D> list = new ArrayList<>();
		Rect3D r;
		
		for (int x = 0; x < 3; x++) {
			for (int y = 0; y < 3; y++) {
				r = new Rect3D();
				r.setX(new float[]{x*60, x*60 + 50});
				r.setY(new float[]{y*60, y*60 + 50});
				r.setZ(new float[]{0, 50});
				r.setColor(new Color(x * 80, y * 80, 255));
				list.add(r);
			}
		}
		
		return list;
	}

}
