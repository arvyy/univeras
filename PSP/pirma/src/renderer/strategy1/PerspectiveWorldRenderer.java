package renderer.strategy1;

import renderer.commons.*;

public class PerspectiveWorldRenderer implements WorldRenderer {
	
	@Override
	public int getRenderPriority(Rect3D rect, Camera cam) {
		int cx = (int) ((rect.getX()[1] - rect.getX()[0]) * 0.5 + rect.getX()[0]);
		int cy = (int) ((rect.getY()[1] - rect.getY()[0]) * 0.5 + rect.getY()[0]);
		return 10000 - Math.abs(cam.getX() - cx) - Math.abs(cam.getY() - cy);
	}

	@Override
	public int x(float x, float y, float z, Camera cam) {
		return (int)(x + (-cam.getX() + x) * z * 0.01);
	}

	@Override
	public int y(float x, float y, float z, Camera cam) {
		return (int)(y + (-cam.getY() + y) * z * 0.01);
	}
} 
