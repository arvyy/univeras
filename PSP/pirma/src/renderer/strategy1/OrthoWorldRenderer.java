package renderer.strategy1;

import renderer.commons.*;

public class OrthoWorldRenderer implements WorldRenderer {

	@Override
	public int x(float x, float y, float z, Camera cam) {
		return (int) (-0.5 * y + x);
	}
	
	@Override
	public int y(float x, float y, float z, Camera cam) {
		return (int) (0.5 * y - z);
	}

	@Override
	public  int getRenderPriority(Rect3D rect, Camera cam) {
		return (int) (rect.getY()[1] * 1000 + rect.getX()[0]);
	}
}

