package renderer.strategy3;

import java.awt.image.BufferedImage;

public class NoPostprocessing implements Postprocessing {

	@Override
	public void applyPostprocessing(BufferedImage image) {}

}
