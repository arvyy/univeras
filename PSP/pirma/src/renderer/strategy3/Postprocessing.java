package renderer.strategy3;

import java.awt.image.BufferedImage;

public interface Postprocessing {

	public void applyPostprocessing(BufferedImage image);
	
}
