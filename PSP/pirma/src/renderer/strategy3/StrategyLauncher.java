package renderer.strategy3;

import java.awt.Color;

import javax.swing.*;

import renderer.commons.Rect3D;
import renderer.commons.Utils;

public class StrategyLauncher {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			JFrame frame = new JFrame("World template");
			frame.setSize(800, 400);
			JPanel panel = new JPanel();
			World world;
			world = new World(400, 400, new OrthoWorldRenderer(), new NoPostprocessing());
			for (Rect3D r : Utils.createTestcaseRectList())
				world.addRectangle(r);
			panel.add(world);
			world = new World(400, 400, new PerspectiveWorldRenderer(), new NoPostprocessing());
			for (Rect3D r : Utils.createTestcaseRectList())
				world.addRectangle(r);
			panel.add(world);
			world = new World(400, 400, new OrthoWorldRenderer(), new GrayscalePostprocessing());
			for (Rect3D r : Utils.createTestcaseRectList())
				world.addRectangle(r);
			panel.add(world);
			world = new World(400, 400, new PerspectiveWorldRenderer(), new GrayscalePostprocessing());
			for (Rect3D r : Utils.createTestcaseRectList())
				world.addRectangle(r);
			world.setLocation(0, 400);
			panel.add(world);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			JScrollPane scrollPane = new JScrollPane(panel);
			frame.add(scrollPane);
			frame.setVisible(true);
		});
	}
	
}
