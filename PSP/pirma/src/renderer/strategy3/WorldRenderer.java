package renderer.strategy3;

import renderer.commons.*;

public interface WorldRenderer {
	int getRenderPriority(Rect3D rect, Camera cam);
	int x(float x, float y, float z, Camera cam);
	int y(float x, float y, float z, Camera cam);
}
