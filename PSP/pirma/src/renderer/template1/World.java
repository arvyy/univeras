package renderer.template1;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.Timer;

import renderer.commons.Camera;
import renderer.commons.Rect3D;

public abstract class World extends JComponent implements MouseMotionListener, ActionListener {
	

	protected abstract int getRenderPriority(Rect3D rect, Camera cam);
	protected abstract int x(float x, float y, float z, Camera cam);
	protected abstract int y(float x, float y, float z, Camera cam);
	
	private Timer timer;
	private Camera cam;
	
	private int lastMouseX, lastMouseY;
	
	private Color background;
	
	private List<Rect3D> rectangles;
	
	private List<int[]> connecting_indeces;
	
	private BufferedImage buffer;
	
	private int width, height;
	
	public World(int width, int height) {
		cam = new Camera();
		
		timer = new Timer(10, this);
		timer.start();
		
		addMouseMotionListener(this);
		
		background = Color.green;
		
		rectangles = new ArrayList<>();
		
		buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		this.width = width;
		this.height = height;
		
		connecting_indeces = Arrays.asList(
				//virsus
				new int[]{0, 0, 0, 1, 0, 0},
				new int[]{0, 0, 0, 0, 1, 0},
				new int[]{1, 0, 0, 1, 1, 0},
				new int[]{0, 1, 0, 1, 1, 0},
				//sonai
				new int[]{0, 0, 0, 0, 0, 1},
				new int[]{0, 1, 0, 0, 1, 1},
				new int[]{1, 0, 0, 1, 0, 1},
				new int[]{1, 1, 0, 1, 1, 1},
				//apacia
				new int[]{0, 0, 1, 1, 0, 1},
				new int[]{0, 0, 1, 0, 1, 1},
				new int[]{1, 0, 1, 1, 1, 1},
				new int[]{0, 1, 1, 1, 1, 1}
			);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		this.repaint();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = buffer.createGraphics();
		g2.setBackground(background);
		g2.clearRect(0, 0, buffer.getWidth(), buffer.getHeight());
		g2.translate(-cam.getX() + width / 2, -cam.getY() + height / 2);
		rectangles.sort((a, b) -> {
			return Integer.compare(getRenderPriority(a, cam), getRenderPriority(b, cam));
		});
		rectangles.forEach(rect -> renderRect(g2, rect, cam));
		g2.setTransform(new AffineTransform());
		g2.dispose();
		g.drawImage(buffer, 0, 0, null);
	}
	
	private void renderRect(Graphics2D g, Rect3D rect, Camera cam) {
		
		g.setStroke(new BasicStroke(5));
		g.setColor(rect.getColor());
		
		for (int[] connection : connecting_indeces) {
			g.drawLine(
				x(rect.getX()[connection[0]], rect.getY()[connection[1]], rect.getZ()[connection[2]], cam), 
				y(rect.getX()[connection[0]], rect.getY()[connection[1]], rect.getZ()[connection[2]], cam), 
				x(rect.getX()[connection[3]], rect.getY()[connection[4]], rect.getZ()[connection[5]], cam), 
				y(rect.getX()[connection[3]], rect.getY()[connection[4]], rect.getZ()[connection[5]], cam)
			);
		}
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(width, height);
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		lastMouseX = e.getX();
		lastMouseY = e.getY();
	}
	
	@Override
    public void mouseDragged(MouseEvent e) {
		cam.move(-e.getX() + lastMouseX, -e.getY() + lastMouseY);
		lastMouseX = e.getX();
		lastMouseY = e.getY();
	}
	public Color getBackground() {
		return background;
	}
	public void setBackground(Color background) {
		this.background = background;
	}
	public List<Rect3D> getRectangles() {
		return rectangles;
	}
	public void addRectangle(Rect3D rectangle) {
		rectangles.add(rectangle);
	}
}
