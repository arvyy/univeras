package renderer.strategy2;

import java.awt.Color;

import javax.swing.*;

import renderer.commons.Rect3D;
import renderer.commons.Utils;
import renderer.strategy1.OrthoWorldRenderer;
import renderer.strategy1.PerspectiveWorldRenderer;

public class StrategyLauncher {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			JFrame frame = new JFrame("World template");
			frame.setSize(800, 400);
			JPanel panel = new JPanel();
			Architect world = new Architect(400, 400, new OrthoWorldRenderer());
			for (Rect3D r : Utils.createTestcaseRectList())
				world.addRectangle(r);
			panel.add(world);
			world = new Architect(400, 400, new PerspectiveWorldRenderer());
			for (Rect3D r : Utils.createTestcaseRectList())
				world.addRectangle(r);
			world.setLocation(0, 400);
			panel.add(world);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.add(panel);
			frame.setVisible(true);
		});
	}
	
}
