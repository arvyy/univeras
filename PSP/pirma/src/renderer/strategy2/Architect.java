package renderer.strategy2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.Timer;

import renderer.commons.Camera;
import renderer.commons.Rect3D;
import renderer.strategy1.WorldRenderer;

public class Architect extends JComponent {
	
	private Camera cam;
	
	private Color background;
	
	private List<Rect3D> rectangles;
	
	private List<int[]> connecting_indeces;
	
	private BufferedImage buffer;
	
	private int width, height;
	
	private final WorldRenderer renderer;

	public Architect(int width, int height, WorldRenderer renderer) {
		JButton addNew = new JButton("+");
		JTextField x[] = new JTextField[]{
			new JTextField(),
			new JTextField()
		};
		JTextField y[] = new JTextField[]{
				new JTextField(),
				new JTextField()
			};
		JTextField z[] = new JTextField[]{
				new JTextField(),
				new JTextField()
			};
		JColorChooser col = new JColorChooser(Color.BLUE);
		addNew.addActionListener(l -> {
			Object items[] = new Object[]{
				"x1",
				x[0],
				"x2",
				x[1],
				"y1",
				y[0],
				"y2",
				y[1],
				"z1",
				z[0],
				"z2",
				z[1],
				col
			};
			int rez = JOptionPane.showConfirmDialog(null, items, "", JOptionPane.OK_CANCEL_OPTION);
			if (rez == JOptionPane.OK_OPTION) {
				Rect3D r = new Rect3D();
				r.setX(new float[]{Float.parseFloat(x[0].getText()), Float.parseFloat(x[1].getText())});
				r.setY(new float[]{Float.parseFloat(y[0].getText()), Float.parseFloat(y[1].getText())});
				r.setZ(new float[]{Float.parseFloat(z[0].getText()), Float.parseFloat(z[1].getText())});
				r.setColor(col.getColor());
				rectangles.add(r);
				repaint();
			}
		});
		setLayout(null);
		addNew.setSize(64, 32);
		addNew.setLocation(16, 16);
		add(addNew);
		this.renderer = renderer;
		cam = new Camera();
		
		background = Color.green;
		
		rectangles = new ArrayList<>();
		
		buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		this.width = width;
		this.height = height;
		
		connecting_indeces = Arrays.asList(
				//virsus
				new int[]{0, 0, 0, 1, 0, 0},
				new int[]{0, 0, 0, 0, 1, 0},
				new int[]{1, 0, 0, 1, 1, 0},
				new int[]{0, 1, 0, 1, 1, 0},
				//sonai
				new int[]{0, 0, 0, 0, 0, 1},
				new int[]{0, 1, 0, 0, 1, 1},
				new int[]{1, 0, 0, 1, 0, 1},
				new int[]{1, 1, 0, 1, 1, 1},
				//apacia
				new int[]{0, 0, 1, 1, 0, 1},
				new int[]{0, 0, 1, 0, 1, 1},
				new int[]{1, 0, 1, 1, 1, 1},
				new int[]{0, 1, 1, 1, 1, 1}
			);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = buffer.createGraphics();
		g2.setBackground(background);
		g2.clearRect(0, 0, buffer.getWidth(), buffer.getHeight());
		g2.translate(-cam.getX() + width / 2, -cam.getY() + height / 2);
		rectangles.sort((a, b) -> {
			return Integer.compare(renderer.getRenderPriority(a, cam), renderer.getRenderPriority(b, cam));
		});
		rectangles.forEach(rect -> renderRect(g2, rect, cam));
		g2.setTransform(new AffineTransform());
		g2.dispose();
		g.drawImage(buffer, 0, 0, null);
	}
	
	private void renderRect(Graphics2D g, Rect3D rect, Camera cam) {
		
		g.setStroke(new BasicStroke(5));
		g.setColor(rect.getColor());
		
		for (int[] connection : connecting_indeces) {
			g.drawLine(
				renderer.x(rect.getX()[connection[0]], rect.getY()[connection[1]], rect.getZ()[connection[2]], cam), 
				renderer.y(rect.getX()[connection[0]], rect.getY()[connection[1]], rect.getZ()[connection[2]], cam), 
				renderer.x(rect.getX()[connection[3]], rect.getY()[connection[4]], rect.getZ()[connection[5]], cam), 
				renderer.y(rect.getX()[connection[3]], rect.getY()[connection[4]], rect.getZ()[connection[5]], cam)
			);
		}
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(width, height);
	}
	
	public Color getBackground() {
		return background;
	}
	public void setBackground(Color background) {
		this.background = background;
	}
	public List<Rect3D> getRectangles() {
		return rectangles;
	}
	public void addRectangle(Rect3D rectangle) {
		rectangles.add(rectangle);
	}
}
