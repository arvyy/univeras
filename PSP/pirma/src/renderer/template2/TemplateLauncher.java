package renderer.template2;

import java.awt.Color;

import javax.swing.*;

import renderer.commons.Rect3D;

public class TemplateLauncher {

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			JFrame frame = new JFrame("World template");
			frame.setSize(800, 400);
			JPanel panel = new JPanel();
			Architect architect = new OrthoArchitect(400, 400);
//			for (Rect3D r : Rect3D.create())
//				architect.addRectangle(r);
			panel.add(architect);
			architect = new PerspectiveArchitect(400, 400);
//			for (Rect3D r : Rect3D.create())
//				architect.addRectangle(r);
			architect.setLocation(0, 400);
			panel.add(architect);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.add(panel);
			frame.setVisible(true);
		});
	}
	
}
