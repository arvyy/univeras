package renderer.template2;

import renderer.commons.Camera;
import renderer.commons.Rect3D;

public class PerspectiveArchitect extends Architect {

	public PerspectiveArchitect(int width, int height) {
		super(width, height);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected int getRenderPriority(Rect3D rect, Camera cam) {
		int cx = (int) ((rect.getX()[1] - rect.getX()[0]) * 0.5 + rect.getX()[0]);
		int cy = (int) ((rect.getY()[1] - rect.getY()[0]) * 0.5 + rect.getY()[0]);
		return 10000 - Math.abs(cam.getX() - cx) - Math.abs(cam.getY() - cy);
	}
	
	@Override
	protected int x(float x, float y, float z, Camera cam) {
		return (int)(x + (-cam.getX() + x) * z * 0.01);
	}
	
	@Override
	protected int y(float x, float y, float z, Camera cam) {
		return (int)(y + (-cam.getY() + y) * z * 0.01);
	}
}
