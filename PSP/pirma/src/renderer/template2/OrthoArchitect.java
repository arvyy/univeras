package renderer.template2;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import renderer.commons.Camera;
import renderer.commons.Rect3D;

public class OrthoArchitect extends Architect {

	public OrthoArchitect(int width, int height) {
		super(width, height);
	}
	
	@Override
	protected int x(float x, float y, float z, Camera cam) {
		return (int) (-0.5 * y + x);
	}
	
	@Override
	protected int y(float x, float y, float z, Camera cam) {
		return (int) (0.5 * y - z);
	}

	@Override
	protected int getRenderPriority(Rect3D rect, Camera cam) {
		return (int) (rect.getY()[1] * 1000 + rect.getX()[0]);
	}

}
