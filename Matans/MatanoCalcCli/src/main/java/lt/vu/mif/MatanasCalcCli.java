package lt.vu.mif;

import java.awt.AlphaComposite;
import java.math.BigDecimal;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import lt.vu.mif.matCalc.AddFunc;
import lt.vu.mif.matCalc.ArctgFunc;
import lt.vu.mif.matCalc.Calculator;
import lt.vu.mif.matCalc.CompositionFunc;
import lt.vu.mif.matCalc.CosFunc;
import lt.vu.mif.matCalc.DivFunc;
import lt.vu.mif.matCalc.ExpFunc;
import lt.vu.mif.matCalc.LnFunc;
import lt.vu.mif.matCalc.MulFunc;
import lt.vu.mif.matCalc.PolynomFunc;
import lt.vu.mif.matCalc.PowerAlphaFunc;
import lt.vu.mif.matCalc.SinFunc;
import lt.vu.mif.matCalc.TransformableToFrac;
import lt.vu.mif.matCalc.Util;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

public class MatanasCalcCli {

	public static void main(String[] args) throws ScriptException {
		
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
		Bindings b = engine.createBindings();
		
		Supplier<SinFunc> sin = SinFunc::new;
		b.put("sin", sin);
		
		Supplier<CosFunc> cos = CosFunc::new;
		b.put("cos", cos);
		
		Supplier<ArctgFunc> arc = ArctgFunc::new;
		b.put("arctg", arc);
		
		Supplier<LnFunc> ln = LnFunc::new;
		b.put("ln", ln);
		
		Supplier<ExpFunc> exp = ExpFunc::new;
		b.put("exp", exp);
		
		Supplier<PowerAlphaFunc> alpha = PowerAlphaFunc::new;
		b.put("powA", alpha);
		
		BiFunction<TransformableToFrac, TransformableToFrac, DivFunc> div = (aa, bb) -> {
			DivFunc d = new DivFunc();
			d.setA(aa);
			d.setB(bb);
			return d;
		};
		b.put("div", div);
		
		BiFunction<TransformableToFrac, TransformableToFrac, MulFunc> mul = (aa, bb) -> {
			MulFunc d = new MulFunc();
			d.setA(aa);
			d.setB(bb);
			return d;
		};
		b.put("mul", mul);
		
		BiFunction<TransformableToFrac, TransformableToFrac, AddFunc> add = (aa, bb) -> {
			AddFunc d = new AddFunc();
			d.setA(aa);
			d.setB(bb);
			return d;
		};
		b.put("add", add);
		
		BiFunction<TransformableToFrac, TransformableToFrac, AddFunc> sub = (aa, bb) -> {
			AddFunc d = new AddFunc();
			d.setA(aa);
			d.setB(bb);
			d.setNegB(true);
			return d;
		};
		b.put("sub", sub);
		
		BiFunction<TransformableToFrac, TransformableToFrac, CompositionFunc> comp = (aa, bb) -> {
			CompositionFunc d = new CompositionFunc();
			d.setOutterFunc(aa);
			d.setInnerFunc(bb);
			return d;
		};
		b.put("comp", comp);
		
		Function<ScriptObjectMirror, PolynomFunc> polFunc = mults -> {
			int length = (int)mults.get("length");
			PolynomFunc f = new PolynomFunc();
			for (int i = 0; i < length; i++) {
				f.set(i, new BigDecimal(mults.getSlot(i).toString()));
			}
			return f;
		}; 
		b.put("pol", polFunc);
		
		BiFunction<TransformableToFrac, Object, String> lim = (func, val_obj) -> {
			Calculator calc = new Calculator();
			BigDecimal arg;
			String val = val_obj.toString();
			if ("+inf".equals(val)) {
				arg = Util.POSITIVE_INFINITY;
			} else if ("-inf".equals(val)) {
				arg = Util.NEGATIVE_INFINITY;
			} else {
				arg = new BigDecimal(val);
			}
			double res = calc.getLim(func, arg);
			
			if (Double.isNaN(res)) return "Ribos nėra";
			if (res == Double.POSITIVE_INFINITY) return "+inf";
			if (res == Double.NEGATIVE_INFINITY) return "-inf";
			return String.valueOf(res);
		};
		b.put("lim", lim);
		
		engine.setBindings(b, ScriptContext.ENGINE_SCOPE);
		
		Scanner scanner = new Scanner(System.in);
		while (true) {
			String in = scanner.nextLine();
			System.out.println(engine.eval(in));
		}
	}
	
}
