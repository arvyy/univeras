//package lt.vu.mif.matCalc.test;
//
//import static org.junit.Assert.*;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.junit.Test;
//
//import lt.vu.mif.matCalc.*;
//
//public class MatCalcTests {
//	
//	@Test
//	public void factTest() {
//		assertEquals(120, Util.fact(5));
//	}
//	/*
//	 * perm test
//	 */
//	@Test
//	public void permutatorTest() {
//		Permutator.set(3);
//		Permutator p = Permutator.getPermutator();
//		assertEquals(0, p.get());
//		p.nextPerm();
//		assertEquals(1, p.get());
//		p.nextPerm();
//		assertEquals(2, p.get());
//		p.nextPerm();
//		p.get();
//		assertEquals(1, p.get());
//	}
//	
//	/*
//	 * y^2 + 1, y = 2x + 1 => 4x^2 + 4x + 2 
//	 */
//	@Test
//	public void substitutionTest() {
//		BasePolynom outter = new BasePolynom();
//		outter.getTerms().add(new BaseTerm(1, 2));
//		outter.getTerms().add(new BaseTerm(1, 0));
//		BasePolynom inner = new BasePolynom();
//		inner.getTerms().add(new BaseTerm(2, 1));
//		inner.getTerms().add(new BaseTerm(1, 0));
//		
//		System.out.println(BasePolynom.substitute(outter, inner));
//	}
//	
//	@Test
//	public void raiseToPowerTest() {
//		BasePolynom inner = new BasePolynom();
//		inner.getTerms().add(new BaseTerm(2, 1));
//		inner.getTerms().add(new BaseTerm(1, 0));
//		System.out.println(BasePolynom.raiseToPower(inner, 2));
//	}
//	
//	/*
//	 * test sin(x) / x, x -> 0
//	 */
//	@Test
//	public void test1() {
//		DivFunc div = new DivFunc();
//		
//		SinFunc sin = new SinFunc();
//		
//		PolynomFunc pol = new PolynomFunc();
//		HashMap<Integer, Double> mult = new HashMap<>();
//		mult.put(1, 1d);
//		pol.setMultiplier(mult);
//		
//		div.setA(sin);
//		div.setB(pol);
//		
//		Fraction f = div.transform(0);
//		double lim = f.getLim(0);
//		assertEquals(1d, lim, 0d);
//	}
//	
//	/*
//	 * test 1 / x, x -> 0
//	 */
//	@Test
//	public void test2() {
//		DivFunc div = new DivFunc();
//		
//		PolynomFunc pol = new PolynomFunc();
//		HashMap<Integer, Double> mult = new HashMap<>();
//		mult.put(0, 1d);
//		pol.setMultiplier(mult);
//		
//		PolynomFunc pol2 = new PolynomFunc();
//		HashMap<Integer, Double> mult2 = new HashMap<>();
//		mult2.put(1, 1d);
//		pol2.setMultiplier(mult2);
//		
//		div.setA(pol);
//		div.setB(pol2);
//		
//		Fraction f = div.transform(0);
//		double lim = f.getLim(0);
//		assertTrue(Double.isNaN(lim));
//	}
//	
//	/*
//	 * test arctg(x), x -> +inf
//	 */
//	@Test
//	public void test3() {
//		ArctgFunc arctg = new ArctgFunc();
//		Fraction f = arctg.transform(Double.MAX_VALUE);
//		double lim = f.getLim(Double.MAX_VALUE);
//		assertEquals(Math.PI / 2, lim, 0.1d);
//	}
//	
//	@Test
//	public void test4() {
//		Calculator calc = new Calculator();
//		
//		// test sin(x), x->+inf
//		SinFunc sf= new SinFunc();
//		double lim = calc.getLim(sf, Double.POSITIVE_INFINITY);
//		assertTrue(Double.isNaN(lim));
//		
//		//test sin(x) / x, x->+inf
//		PolynomFunc pf = new PolynomFunc();
//		Map<Integer, Double> mults = new HashMap<>();
//		mults.put(1, 1d);
//		pf.setMultiplier(mults);
//		DivFunc div = new DivFunc();
//		div.setA(sf);
//		div.setB(pf);
//		lim = calc.getLim(div, Double.POSITIVE_INFINITY);
//		assertEquals(0d, lim, 0d);
//	}
//	
//	/*
//	 * test sin(x)^2, x->+inf
//	 */
//	@Test
//	public void test5() {
//		Calculator calc = new Calculator();
//		
//		CompositionFunc comp = new CompositionFunc();
//		SinFunc sin = new SinFunc();
//		PolynomFunc pf = new PolynomFunc();
//		pf.set(2, 1);
//		comp.setInnerFunc(sin);
//		comp.setOutterFunc(pf);
//		double lim = calc.getLim(comp, Double.POSITIVE_INFINITY);
//		System.out.println(lim);
//		assertTrue(Double.isNaN(lim));
//	}
//	
//	@Test
//	public void test6() {
//		Calculator calc = new Calculator();
//		SinFunc sin = new SinFunc();
//		assertEquals(0d, calc.getLim(sin, Math.PI * 2), 0.001d);
//	}
//	
//	@Test
//	public void test7() {
//		Calculator calc = new Calculator();
//		ExpFunc exp = new ExpFunc();
//		assertEquals(0d, calc.getLim(exp, Double.NEGATIVE_INFINITY), 0d);
//	}
//	
//	@Test
//	public void test8() {
//		Calculator calc = new Calculator();
//		SinFunc sin = new SinFunc();
//		assertEquals(0d, calc.getLim(sin, 0), 0d);
//		assertEquals(0d, calc.getLim(sin, Math.PI * 30), 0d);
//	}
//}
