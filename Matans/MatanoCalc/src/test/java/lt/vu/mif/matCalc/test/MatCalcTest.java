package lt.vu.mif.matCalc.test;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

import lt.vu.mif.matCalc.AddFunc;
import lt.vu.mif.matCalc.ArctgFunc;
import lt.vu.mif.matCalc.Calculator;
import lt.vu.mif.matCalc.CompositionFunc;
import lt.vu.mif.matCalc.CosFunc;
import lt.vu.mif.matCalc.DivFunc;
import lt.vu.mif.matCalc.ExpFunc;
import lt.vu.mif.matCalc.LnFunc;
import lt.vu.mif.matCalc.PolynomFunc;
import lt.vu.mif.matCalc.PowerAlphaFunc;
import lt.vu.mif.matCalc.SinFunc;
import lt.vu.mif.matCalc.TransformableToFrac;
import lt.vu.mif.matCalc.Util;

public class MatCalcTest {

	private Calculator calc;
	
	@Before
	public void init() {
		calc = new Calculator();
	}
	
	@Test
	public void testSin() {
		SinFunc sin = new SinFunc();
		assertEquals(0d, calc.getLim(sin, new BigDecimal("0")), 0d);
		assertEquals(0d, calc.getLim(sin, new BigDecimal(Math.PI * 30)), 0d);
		assertEquals(Double.NaN, calc.getLim(sin, Util.POSITIVE_INFINITY), 0d);
		assertEquals(Double.NaN, calc.getLim(sin, Util.NEGATIVE_INFINITY), 0d);
	}
	
	@Test
	public void testCos() {
		CosFunc cos = new CosFunc();
		assertEquals(1d, calc.getLim(cos, new BigDecimal("0")), 0d);
		assertEquals(1d, calc.getLim(cos, new BigDecimal(Math.PI * 30)), 0d);
		assertEquals(Double.NaN, calc.getLim(cos, Util.POSITIVE_INFINITY), 0d);
		assertEquals(Double.NaN, calc.getLim(cos, Util.NEGATIVE_INFINITY), 0d);
	}
	
	@Test
	public void testExp() {
		ExpFunc exp = new ExpFunc();
		assertEquals(Double.POSITIVE_INFINITY, calc.getLim(exp, Util.POSITIVE_INFINITY), 0d);
		assertEquals(0d, calc.getLim(exp, Util.NEGATIVE_INFINITY), 0d);
		assertEquals(1d, calc.getLim(exp, new BigDecimal(0)), 0d);
		assertEquals(Math.E, calc.getLim(exp, new BigDecimal(1)), 0.0005d);
	}
	
	@Test
	public void testArctg() {
		ArctgFunc arc = new ArctgFunc();
		assertEquals(0, calc.getLim(arc, new BigDecimal(0)), 0.0005d);
		assertEquals(-1.249d, calc.getLim(arc, new BigDecimal(-3)), 0.0005d);
		assertEquals(-Math.PI / 2d, calc.getLim(arc, Util.NEGATIVE_INFINITY), 0.0005d);
		assertEquals(Math.PI / 2d, calc.getLim(arc, Util.POSITIVE_INFINITY), 0.0005d);
	}
	
	@Test
	public void testLn() {
		LnFunc ln = new LnFunc();
		assertEquals(0, calc.getLim(ln, new BigDecimal(0)), 0d);
		assertEquals(0.182d, calc.getLim(ln, new BigDecimal(0.2)), 0d);
		assertEquals(Double.POSITIVE_INFINITY, calc.getLim(ln, Util.POSITIVE_INFINITY), 0d);
		assertEquals(1.716d, calc.getLim(ln, new BigDecimal("4.56")), 0.0005d);
		//TODO: kai ln arg maziau uz 0
		//assertEquals(Double.NaN, calc.getLim(ln, new BigDecimal(-1)), 0d);
	}
	
	@Test
	public void testPowerAlpha() {
		PowerAlphaFunc alpha = new PowerAlphaFunc();
		alpha.setA(new BigDecimal("0.5"));
		assertEquals(1.118d, calc.getLim(alpha, new BigDecimal(0.25)), 0.0005d);
		assertEquals(Math.sqrt(5d), calc.getLim(alpha, new BigDecimal(4)), 0.0005d);
	}

	@Test
	public void testPythagor() {
		SinFunc sin = new SinFunc();
		PolynomFunc sin_poli = new PolynomFunc();
		sin_poli.set(2, BigDecimal.ONE);
		CompositionFunc sin_comp = new CompositionFunc();
		sin_comp.setOutterFunc(sin_poli);
		sin_comp.setInnerFunc(sin);
		
		CosFunc cos = new CosFunc();
		PolynomFunc cos_poli = new PolynomFunc();
		cos_poli.set(2, BigDecimal.ONE);
		CompositionFunc cos_comp = new CompositionFunc();
		cos_comp.setOutterFunc(cos_poli);
		cos_comp.setInnerFunc(cos);
		
		AddFunc add = new AddFunc();
		add.setA(sin_comp);
		add.setB(cos_comp);

		assertEquals(1d, calc.getLim(add, new BigDecimal(0.2d)), 0.005d);
	}

	@Test
	public void testAdd() {
		testAddInner(new SinFunc(), new CosFunc(), new BigDecimal(Math.PI / 12), 1.224d);
		testAddInner(new CosFunc(), new SinFunc(), new BigDecimal(0), 1);
		testAddInner(new LnFunc(), new ExpFunc(), new BigDecimal(0.2d), 1.404d);
	}
	
	private void testAddInner(TransformableToFrac a, TransformableToFrac b, BigDecimal arg, double expected) {
		AddFunc add = new AddFunc();
		add.setA(a);
		add.setB(b);
		assertEquals(expected, calc.getLim(add, arg), 0.005d);
	}
	
	@Test
	public void testComposition() {
		testCompInner(new SinFunc(), new CosFunc(), new BigDecimal(0.2d), 0.831d);
		testCompInner(new SinFunc(), new CosFunc(), Util.POSITIVE_INFINITY, Double.NaN);
		PowerAlphaFunc alpha = new PowerAlphaFunc();
		alpha.setA(new BigDecimal("0.5"));
		testCompInner(alpha, new LnFunc(), new BigDecimal("4.56"), 1.648d);
	}
	
	private void testCompInner(TransformableToFrac a, TransformableToFrac b, BigDecimal arg, double expected) {
		CompositionFunc comp = new CompositionFunc();
		comp.setOutterFunc(a);
		comp.setInnerFunc(b);
		assertEquals(expected, calc.getLim(comp, arg), 0.02d);
	}
	
	/*
	 * Test sinx / x
	 */
	@Test
	public void test1() {
		SinFunc sin = new SinFunc();
		PolynomFunc p = new PolynomFunc();
		p.set(1, BigDecimal.ONE);
		DivFunc div = new DivFunc();
		div.setA(sin);
		div.setB(p);		
		assertEquals(1d, calc.getLim(div, new BigDecimal(0)), 0d);
	}
	
	@Test
	/*
	 * Test 1 / x
	 */
	public void test2() {
		DivFunc div = new DivFunc();
		PolynomFunc nom = new PolynomFunc();
		nom.set(0, new BigDecimal(1));
		PolynomFunc denom = new PolynomFunc();
		denom.set(1, new BigDecimal(1));
		div.setA(nom);
		div.setB(denom);
		assertEquals(Double.NaN, calc.getLim(div, new BigDecimal(0)), 0d);
	}
}
