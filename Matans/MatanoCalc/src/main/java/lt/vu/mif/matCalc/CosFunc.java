package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class CosFunc implements BaseFunc {

	@Override
	public Fraction getTaylorSeries(BigDecimal a, int count) {
		
		if (Util.isInfinite(a)) return BaseFunc.super.getTaylorSeries(a, count);
		BigDecimal angle = a.remainder(new BigDecimal(Math.PI * 2));
		if (Util.cmp(angle, new BigDecimal(Math.PI)) == 1) angle = angle.subtract(new BigDecimal(Math.PI * 2));
		if (Util.cmp(angle, new BigDecimal(-Math.PI)) == -1) angle = angle.add(new BigDecimal(Math.PI * 2));
		Fraction bp = BaseFunc.super.getTaylorSeries(angle, count);
		
		if (a != angle) {
			BasePolynom transform = new BasePolynom();
			transform.getTerms().add(new BaseTerm(BigDecimal.ONE, 1));
			transform.getTerms().add(new BaseTerm(angle.subtract(a), 0));
			return Fraction.substitute(bp, new Fraction(transform));
		} else {
			return bp;
		}
	}
	
	@Override
	public Fraction getNthTaylorTerm(int n) {
		BigDecimal denom_mult = new BigDecimal(Util.fact(2*n));
		if (n % 2 == 1) {
			denom_mult = denom_mult.negate();
		}
		BaseTerm num = new BaseTerm(BigDecimal.ONE, 2*n);
		BaseTerm denom = new BaseTerm(denom_mult, 0);
		return new Fraction(new BasePolynom(num), new BasePolynom(denom));
	}

	@Override
	public boolean hasLim(BigDecimal a) {
		return Util.isFinite(a);
	}
	
	@Override
	public BigDecimal limInf(BigDecimal a) {
		return new BigDecimal(-1);
	}
	
	@Override
	public BigDecimal limSup(BigDecimal a) {
		return new BigDecimal(1);
	}
}
