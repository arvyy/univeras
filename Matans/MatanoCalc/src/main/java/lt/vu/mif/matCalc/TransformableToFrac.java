package lt.vu.mif.matCalc;

import java.math.BigDecimal;

public interface TransformableToFrac {

	Fraction transform(BigDecimal a);
	
}
