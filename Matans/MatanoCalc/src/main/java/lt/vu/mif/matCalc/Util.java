package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.stream.IntStream;

public class Util {

	
	public final static BigDecimal POSITIVE_INFINITY = new BigDecimal(0);
	public final static BigDecimal NEGATIVE_INFINITY = new BigDecimal(0);
	public final static BigDecimal NaN     = new BigDecimal(0);
	
	public static boolean isInfinite(BigDecimal bd) {
		return bd == POSITIVE_INFINITY || bd == NEGATIVE_INFINITY;
	}
	
	public static boolean isFinite(BigDecimal bd) {
		return !isInfinite(bd) && !isNaN(bd);
	}
	
	public static boolean isNaN(BigDecimal bd) {
		return bd == NaN;
	}
	
	public static int cmp(BigDecimal a, BigDecimal b) {
		if (a == POSITIVE_INFINITY && b == POSITIVE_INFINITY) return 0;
		if (a == NEGATIVE_INFINITY && b == NEGATIVE_INFINITY) return 0;
		if (a == NEGATIVE_INFINITY) return -1;
		if (b == NEGATIVE_INFINITY) return 1;
		if (a == POSITIVE_INFINITY) return 1;
		if (b == POSITIVE_INFINITY) return -1;
		return a.compareTo(b);
	}
	
	public static long fact(int a) {
		if (a == 0) return 1;
		long mul = 1;
		for (int i = 1; i <= a; i++) {
			mul *= i;
		}
		return mul;
	}

	public static int sign(BigDecimal d) {
		if (d == POSITIVE_INFINITY) return 1;
		if (d == NEGATIVE_INFINITY) return -1;
		return d.signum();
	}

	public static BigDecimal abs(BigDecimal a) {
		if (a == NEGATIVE_INFINITY) return POSITIVE_INFINITY;
		if (a == POSITIVE_INFINITY) return POSITIVE_INFINITY;
		return a.abs();
	}

}
