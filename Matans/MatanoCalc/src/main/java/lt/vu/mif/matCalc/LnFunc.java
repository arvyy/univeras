package lt.vu.mif.matCalc;

import java.math.BigDecimal;

/**
 * 
 * f(x) = ln(1 + x)
 */
public class LnFunc implements BaseFunc {

	
	
	@Override
	public Fraction getTaylorSeries(BigDecimal a, int count) {
		if (Util.isFinite(a) && a.abs().compareTo(BigDecimal.ONE) != 1) {
			return BaseFunc.super.getTaylorSeries(a, count);
		}
		if (Util.isInfinite(a)) {
			return BaseFunc.super.getTaylorSeries(a, count % 2 == 0 ? count + 1 : count);
		}
		int power = 1;
		BigDecimal e = new BigDecimal(Math.E);
		while (a.divide(e.pow(power), Cfg.resultMc()).compareTo(BigDecimal.ONE) == 1) {
			power++;
		}
		Fraction f = BaseFunc.super.getTaylorSeries(a.divide(e.pow(power), Cfg.resultMc()), count);
		Fraction sub = new Fraction();
		sub.setNum(new BasePolynom(
				new BaseTerm(BigDecimal.ONE, 0),
				new BaseTerm(BigDecimal.ONE, 1),
				new BaseTerm(e.pow(power).negate(), 0)
		));
		sub.setDenom(new BasePolynom(new BaseTerm(e.pow(power), 0)));
		f = Fraction.substitute(f, sub);
		Fraction minusN = new Fraction(new BasePolynom(new BaseTerm(new BigDecimal(power), 0)));
		f = Fraction.add(f, minusN);
		f.simplify();
		return f;
	}
	
	@Override
	public Fraction getNthTaylorTerm(int n) {
		BaseTerm num = new BaseTerm (n % 2 == 1 ? BigDecimal.ONE.negate() : BigDecimal.ONE, n + 1);
		BaseTerm denom = new BaseTerm(new BigDecimal(n + 1), 0);
		return new Fraction(new BasePolynom(num), new BasePolynom(denom));
		/*
		BigDecimal mult = new BigDecimal(n + 1);
		if (n % 2 == 1) {
			mult = mult.negate();
		}
		BaseTerm bt = new BaseTerm(mult, n+1);
		return new Fraction(new BasePolynom(bt));
		*/
	}
	
	@Override
	public boolean hasLim(BigDecimal a) {
		return Util.cmp(a, BigDecimal.ZERO) > 0;
	}
	
	@Override
	public BigDecimal limInf(BigDecimal a) {
		return Util.NEGATIVE_INFINITY;
	}
	
	@Override
	public BigDecimal limSup(BigDecimal a) {
		return Util.POSITIVE_INFINITY;
	}

}
