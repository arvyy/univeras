package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface Func extends TransformableToFrac {

	Fraction getTaylorSeries(BigDecimal a, int count);
	
	@Override
	default Fraction transform(BigDecimal a) {
		return getTaylorSeries(a, 6);
	}
	
}
