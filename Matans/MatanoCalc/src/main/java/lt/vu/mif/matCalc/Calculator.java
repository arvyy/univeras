package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Calculator {

	
	public double getLim(TransformableToFrac t, BigDecimal a) {
		Permutator.set(2);
		double last = t.transform(a).getLimDouble(a);
		Permutator p = Permutator.getPermutator();
		p.nextPerm();
		int length = (int) Math.pow(3, p.getLength());
		for (int i = 1; i < length; i++) {
			double newVal = t.transform(a).getLimDouble(a);
			if (newVal != last) {
				return Double.NaN;
			}
			last = newVal;
			p.nextPerm();
		}
		return last;
	}
	
}
