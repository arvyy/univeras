package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface BaseFunc extends Func {
	
	@Override
	default Fraction getTaylorSeries(BigDecimal a, int count) {
		if (hasLim(a)) {
			Fraction f = getNthTaylorTerm(0);
			for (int i = 1; i < count; i++) {
				f = Fraction.add(f, getNthTaylorTerm(i));
			}
			return f;
		} else {
			BasePolynom bp = new BasePolynom();
			/*
			bp.setLimExists(false);
			bp.setLimSup(limSup(a));
			bp.setLimInf(limInf(a));
			*/
			int choice = Permutator.getPermutator().get();
			BigDecimal val;
			BigDecimal inf = limInf(a);
			BigDecimal sup = limSup(a);
			if (choice == 0) val = inf;
			else val = sup;
			BaseTerm bt = new BaseTerm(val, 0);
			bp.getTerms().add(bt);
			Fraction f = new Fraction();
			f.setNum(bp);
			f.setDenom(BasePolynom.createConst(1));
			return f;
		}
	}
	
	Fraction getNthTaylorTerm(int n);
	
	default boolean hasLim(BigDecimal a) {return true;}
	
	default BigDecimal limSup(BigDecimal a) { return BigDecimal.ZERO; }
	
	default BigDecimal limInf(BigDecimal a) { return BigDecimal.ZERO; }
}
