package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ArctgFunc implements BaseFunc {

	@Override
	public Fraction getTaylorSeries(BigDecimal a, int count) {
		if (Util.cmp(Util.abs(a), new BigDecimal(1)) != 1) {
			return BaseFunc.super.getTaylorSeries(a, count);
		}
		//arctan(x) = -arctan(1/x) +- PI/2  
		BigDecimal newArg = null;
		if (Util.isFinite(a)) {
			newArg = BigDecimal.ONE.divide(a, Cfg.inverseScale(), RoundingMode.HALF_UP);
		} else {
			newArg = BigDecimal.ZERO;
		}
		Fraction bp = BaseFunc.super.getTaylorSeries(newArg, count);
		bp.getNum().neg();
		BigDecimal mult = new BigDecimal((Util.sign(a) <= 0 ? -1 : 1) * Math.PI / 2);
		BaseTerm bt = new BaseTerm(mult, 0);
		bp = Fraction.add(bp, new Fraction(new BasePolynom(bt)));
		bp.simplify();
		return Fraction.substitute(bp, new Fraction(BasePolynom.createConst(1), new BasePolynom(new BaseTerm(BigDecimal.ONE, 1))));
	}
	
	@Override
	public Fraction getNthTaylorTerm(int n) {
		
		BaseTerm num = new BaseTerm(n % 2 == 1 ? BigDecimal.ONE.negate() : BigDecimal.ONE, 2*n + 1);
		BaseTerm denom = new BaseTerm(new BigDecimal(2 * n + 1), 0);
		return new Fraction(new BasePolynom(num), new BasePolynom(denom));
		/*
		BigDecimal mult = BigDecimal.ONE.divide(new BigDecimal(2 * n + 1), Cfg.inverseScale(), RoundingMode.HALF_UP);
		if (n % 2 == 1) {
			mult = mult.negate();
		}
		BaseTerm bt = new BaseTerm(mult, 2*n + 1);
		return new Fraction(new BasePolynom(bt));
		*/
	}
}
