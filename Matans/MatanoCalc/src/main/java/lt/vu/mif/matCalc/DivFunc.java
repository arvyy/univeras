package lt.vu.mif.matCalc;

import java.math.BigDecimal;

public class DivFunc implements TransformableToFrac {
	
	private TransformableToFrac a;
	private TransformableToFrac b;

	@Override
	public Fraction transform(BigDecimal ap) {
		Fraction frac = new Fraction();
		Fraction fracA = a.transform(ap);
		Fraction fracB = b.transform(ap);
		frac.setDenom(BasePolynom.multiply(fracA.getDenom(), fracB.getNum()));
		frac.setNum(BasePolynom.multiply(fracA.getNum(), fracB.getDenom()));
		return frac;
	}

	public TransformableToFrac getA() {
		return a;
	}

	public void setA(TransformableToFrac a) {
		this.a = a;
	}

	public TransformableToFrac getB() {
		return b;
	}

	public void setB(TransformableToFrac b) {
		this.b = b;
	}

	
}
