package lt.vu.mif.matCalc;

import java.math.BigDecimal;

public class MulFunc implements TransformableToFrac {
	
	private TransformableToFrac a;
	private TransformableToFrac b;

	@Override
	public Fraction transform(BigDecimal a_point) {
		Fraction frac = new Fraction();
		Fraction fracA = a.transform(a_point);
		Fraction fracB = b.transform(a_point);
		frac.setDenom(BasePolynom.multiply(fracA.getDenom(), fracB.getDenom()));
		frac.setNum(BasePolynom.multiply(fracA.getNum(), fracB.getNum()));
		return frac;
	}

	public TransformableToFrac getA() {
		return a;
	}

	public void setA(TransformableToFrac a) {
		this.a = a;
	}

	public TransformableToFrac getB() {
		return b;
	}

	public void setB(TransformableToFrac b) {
		this.b = b;
	}

	
}
