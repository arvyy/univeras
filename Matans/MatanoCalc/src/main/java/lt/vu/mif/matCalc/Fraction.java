package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Fraction {

	private BasePolynom num;
	private BasePolynom denom;
	
	public Fraction() {}
	
	public Fraction(BasePolynom num) {
		this(num, BasePolynom.createConst(1));
	}
	
	public Fraction(BasePolynom num, BasePolynom denom) {
		this.num = num;
		this.denom = denom;
	}
	
	public void simplify() {
		num.simplify();
		denom.simplify();
	}
	
	public double getLimDouble(BigDecimal arg) {
		BigDecimal lim = getLim(arg);
		if (lim == Util.POSITIVE_INFINITY) return Double.POSITIVE_INFINITY;
		if (lim == Util.NEGATIVE_INFINITY) return Double.NEGATIVE_INFINITY;
		if (lim == Util.NaN) return Double.NaN;
		return lim.setScale(Cfg.resultScale(), RoundingMode.HALF_UP).doubleValue();
	}
	
	public BigDecimal getLim(BigDecimal arg) {
		simplify();
		BigDecimal nv = num.calcValue(arg);
		BigDecimal dv = denom.calcValue(arg);
		
		/*
		 * Virsuj ir apacioj paprasti skaiciia (ne nuliai ir ne begalybes)? grazinam santyki
		 */
		if (Util.isFinite(nv) && Util.isFinite(dv) && Util.cmp(nv, BigDecimal.ZERO) != 0 && Util.cmp(dv, BigDecimal.ZERO) != 0) {
			return nv.divide(dv, Cfg.resultMc());
		}
		
		/*
		 * Jei apacia nulis, tikrinam ar sutampa riba is virsaus ir apacios. Jei ne, ribos nera. 
		 */
		if (Util.cmp(dv, BigDecimal.ZERO) == 0 && Util.isFinite(dv)) {
			BigDecimal plus = arg.add(Cfg.delta());
			BigDecimal minus = arg.subtract(Cfg.delta());
			
			BigDecimal nvPos = num.calcValue(plus);
			BigDecimal denomPos = denom.calcValue(plus);
			int signPos = nvPos.signum() * denomPos.signum();
			
			BigDecimal nvNeg = num.calcValue(minus);
			BigDecimal denomNeg = denom.calcValue(minus);
			int signNeg = nvNeg.signum() * denomNeg.signum();
			
			if (signNeg != signPos) {
				return Util.NaN;
			}
		}
		
			BaseTerm biggestNum = num.getLargestTerm(arg).get();
			BaseTerm biggestDenom = denom.getLargestTerm(arg).get();
			int cmp = BaseTerm.compareMagnitude(biggestNum, biggestDenom, arg);
			if (cmp == 0) {
				return biggestNum.getMult().divide(biggestDenom.getMult(), Cfg.resultMc());
			}
			int sign = Util.sign(dv) * Util.sign(nv);
			if (cmp == 1) {
				return sign > 0 ? Util.POSITIVE_INFINITY : Util.NEGATIVE_INFINITY;
			}
			return BigDecimal.ZERO;
	}

	public BasePolynom getNum() {
		return num;
	}

	public void setNum(BasePolynom num) {
		this.num = num;
	}

	public BasePolynom getDenom() {
		return denom;
	}

	public void setDenom(BasePolynom denom) {
		this.denom = denom;
	}
	
	
	public static Fraction add(Fraction fracA, Fraction fracB) {
		Fraction frac = new Fraction();
		frac.setDenom(BasePolynom.multiply(fracA.getDenom(), fracB.getDenom()));
		BasePolynom nomA = BasePolynom.multiply(fracA.getNum(), fracB.getDenom());
		BasePolynom nomB = BasePolynom.multiply(fracB.getNum(), fracA.getDenom());
		frac.setNum(BasePolynom.add(nomA, nomB));
		frac.simplify();
		return frac;
	}
	
	public static Fraction mul(Fraction fracA, Fraction fracB) {
		Fraction frac = new Fraction();
		frac.setDenom(BasePolynom.multiply(fracA.getDenom(), fracB.getDenom()));
		frac.setNum(BasePolynom.multiply(fracA.getNum(), fracB.getNum()));
		frac.simplify();
		return frac;
	}
	
	public static Fraction mul(Fraction f, BigDecimal mult) {
		return new Fraction(BasePolynom.multiply(f.getNum(), mult), new BasePolynom(f.getDenom()));
	}
	
	public static Fraction div(Fraction fracA, Fraction fracB) {
		return Fraction.mul(fracA, new Fraction(new BasePolynom(fracB.getDenom()), new BasePolynom(fracB.getNum())));
	}
	
	public static Fraction raise(Fraction f, int power) {
		if (power == 0) return new Fraction(BasePolynom.createConst(1));
		BasePolynom top  = BasePolynom.raiseToPower(f.getNum(), Math.abs(power));
		BasePolynom bot  = BasePolynom.raiseToPower(f.getDenom(), Math.abs(power));
		if (power < 0) {
			BasePolynom temp = top;
			top = bot;
			bot = temp;
		}
		Fraction frac = new Fraction(top, bot);
		return frac;
	}
	
	public static Fraction substitute(BaseTerm term, Fraction sub) {
		return Fraction.mul(Fraction.raise(sub, term.getPower()), term.getMult());
	}
	
	public static Fraction substitute(BasePolynom current, Fraction sub) {
		Fraction f = new Fraction(BasePolynom.createConst(0));
		for (BaseTerm bt : current.getTerms()) {
			f = Fraction.add(f, Fraction.substitute(bt, sub));
		}
		f.simplify();
		return f;
	}
	
	public static Fraction substitute(Fraction current, Fraction sub) {
		Fraction top = Fraction.substitute(current.getNum(), sub);
		Fraction bot = Fraction.substitute(current.getDenom(), sub);
		return Fraction.div(top, bot);
	}
}
