package lt.vu.mif.matCalc;

import java.math.BigDecimal;

public class AddFunc implements TransformableToFrac {

	private TransformableToFrac a;
	private TransformableToFrac b;
	
	private boolean negA = false;
	private boolean negB = false;
	

	@Override
	public Fraction transform(BigDecimal a_point) {
		Fraction frac = new Fraction();
		Fraction fracA = a.transform(a_point);
		Fraction fracB = b.transform(a_point);
		frac.setDenom(BasePolynom.multiply(fracA.getDenom(), fracB.getDenom()));
		BasePolynom nomA = BasePolynom.multiply(fracA.getNum(), fracB.getDenom());
		if (negA) nomA.neg();
		BasePolynom nomB = BasePolynom.multiply(fracB.getNum(), fracA.getDenom());
		if (negB) nomB.neg();
		frac.setNum(BasePolynom.add(nomA, nomB));
		frac.simplify();
		return frac;
	}

	public boolean isNegA() {
		return negA;
	}

	public void setNegA(boolean negA) {
		this.negA = negA;
	}

	public boolean isNegB() {
		return negB;
	}

	public void setNegB(boolean negB) {
		this.negB = negB;
	}


	public TransformableToFrac getA() {
		return a;
	}

	public void setA(TransformableToFrac a) {
		this.a = a;
	}

	public TransformableToFrac getB() {
		return b;
	}

	public void setB(TransformableToFrac b) {
		this.b = b;
	}

	
	
}
