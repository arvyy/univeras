package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ExpFunc implements BaseFunc {

	@Override
	public Fraction getTaylorSeries(BigDecimal a, int count) {
		Fraction bp = BaseFunc.super.getTaylorSeries(a, count);
		if (Util.cmp(a, BigDecimal.ZERO) == -1 || a == Util.NEGATIVE_INFINITY) {
			Fraction f = Fraction.substitute(bp, new Fraction(new BasePolynom(new BaseTerm(new BigDecimal(-1), 1))));
			f = Fraction.div(new Fraction(BasePolynom.createConst(1)), f);
			return f;
		}
		return bp;
	}
	
	@Override
	public Fraction getNthTaylorTerm(int n) {
		BaseTerm num = new BaseTerm(BigDecimal.ONE, n);
		BaseTerm denom = new BaseTerm(new BigDecimal(Util.fact(n)), 0);
		return new Fraction(new BasePolynom(num), new BasePolynom(denom));
	}

}
