package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BasePolynom {

	private List<BaseTerm> terms = new ArrayList<>();
	private boolean dirty = false;
	
	public BasePolynom() {}
	
	public BasePolynom(BaseTerm... terms) {
		this.terms = Arrays.asList(terms);
	}

	public BasePolynom(BasePolynom other) {
		this.terms = other.getTerms().stream().map(t -> new BaseTerm(t)).collect(Collectors.toList());
	}
	
	public void multiply(BigDecimal d) {
		//terms.forEach(bt -> bt.setMult(bt.getMult() * d));
		terms = terms.parallelStream()
				.map(bt -> new BaseTerm(bt.getMult().multiply(d), bt.getPower()))
				.collect(Collectors.toList());
	}
	
	public void neg() {
		terms = terms.parallelStream()
				.map(bt -> new BaseTerm(bt.getMult().negate(), bt.getPower()))
				.collect(Collectors.toList());
	}
	
	public List<BaseTerm> getTerms() {
		return terms;
	}

	public void setTerms(List<BaseTerm> terms) {
		this.terms = terms;
	}
	
	public void simplify() {
		HashMap<Integer, BaseTerm> newTerms = new HashMap<>(); //termai pagal laipsni
		terms.forEach(t -> {
			int p = t.getPower();
			if (newTerms.containsKey(p)) {
				BaseTerm other = newTerms.get(p);
				t = new BaseTerm(t.getMult().add(other.getMult()), p);
			}
			newTerms.put(p, t);
		});
		setTerms(newTerms.values()
			.parallelStream()
			.filter(t -> Util.cmp(t.getMult(), new BigDecimal(0, Cfg.mc())) != 0)
			.collect(Collectors.toList()));
		if (getTerms().isEmpty()) {
			setTerms(Arrays.asList(new BaseTerm(new BigDecimal(0, Cfg.mc()), 0)));
		}
	}
	
	public Optional<BaseTerm> getLargestTerm(BigDecimal arg) {
		return terms.stream()
				.sorted((a, b) -> -BaseTerm.compareMagnitude(a, b, arg))
				.findFirst()
				.map(BaseTerm::new);
	}

	
	public BigDecimal calcValue(BigDecimal arg) {
		//arg == 0
		if (Util.cmp(arg, new BigDecimal(0, Cfg.mc())) == 0) {
			BaseTerm l = getLargestTerm(arg).get();
			if (l.getPower() == 0) return l.getMult();
			//nulis vardiklyje -> rezultatas begalybe
			/*
			uskomentinta, nes siaip power negali but neigiamas
			if (l.getPower() < 0) {
				return l.getMult().signum() == -1 ? Util.NEGATIVE_INFINITY : Util.POSITIVE_INFINITY;
			}
			*/
			return new BigDecimal(0, Cfg.mc());
		}
		if (Util.isInfinite(arg)) {
			BaseTerm l = getLargestTerm(arg).get();
			if (l.getPower() == 0) return l.getMult();
			if (l.getPower() < 0) {
				return new BigDecimal(0, Cfg.mc());
			}
			int cmp = Util.NEGATIVE_INFINITY == arg ? -1 : 1;
			if (l.getPower() % 2 == 0) cmp = 1;
			cmp = cmp * Util.cmp(l.getMult(), BigDecimal.ZERO);
			
			return cmp == 0 ? BigDecimal.ZERO : (cmp > 0 ? Util.POSITIVE_INFINITY : Util.NEGATIVE_INFINITY);
		}
		BigDecimal sum = BigDecimal.ZERO;
		for (BaseTerm bt : terms) {
			sum = sum.add(bt.getMult().multiply(arg.pow(bt.getPower())));
		}
		return sum;
	}
	
	public static BasePolynom raiseToPower(BasePolynom a, int power) {
		if (power < 0) throw new RuntimeException("Neigiamas laipsnis");
		if (power == 0) return BasePolynom.createConst(1);
		BasePolynom res = a;
		for (int i = 1; i < power; i++) {
			res = multiply(res, a);
		}
		res.simplify();
		return res;
	}
	
	public static BasePolynom multiply(BasePolynom a, BasePolynom b) {
		ArrayList<BaseTerm> newTerms = new ArrayList<>();
		for (BaseTerm at : a.terms) {
			for (BaseTerm bt : b.terms) {
				BaseTerm t = BaseTerm.multiply(at, bt);
				newTerms.add(t);
			}
		}
		
		BasePolynom bp = new BasePolynom();
		bp.setTerms(newTerms);
		bp.simplify();
		return bp;
	}
	
	public static BasePolynom multiply(BasePolynom a, BigDecimal mult) {		
		BasePolynom bp = new BasePolynom();
		List<BaseTerm> terms = a.getTerms().stream().map(bt -> {
			return new BaseTerm(bt.getMult().multiply(mult), bt.getPower());
		}).collect(Collectors.toList());
		bp.setTerms(terms);
		return bp;
	}
	
	public static BasePolynom add(BasePolynom a, BasePolynom b) {
		BasePolynom bp = new BasePolynom();
		Stream.concat(a.terms.stream(), b.terms.stream()).forEach(t -> {
			bp.getTerms().add(new BaseTerm(t));
		});
		bp.simplify();
		return bp;
	}
	
	public static BasePolynom substitute(BaseTerm bt, BasePolynom bp) {
		return multiply(raiseToPower(bp, bt.getPower()), bt.getMult());
	}
	
	public static BasePolynom substitute(BasePolynom current, BasePolynom substitution) {
		BasePolynom bp = new BasePolynom();
		for (BaseTerm bt : current.getTerms()) {
			bp.getTerms().addAll(BasePolynom.substitute(bt, substitution).getTerms());
		}
		bp.simplify();
		return bp;
	}
	
	public static BasePolynom createConst(double constValue) {
		BaseTerm bt = new BaseTerm(new BigDecimal(constValue), 0);
		BasePolynom bp = new BasePolynom();
		bp.getTerms().add(bt);
		return bp;
	}
/*
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (BaseTerm bt : terms) {
			sb.append(String.format(" %s %fx^%d", bt.getMult() > 0 ? "+" : "-", Math.abs(bt.getMult()), bt.getPower()));
		}
		return sb.toString();
	}
*/
}
