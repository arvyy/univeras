package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.util.List;

public class BaseTerm {
	
	public BaseTerm(BigDecimal mult, int power) {
		this.mult = mult;
		this.power = power;
		if (power < 0) {
			throw new Error("Neg power");
		}
	}
	
	public BaseTerm(BaseTerm other) {
		this(other.getMult(), other.getPower());
	}
	
	private final BigDecimal mult;
	private final int power;
		
	/*
	 * 4x^2 * 5x^3 -> 20x^5
	 */
	public static BaseTerm multiply(BaseTerm a, BaseTerm b) {
		BigDecimal mult = a.getMult().multiply(b.getMult(), Cfg.mc());
		int power = a.getPower() + b.getPower();
		return new BaseTerm(mult, power);
	}
	
	public static int compareMagnitude(BaseTerm a, BaseTerm b, BigDecimal arg) {
		int mult = 1;
		if (Util.isFinite(arg)) {
			mult = Util.cmp(arg.abs(Cfg.mc()), new BigDecimal(1, Cfg.mc())) == 1 ? 1 : -1;
		}
		return Integer.compare(a.getPower(), b.getPower()) * mult;
	}

	public BigDecimal getMult() {
		return mult;
	}

	public int getPower() {
		return power;
	}
	
	@Override
	public String toString() {
		return String.format("%f x ^ %d", mult, power);
	}
	
}
