package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Cfg {
	
	// skaiciavimams
	public static MathContext mc() {
		return MathContext.UNLIMITED;
	}
	
	// rezultatui
	public static MathContext resultMc() {
		return MathContext.DECIMAL128;
		//return new MathContext(1000);
	}
	
	public static BigDecimal delta() {
		return new BigDecimal("0.001");
	}
	
	public static int inverseScale() {
		return 100;
	}
	
	public static int resultScale() {
		return 3;
	}
	
}
