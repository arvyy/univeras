package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PolynomFunc implements Func {

	//power -> mult
	private Map<Integer, BigDecimal> multiplier = new HashMap<>();

	public void set(int power, BigDecimal mult) {
		multiplier.put(power, mult);
	}
	
	@Override
	public Fraction getTaylorSeries(BigDecimal a, int count) {
		BasePolynom bp = new BasePolynom();
		for (int p : multiplier.keySet()) {
			BaseTerm bt = new BaseTerm(multiplier.get(p), p);
			bp.getTerms().add(bt);
		}
		return new Fraction(bp);
	}

	public Map<Integer, BigDecimal> getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(Map<Integer, BigDecimal> multiplier) {
		this.multiplier = multiplier;
	}

}
