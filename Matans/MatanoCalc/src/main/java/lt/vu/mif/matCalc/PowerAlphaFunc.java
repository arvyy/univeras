package lt.vu.mif.matCalc;

import java.math.BigDecimal;

public class PowerAlphaFunc implements BaseFunc {

	private BigDecimal a;
	
	public void setA(BigDecimal a) {
		this.a = a;
	}
	
	public BigDecimal getA() {
		return a;
	}
	
	@Override
	public Fraction getTaylorSeries(BigDecimal arg, int count) {
		if (Util.cmp(Util.abs(arg), new BigDecimal(1)) != 1 || Util.isInfinite(arg)) {
			return BaseFunc.super.getTaylorSeries(arg, count);
		}
		BigDecimal out = new BigDecimal( Math.pow(arg.doubleValue(), a.doubleValue()));
		BigDecimal newArg = arg.add(BigDecimal.ONE).divide(arg, Cfg.resultMc()).subtract(BigDecimal.ONE);
		Fraction f = BaseFunc.super.getTaylorSeries(newArg, count);
		f = Fraction.substitute(f, new Fraction(BasePolynom.createConst(1), new BasePolynom(new BaseTerm(BigDecimal.ONE, 1))));
		f = Fraction.mul(f, out);
		f.simplify();
		return f;
	}
	
	@Override
	public Fraction getNthTaylorTerm(int n) {
		BigDecimal num_d = new BigDecimal(1);
		for (int i = 0; i < n; i++) {
			num_d = num_d.multiply(a.subtract(new BigDecimal(i)));
		}
		BigDecimal denom_d = new BigDecimal(Util.fact(n));
		BaseTerm num = new BaseTerm(num_d, n);
		BaseTerm denom = new BaseTerm(denom_d, 0);
		return new Fraction(new BasePolynom(num), new BasePolynom(denom));
	}

}
