package lt.vu.mif.matCalc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CompositionFunc implements TransformableToFrac {

	private TransformableToFrac outterFunc;
	private TransformableToFrac innerFunc;
	
	@Override
	public Fraction transform(BigDecimal a) {
		Fraction inner = innerFunc.transform(a);
		Fraction outter = outterFunc.transform(inner.getLim(a));
		return Fraction.substitute(outter, inner);
	}

	public TransformableToFrac getOutterFunc() {
		return outterFunc;
	}

	public void setOutterFunc(TransformableToFrac outterFunc) {
		this.outterFunc = outterFunc;
	}

	public TransformableToFrac getInnerFunc() {
		return innerFunc;
	}

	public void setInnerFunc(TransformableToFrac innerFunc) {
		this.innerFunc = innerFunc;
	}

}
