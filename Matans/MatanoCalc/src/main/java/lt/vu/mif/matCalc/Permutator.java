package lt.vu.mif.matCalc;

public class Permutator {
	
	private static ThreadLocal<Permutator> p = new ThreadLocal<>();
	
	public static void set(int n) {
		p.set(new Permutator(n));
	}
	
	public static Permutator getPermutator() {
		return p.get();
	}
	
	private int maxVal;
	private int permNr = 0;
	private int posNr = 0;
	private int length;
	
	private Permutator(int maxVal) {
		this.maxVal = maxVal;
	}
	
	public void nextPerm() {
		length = posNr;
		posNr = 0;
		permNr++;
	}
	
	public int get() {
		int val = Math.floorDiv(permNr, (int) Math.pow(maxVal, posNr)) % maxVal;
		posNr++;
		return val;
	}
	
	public int getLength() {
		return length;
	}
	
}
