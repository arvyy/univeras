angular.module('myApp', ['ngMaterial'])
  .config(function ($httpProvider) {
    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};
  })

  .controller('controller', ["$scope", "$rootScope", "$http", function ($scope, $rootScope, $http) {
    $scope.customPoint = 0;
    $scope.getCustPoint = function () {
      return $scope.customPoint;
    }
    $scope.points = [
      {
        name: "-∞",
        number: 1,
        tooltip: "Riba bus skaičiuojama funkcijai artėjant į minus begalybę",
        class: 'md-raised',
        symbol: "neg_inf"
      },
      {
        name: "Pasirinktinis",
        number: 2,
        tooltip: "Riba bus skaičiuojama funkcijai artėjant į pasirinktą tašką",
        class: 'md-raised md-warn',
        symbol: $scope.getCustPoint()
      },
      {
        name: "+∞",
        number: 3,
        tooltip: "Riba bus skaičiuojama funkcijai artėjant į plius begalybę",
        class: 'md-raised',
        symbol: "pos_inf"
      }
    ]

    $scope.pointNumber = 2;
    $scope.expression = "\\sin{cos{x}}";
    $scope.operations = [
      {
        name: "sin(x)",
        expr: "\\sin{x}",
        tooltip: "Argumentą veskite tarp riestinių skliaustų"
      },
      {
        name: "cos(x)",
        expr: "\\cos{x}",
        tooltip: "Argumentą veskite tarp riestinių skliaustų"
      },
      {
        name: "Exp(x)",
        expr: "{e^x}",
        tooltip: "Eksponentė pakelta x-uoju laipsniu"
      },
      {
        name: "arcTg(x)",
        expr: "\\arctan{x}",
        tooltip: "Argumentą veskite tarp riestinių skliaustų"
      },
      {
        name: "Ln(1+x)",
        expr: "\\ln{x}",
        tooltip: "Vardiklį veskite tarp riestinių skliaustų"
      },
      {
        name: "Polinomas",
        expr: "\\pol{x}+{y^2}+{z^3}",
        tooltip: "Polinomo kintamuosius rašykite laipsnių didėjimo tvarka"
      }
    ]

    $scope.casualOps = [
      {
        name: "Sudėtis",
        expr: "+",
        tooltip: "Paprasta sudėtis"
      },
      {
        name: "Atimtis",
        expr: "-",
        tooltip: "Paprasta atimtis"
      },
      {
        name: "Daugyba",
        expr: "\\times{x}",
        tooltip: "Daugiklį veskite tarp riestinių skliaustų"
      },
      {
        name: "Dalyba",
        expr: "\\div{x}",
        tooltip: "Daliklį veskite tarp riestinių skliaustų"
      }
    ]

    var me = this
    $scope.limUrl = $scope.customPoint

    var math = document.getElementById("reiskinys");

    $scope.changePoint = function (point) {
      $scope.pointNumber = point.number;
      console.log($scope.pointNumber);
      console.log($scope.points);
      if ($scope.pointNumber == 2) {
        $scope.limUrl = $scope.customPoint
      } else {
        $scope.limUrl = point.symbol;
      }
      for (var i = 1; i < 4; i++) {
        console.log($scope.pointNumber);
        if (i == $scope.pointNumber) {
          $scope.points[i - 1].class = 'md-raised md-warn';
        } else {
          $scope.points[i - 1].class = 'md-raised';
        }
      }
    }

    $scope.getLimPoint = function () {
      if ($scope.pointNumber == 1) {
        return 'neg_inf'
      }
      if ($scope.pointNumber == 2) {
        return $scope.customPoint
      }
      if ($scope.pointNumber == 3) {
        return 'pos_inf'
      }
    }

    $scope.addOperation = function (op, ev) {
      $scope.expression += ' ' + op.expr
      var math = MathJax.Hub.getAllJax("MathDiv")[0];
      MathJax.Hub.Queue(["Text", math, $scope.expression]);
    }

    $scope.addExpr = function () {
      console.log($rootScope.addOp)
      var op = $rootScope.addOp;
      console.log(op);
      $rootScope.exprObj.push({
        name: op.name,
        argument: $scope.argumentOne,
        expression: op.expr + argument + '}'
      })

      console.log($scope.exprObj);
    }

    $scope.change = function () {
      console.log($scope.expression);
      var math = MathJax.Hub.getAllJax("MathDiv")[0];
      MathJax.Hub.Queue(["Text", math, $scope.expression]);
    }

    $scope.pointChange = function () {

    }

    $scope.createExpression = function () {
      $scope.exprToJson();
    }

    $scope.cancel = function () {
      $mdDialog.cancel();
    };

    $scope.exprToJson = function () {
      var arr = $scope.expression.split(" ");
      for (var i = 0; i < arr.length; i++) {
        ret = $scope.splitter(arr[i])
      }
      return ret;
    }

    $scope.splitter = function (string) {
      var tempArr = [];
      for (var i = 0; i < string.length; i++) {
        var backslash = 0;
        if (string[0] == "\\") {
          backslash = 1;
        }
        if (string[i] == "{") {
          tempArr[0] = string.slice(0 + backslash, i);
          tempArr[1] = string.slice(i + 1, string.length - 1);
          for (var j = 0; j < tempArr[1].length; j++) {
            if (tempArr[1][j] == "{") {
              if ($scope.splitter(tempArr[1]) == "x") {
                tempArr[1] = [];
              } else {
                tempArr[1] = $scope.splitter(tempArr[1]);
              }
            }
          }
          return $scope.jsonMaker(tempArr);
        }
        backslash = 0;
      }
    }

    $scope.getPoly = function getPol(pol) {
      var polynome = pol.split("pol");
      var polynome = pol.split("{");
      var polynomeArr = [];
      for (var i = 0; i < polynome.length; i++) {
        polynome[i] = polynome[i].split("^");
      }
      polynome[1] = polynome[1][0].split("}");
      polynome
      for (var i = 1; i < polynome.length; i++) {
        polynome[i] = polynome[i][0];
        polynomeArr[i - 1] = polynome[i];
      }
      var obj = {
        name: "Polynome",
        args: polynomeArr
      }
      return obj;
    }

    $scope.makeRequest = function () {
      console.log($scope.customPoint);
      var dataObj = $scope.exprToJson();
      $http({
        method: 'POST',
        url: 'http://localhost:8080/lim?value=' + $scope.getLimPoint(),
        data: dataObj,
        contentType: "application/json; charset=utf-8",
        crossDomain: true,
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': true
        },
        dataType: "json",
      }).then(function successCallback(response) {
        console.log("success");
        alert("Įvesto reiškinio riba: " + response.data);
        console.log(response.data);
      }, function errorCallback(response) {
        console.log("fail");
        console.log(response);
      });
    }

    $scope.isExp = function (string) {
      for (var i = 0; i < string.length; i++) {
        if (string[i] == '^') {
          console.log("EKSPONENTE");
          return true;
        }
      }
      return false;
    }

    $scope.isLn = function (string) {
      for (var i = 0; i < string[1].length; i++) {
        if (string[1] == 'l' && string[2] == 'n') {
          console.log("Naturinis logaritmas");
          return true;
        }
      }
      return false;
    }

    $scope.simplyArgs = function (args) {
      return args.split(",");
    }

    $scope.jsonMaker = function (obj) {
      var args = []
      if (obj[0] == "pol") {
        console.log($scope.getPol(obj[1]));
      }
      console.log($scope.simplyArgs(obj[1]));
      obj[1] = $scope.simplyArgs(obj[1]);
      if ($scope.isExp(obj[1])) {
        console.log(obj[1]);
        var exp = obj[1].split("^");
        obj[0] = "exp";
        obj[1] = exp[1];
      }
      if (obj[1] == "x") {
        args.push()
      } else {
        args.push(obj[1]);
      }
      if (obj[0] == "arctan") {
        obj[0] = "arctg";
      }
      var returnableObj = {
        name: obj[0],
        arguments: args
      }
      console.log(returnableObj);
      return returnableObj;
    }
  }]);