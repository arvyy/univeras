package lt.vu.mif.matanaslimcalcrest;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import jdk.nashorn.api.scripting.ScriptObjectMirror;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import lt.vu.mif.matCalc.AddFunc;
import lt.vu.mif.matCalc.ArctgFunc;
import lt.vu.mif.matCalc.Calculator;
import lt.vu.mif.matCalc.CompositionFunc;
import lt.vu.mif.matCalc.CosFunc;
import lt.vu.mif.matCalc.DivFunc;
import lt.vu.mif.matCalc.ExpFunc;
import lt.vu.mif.matCalc.Func;
import lt.vu.mif.matCalc.LnFunc;
import lt.vu.mif.matCalc.MulFunc;
import lt.vu.mif.matCalc.PolynomFunc;
import lt.vu.mif.matCalc.SinFunc;
import lt.vu.mif.matCalc.TransformableToFrac;

@SpringBootApplication
public class MatanasLimCalcRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MatanasLimCalcRestApplication.class, args);
	}
	
	@Bean
	Calculator calc() {
		return new Calculator();
	}
}
