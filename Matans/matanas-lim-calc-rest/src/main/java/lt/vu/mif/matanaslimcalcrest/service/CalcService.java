package lt.vu.mif.matanaslimcalcrest.service;

import java.math.BigDecimal;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lt.vu.mif.matCalc.AddFunc;
import lt.vu.mif.matCalc.ArctgFunc;
import lt.vu.mif.matCalc.Calculator;
import lt.vu.mif.matCalc.CompositionFunc;
import lt.vu.mif.matCalc.CosFunc;
import lt.vu.mif.matCalc.DivFunc;
import lt.vu.mif.matCalc.ExpFunc;
import lt.vu.mif.matCalc.Fraction;
import lt.vu.mif.matCalc.Func;
import lt.vu.mif.matCalc.LnFunc;
import lt.vu.mif.matCalc.MulFunc;
import lt.vu.mif.matCalc.PolynomFunc;
import lt.vu.mif.matCalc.PowerAlphaFunc;
import lt.vu.mif.matCalc.SinFunc;
import lt.vu.mif.matCalc.TransformableToFrac;
import lt.vu.mif.matCalc.Util;
import lt.vu.mif.matanaslimcalcrest.domain.CalcRequestFunction;

@Service
public class CalcService {

	@Autowired
	Calculator calc;

	public String getLim(CalcRequestFunction f, String value) {
		BigDecimal arg = null;
		if ("neg_inf".equals(value)) {
			arg = Util.NEGATIVE_INFINITY;
		} else if ("pos_inf".equals(value)) {
			arg = Util.POSITIVE_INFINITY;
		} else {
			arg = new BigDecimal(value);
		}
		TransformableToFrac func = parseCalcRequestFunction(f);
		double result = calc.getLim(func, arg);
		if (Double.isNaN(result)) return "Ribos nėra";
		if (result == Double.POSITIVE_INFINITY) return "+ begalybe";
		if (result == Double.NEGATIVE_INFINITY) return "- begalybe";
		return String.valueOf(result);
	}
	
	private TransformableToFrac parseCalcRequestFunction(CalcRequestFunction f) {
		if (f.getName().getArgCount() == 1) return parseSingleArgCalcRequestFunction(f);
		return parseMultiArgCalcRequestFunction(f);
	}
	
	private TransformableToFrac parseMultiArgCalcRequestFunction(CalcRequestFunction f) {
		TransformableToFrac a = parseCalcRequestFunction(f.getArguments().get(0));
		TransformableToFrac b = parseCalcRequestFunction(f.getArguments().get(1));
		switch (f.getName()) {
		case add : 
			AddFunc add = new AddFunc();
			add.setA(a);
			add.setB(b);
			return add;
		case sub :
			AddFunc sub = new AddFunc();
			sub.setA(a);
			sub.setB(b);
			sub.setNegB(true);
			return sub;
		case mul:
			MulFunc mul = new MulFunc();
			mul.setA(a);
			mul.setB(b);
			return mul;
		case div:
			DivFunc div = new DivFunc();
			div.setA(a);
			div.setB(b);
			return div;
		default: throw new RuntimeException();
		}
	}

	private TransformableToFrac parseSingleArgCalcRequestFunction(CalcRequestFunction f) {
		Func tr = null;
		switch (f.getName()) {
		case sin : tr = new SinFunc(); break;
		case cos : tr = new CosFunc(); break;
		case arctg : tr = new ArctgFunc(); break;
		case exp : tr = new ExpFunc(); break;
		case ln : tr = new LnFunc(); break;
		case powAlpha : 
			PowerAlphaFunc pa = new PowerAlphaFunc();
			pa.setA(new BigDecimal(f.getPower().get(0)));
			tr = pa;
			break;
		case pol :
			PolynomFunc pol = new PolynomFunc();
			for (int i = 0; i < f.getPower().size(); i++) {
				pol.set(i, new BigDecimal(f.getPower().get(i)));
			}
			tr = pol;
			break;
		default: throw new RuntimeException();
		}
		if (f.getArguments() == null || f.getArguments().isEmpty()) {
			return tr;
		}
		CompositionFunc comp = new CompositionFunc();
		comp.setOutterFunc(tr);
		comp.setInnerFunc(parseCalcRequestFunction(f.getArguments().get(0)));
		return comp;
	}
}
