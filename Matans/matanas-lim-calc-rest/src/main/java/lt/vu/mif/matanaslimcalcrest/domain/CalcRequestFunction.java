package lt.vu.mif.matanaslimcalcrest.domain;

import java.util.List;

public class CalcRequestFunction {
	
	public enum FuncName {
		sin(1),
		cos(1),
		exp(1),
		arctg(1),
		ln(1),
		pol(1),
		powAlpha(1),
		
		add(2),
		sub(2),
		mul(2),
		div(2);
		//TODO
		
		int argcount;
		FuncName(int argcount) {
			this.argcount = argcount;
		}
		public int getArgCount() {
			return argcount;
		}
	}
	
	private FuncName name;
	private List<CalcRequestFunction> arguments;
	private List<Double> power;
	
	public FuncName getName() {
		return name;
	}
	public void setName(FuncName name) {
		this.name = name;
	}
	public List<CalcRequestFunction> getArguments() {
		return arguments;
	}
	public void setArguments(List<CalcRequestFunction> arguments) {
		this.arguments = arguments;
	}
	public List<Double> getPower() {
		return power;
	}
	public void setPower(List<Double> power) {
		this.power = power;
	}
}
