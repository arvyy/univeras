package lt.vu.mif.matanaslimcalcrest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lt.vu.mif.matanaslimcalcrest.domain.CalcRequestFunction;
import lt.vu.mif.matanaslimcalcrest.service.CalcService;

@RestController("rest")
public class CalcController {

	@Autowired CalcService service;	
	@CrossOrigin(origins = "http://localhost:8081")
	@PostMapping("lim")
	public String getLim(@RequestBody CalcRequestFunction func, @RequestParam("value") String value) {
		return service.getLim(func, value);
	}
	
}
