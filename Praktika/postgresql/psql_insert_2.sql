insert into Darbuotojas (id, vardas)
values
	(1, 'D1'),
	(2, 'D2'),
	(3, 'D3'),
	(4, 'D4'),
	(5, 'D5');

insert into Dokumentas (id, pavadinimas)
values
	(1, 'DOC1'),
	(2, 'DOC2'),
	(3, 'DOC3'),
	(4, 'DOC4'),
	(5, 'DOC5');

insert into Tipas (id, pavadinimas)
values
	(1, 'T1'),
	(2, 'T2'),
	(3, 'T3'),
	(4, 'T4');

insert into Role (id, pavadinimas)
values
	(1, 'R1'),
	(2, 'R2'),
	(3, 'R3');

insert into VadovasBriauna (id, virsune_is, virsune_i)
values
	(1, 1, 1),
	(2, 1, 3),
	(3, 2, 4),
	(4, 2, 5),
	(5, 3, 5);

insert into DarbMatoDokumentaBriauna(id, virsune_is, virsune_i)
values
	(1, 5, 3);

insert into DarbMatoTipaBriauna (id, virsune_is, virsune_i)
values
	(1, 4, 2),
	(2, 5, 2);

insert into TuriTipaBriauna(id, virsune_is, virsune_i)
values 
	(1, 1, 1),
	(2, 2, 1),
	(3, 2, 2),
	(4, 3, 3),
	(5, 4, 4),
	(6, 5, 2),
	(7, 5, 3);

insert into RoleMatoTipaBriauna (id, virsune_is, virsune_i)
values
	(1, 1, 1),
	(2, 1, 2),
	(3, 1, 3),
	(4, 1, 4),
	(5, 2, 1),
	(6, 3, 1),
	(7, 3, 2),
	(8, 3, 3);

insert into TuriRoleBriauna(id, virsune_is, virsune_i)
values
	(1, 1, 1),
	(2, 2, 3),
	(3, 3, 3),
	(4, 4, 2);
