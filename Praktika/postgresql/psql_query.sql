select dok.* from (
	
	with darbuotojas as (
		select v.* from Virsunes as v, Darbuotojas as d
		where d.vardas = 'D1' and v.detales = d.id
	),

	tipai as (
		select tipasv.* from Virsunes as tipasv, Briaunos as matob, darbuotojas
		where matob.tipas = 'Mato' and matob.virsune_is = darbuotojas.id and matob.virsune_i = tipasv.id and tipasv.tipas = 'Tipas'
		union
		select tipasv.* from Virsunes as tipasv, Briaunos as turiroleb, Briaunos as matob, Virsunes rolev, darbuotojas
		where 
			turiroleb.virsune_is = darbuotojas.id and turiroleb.virsune_i = rolev.id and turiroleb.tipas = 'TuriRole' and rolev.tipas = 'Role' and
			matob.virsune_is = rolev.id and matob.virsune_i = tipasv.id and matob.tipas = 'Mato' and tipasv.tipas = 'Tipas'
	)

	select d.* from Virsunes as dokv, Dokumentas as d, tipai, Briaunos as turitipab
	where turitipab.tipas = 'TuriTipa' and turitipab.virsune_i = tipai.id and turitipab.virsune_is = dokv.id and dokv.tipas = 'Dokumentas' and dokv.detales = d.id
	union
	select d.* from Virsunes as dokv, Dokumentas as d, Briaunos as matob, darbuotojas
	where matob.virsune_is = darbuotojas.id and matob.virsune_i = dokv.id and matob.tipas = 'Mato' and dokv.detales = d.id


) as dok
where dok.pavadinimas like '%DOC%';
