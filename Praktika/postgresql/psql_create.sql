drop table Virsunes cascade;
drop table Briaunos cascade;
drop table Darbuotojas cascade;
drop table Tipas cascade;
drop table Role cascade;
drop table Dokumentas cascade;

create table Virsunes (
	id int primary key not null,
	tipas char(20) not null,
	detales int
);

create table Briaunos (
	id int primary key not null,
	tipas char(20) not null,
	detales int,
	virsune_is int references Virsunes,
	virsune_i int references Virsunes	
);

create table Darbuotojas (
	id int primary key not null,
	vardas char(20) not null
);

create table Tipas (
	id int primary key not null,
	pavadinimas char(20) not null
);

create table Role (
	id int primary key not null,
	pavadinimas char(20) not null
);

create table Dokumentas (
	id int primary key not null,
	pavadinimas char(20) not null
);
