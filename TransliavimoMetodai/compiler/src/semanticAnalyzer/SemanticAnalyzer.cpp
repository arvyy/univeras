#include <SemanticAnalyzer.hpp>
#include <map>
#include <fstream>
#include "Declarations.hpp"
#include "ASTParser.hpp"


using namespace std;
using namespace ASCompiler;

SemanticAnalyzer::SemanticAnalyzer(string lexemesFile, string statesFile, string rulesFile)
	: lexer(lexemesFile, statesFile), parser(rulesFile) {
		parser.setLexer(lexer);
	}

void SemanticAnalyzer::buildIR(string fileName, IR& out) {
	ifstream in(fileName.c_str());
	lexer.setInputStream(in);
	Symbol root;
	try {
		root = parser.getRootSymbol();
	} catch (ParserException pe) {
		SemanticException se;
		se.type = SemanticException::PARSER_EXCEPTION;
		se.message = "Parser exception: " + pe.message + ", context: " + pe.context;
		throw se;
	}
	IRNameSupplier irn;
	AST ast = ASCompiler::parseASTFromSymbol(root, irn);
	ast.writeIR(out, irn);
}
