#include "IRNameSupplier.hpp"
#include <sstream>

using namespace std;
using namespace ASCompiler;

namespace {
	const string labelName = "l";
	const string tempName = "t";
	const string scopeName = "s";
}

string IRNameSupplier::getLabel() {
	stringstream s;
	s << labelName << label++;
	return s.str();
}

string IRNameSupplier::getTemp() {
	stringstream s;
	s << tempName << temp++;
	return s.str();
}

string IRNameSupplier::getScope() {
	stringstream s;
	s << scopeName << scope++;
	return s.str();
}

