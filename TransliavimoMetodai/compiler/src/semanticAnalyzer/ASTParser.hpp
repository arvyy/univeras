#ifndef AS_ASTPARSER_HPP
#define AS_ASTPARSER_HPP

#include <Parser.hpp>
#include "IRNameSupplier.hpp"
#include "AST.hpp"

namespace ASCompiler {
	
	AST parseASTFromSymbol(Symbol& z, IRNameSupplier& irn);

}

#endif
