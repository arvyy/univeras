#ifndef AS_DECLARATIONS_HPP
#define AS_DECLARATIONS_HPP

#include <string>
#include <vector>
#include <utility> //pair
#include <Parser.hpp>

namespace ASCompiler {
	
	struct VarType {

		VarType();

		bool operator==(const VarType& other) const;
		bool operator!=(const VarType& other) const;

		enum BaseType {
			INT,
			FLOAT,
			CHAR,
			STRUC,

			UNDEFINED
		} base;
		std::string strucName;
		int arrSize;
		enum Mod {
			ARR,
			REF
		};
		
		// galima tik:
		// (none)
		// ?
		// []
		// ?[]
		// []?
		std::vector<Mod> mods;

		bool isRef() {
			return mods.size() > 0 && mods[mods.size() - 1] == Mod::REF;
		}

		bool isArr() {
			return mods.size() > 0 && mods[mods.size() - 1] == Mod::ARR;
		}
	};

	struct FuncDeclaration {
		std::string name;
		bool isProc;
		bool isPrivate;
		bool isExternallyDefined;
		VarType returnType;
		std::vector<VarType> arguments;
	};

	struct StrucDeclaration {
		bool isPrivate;
		bool isExternallyDefined;
		struct StrucMember {
			std::string name;
			VarType type;
			int offset;
		};
		std::string name;
		std::vector<StrucMember> members;
		StrucMember* get(std::string name);
		int size = 0;
	};

		
}

#endif
