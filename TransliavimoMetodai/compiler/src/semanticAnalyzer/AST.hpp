#ifndef AS_AST_HPP
#define AS_AST_HPP

#include <string>
#include <vector>
#include <IntermediateRepresentation.hpp>
#include "IRNameSupplier.hpp"
#include "SymbolTable.hpp"
#include "Declarations.hpp"

namespace ASCompiler {
		
	struct ASTNode {
		ASTNode(SymbolTable& st);
		SymbolTable* table;
		virtual void writeIR(
			IR& ir, 
			IRNameSupplier& irNameSupplier);
	};

	struct ASTVarDeclNode : public ASTNode {
		using ASTNode::ASTNode;
		std::string varName;
		VarType type;
	};

	struct ASTFuncNode : public ASTNode {
		using ASTNode::ASTNode;
		std::vector<ASTNode*> block;
		std::vector<VarType> args;
		bool isProc;
		bool isPrivate;
		bool isExternallyDefined;
		std::string name;
		VarType returnType;
		void writeIR(IR& ir, IRNameSupplier& irNameSupplier);
	};

	struct ASTValueNode : public ASTNode {
		using ASTNode::ASTNode;
		VarType valueType;
		std::string tempVarName;
		virtual void eval(){};
	};

	struct ASTFloatConstValue : public ASTValueNode {
		using ASTValueNode::ASTValueNode;
		virtual void writeIR(IR& ir, IRNameSupplier& irNameSupplier);
		float value;
		void eval();
	};

	struct ASTIntConstValue : public ASTValueNode {
		using ASTValueNode::ASTValueNode;
		virtual void writeIR(IR& ir, IRNameSupplier& irNameSupplier);
		int value;
		void eval();
	};

	struct ASTCharConstValue : public ASTValueNode {
		using ASTValueNode::ASTValueNode;
		//virtual void writeIR(IR& ir, IRNameSupplier& irNameSupplier);
		char value;
		//void eval();
	};

	struct ASTCastValue : public ASTValueNode {
		using ASTValueNode::ASTValueNode;
		VarType castType;
		ASTValueNode* node;
	};

	struct ASTBinaryOperator : public ASTValueNode {
		using ASTValueNode::ASTValueNode;
		virtual void writeIR(IR& ir, IRNameSupplier& irNameSupplier);
		enum Op {
			PLUS,
			MINUS,
			MUL,
			DIV,
			AND,
			OR,
			GT,
			GTE,
			LT,
			LTE,
			EQ,
			NEQ
		} op;
		ASTValueNode* left;
		ASTValueNode* right;
		void eval();
	};

	struct ASTUnaryOperator : public ASTValueNode {
		using ASTValueNode::ASTValueNode;
		virtual void writeIR(IR& ir, IRNameSupplier& irNameSupplier);
		enum Op {
			NOT,
			CHECK_NIL,
			ARR_SIZE
		} op;
		ASTValueNode* node;
		void eval();
	};

	struct ASTVariable : public ASTValueNode {
		using ASTValueNode::ASTValueNode;
		//void eval();
	};

	struct ASTSimpleVar : public ASTVariable {
		using ASTVariable::ASTVariable;
		virtual void writeIR(IR& ir, IRNameSupplier& irNameSupplier);
		std::string name;
		void eval();
	};

	struct ASTStrucMemberAccess : public ASTVariable {
		using ASTVariable::ASTVariable;
		//virtual void writeIR(IR& ir, IRNameSupplier& irNameSupplier);
		ASTVariable* strucVar;
		std::string member;
		//void eval();
	};

	struct ASTArrayEl : public ASTVariable {
		using ASTVariable::ASTVariable;
		//virtual void writeIR(IR& ir, IRNameSupplier& irNameSupplier);
		ASTVariable* arrVar;
		ASTValueNode* index;
		//void eval();
	};

	struct ASTValueAssign : public ASTNode {
		using ASTNode::ASTNode;
		virtual void writeIR(IR& ir, IRNameSupplier& irNameSupplier);
		ASTVariable* left;
		ASTValueNode* rightNode;
		std::string rightString;
		enum Type {
			VAL,
			STRING
		} type;
	};

	struct ASTRefAssign : public ASTNode {
		using ASTNode::ASTNode;
		//using ASTNode::writeIR;
		ASTVariable* left;
		ASTVariable* right;
		ASTValueNode* shift;
		bool shiftRight;
	};

	struct ASTFuncCallNode : ASTValueNode {
		using ASTValueNode::ASTValueNode;
		//using ASTNode::writeIR;
		std::string name;
		std::vector<ASTValueNode*> args;
		//void eval();
	};

	struct ASTProcCallNode : public ASTNode {
		using ASTNode::ASTNode;
		//using ASTNode::writeIR;
		std::string name;
		std::vector<ASTValueNode*> args;
	};

	struct ASTElseIfNode : public ASTNode {
		using ASTNode::ASTNode;
		//using ASTNode::writeIR;
		ASTValueNode* condition;
		std::vector<ASTNode*> block;
	};

	struct ASTIfNode : public ASTNode {
		using ASTNode::ASTNode;
		//using ASTNode::writeIR;
		ASTValueNode* condition;
		std::vector<ASTElseIfNode*> elseIfs;	
		std::vector<ASTNode*> block;
		std::vector<ASTNode*> elseBlock;
	};

	struct ASTWhileNode : public ASTNode {
		using ASTNode::ASTNode;
		//using ASTNode::writeIR;
		std::string loopBeginLabel;
		std::string loopEndLabel;
		ASTValueNode* condition;
		std::vector<ASTNode*> block;
	};

	struct ASTBreakNode : public ASTNode {
		using ASTNode::ASTNode;
		//using ASTNode::writeIR;
		ASTWhileNode* loop;
	};

	struct ASTReadNode : public ASTNode {
		using ASTNode::ASTNode;
		ASTVariable* var;
	};


	struct ASTWriteNode : public ASTNode {
		using ASTNode::ASTNode;
		//using ASTNode::writeIR;
		enum Type {
			STR,
			VALUE
		} type;
		ASTValueNode* value;
		std::string str;
	};

	struct ASTOutNode : public ASTNode {
		using ASTNode::ASTNode;
		//using ASTNode::writeIR;
		ASTValueNode* value;
	};

	class AST {
		public:
			AST();
			~AST();
			void writeIR(IR& out, IRNameSupplier& irn);
			SymbolTableContainer& getSTC();
			void addFuncNode(ASTFuncNode* node);
		private:
			std::vector<ASTFuncNode*> funcNodes;
			SymbolTableContainer stc;
	};

}

#endif
