#include "SymbolTable.hpp"
#include <SemanticAnalyzer.hpp>

using namespace std;
using namespace ASCompiler;

SymbolTableEntry::SymbolTableEntry() {}

SymbolTableEntry::SymbolTableEntry(std::string _name, VarType* _varType)
	: name(_name), varType(_varType), type(VAR) {}

SymbolTableEntry::SymbolTableEntry(std::string _name, FuncDeclaration* fdecl)
	: name(_name), funcDeclaration(fdecl), type(FUNC) {}

SymbolTableEntry::SymbolTableEntry(std::string _name, StrucDeclaration* sdecl)
		: name(_name), strucDeclaration(sdecl), type(STRUC) {}

SymbolTable::SymbolTable(SymbolTableContainer* _stc, string _name, SymbolTable* _parent) :
	stc(_stc), name(_name), parentScope(_parent) 
{}

SymbolTable::SymbolTable(){}

SymbolTableEntry* SymbolTable::getSymbolTableEntry(string name) {
	if (entries.find(name) != entries.end()) {
		return &entries[name];
	} else if (parentScope != NULL) {
		return parentScope -> getSymbolTableEntry(name);
	} else {
		return NULL;
	}
}

SymbolTable* SymbolTable::createNewScope(string name) {
	SymbolTable* t = new SymbolTable(stc, name, this);
	stc->addScopeTable(t);
	return t;
}

vector<SymbolTableEntry*> SymbolTable::getSymbolTableEntries() {
	vector<SymbolTableEntry*> entries_v;
	for (auto& pair : entries) {
		//TODO cia gerai? 
		entries_v.push_back(&pair.second);
	}
	return entries_v;
}

void SymbolTable::addSymbolEntry(SymbolTableEntry e) {
	if (e.name != "" && getSymbolTableEntry(e.name) == NULL) {
		entries[e.name] = e;
	} else {
		//TODO
		SemanticException e;
		e.message = "Var was already declared";
		throw e;
	}
}

string SymbolTable::getName() {
	return name;
}

SymbolTableContainer::SymbolTableContainer() 
	: globalTable(this, "global", NULL)
{}

SymbolTable* SymbolTableContainer::getGlobalTable() {
	return &globalTable;
}

SymbolTable* SymbolTableContainer::getScopeTable(string scopeName) {
	if (scopeTables.find(scopeName) == scopeTables.end()) {
		return NULL;
	}
	return scopeTables[scopeName];
}

void SymbolTableContainer::addScopeTable(SymbolTable* table) {
	scopeTables[table->getName()] = table;	
}
