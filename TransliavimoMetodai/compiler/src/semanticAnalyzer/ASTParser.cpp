#include "ASTParser.hpp"
#include <SemanticAnalyzer.hpp>

using namespace ASCompiler;
using namespace std;

namespace {
	IRNameSupplier irn;

	void throwSE(string message) {
		SemanticException se;
		se.message = message;
		throw se;
	}

	ASTNode* parseFuncImplStatement(Symbol& funcImplStatement, SymbolTable& table);
	ASTValueNode* parseValueNode(Symbol& value, SymbolTable& table);

	VarType parseVarType(Symbol& variableType) {
		VarType type;
		string baseType = variableType.ntvalue[0].ntvalue[0].tvalue;
		if (baseType == "int") {
			type.base = VarType::INT;
		} else if (baseType == "float") {
			type.base = VarType::FLOAT;
		} else if (baseType == "char") {
			type.base = VarType::CHAR;
		} else {
			type.base = VarType::STRUC;
			type.strucName = baseType;
		}
		for (int i = 1; i < variableType.ntvalue.size();) {
			Symbol& s = variableType.ntvalue[i];
			if (s.name == "$[]") {
				type.mods.push_back(VarType::ARR);
				i++;
			} else if (s.name == "$?") {
				type.mods.push_back(VarType::REF);
				i++;
			} else if (s.name == "$[") {
				type.mods.push_back(VarType::ARR);
				type.arrSize = stoi(variableType.ntvalue[i+1].tvalue);
				i+=3;
			} else {
				SemanticException se;
				se.message = "Unknown var mod " + s.name;
				throw se;
			}
		}
		int arrModCount = 0;
		int refModCount = 0;
		bool lastArr = false;
		for (auto mod : type.mods) {
			if (mod == VarType::ARR) {
				arrModCount++;
				lastArr = true;
			}
			if (mod == VarType::REF) {
				refModCount++;
				lastArr = false;
			}
		}
		if (arrModCount > 1 || refModCount > 1) {
			SemanticException e;
			e.message = "Wrong var type mods - can only be ?, [], []?, ?[]";
			throw e;
		}
		if (lastArr && type.arrSize < 1) {
			SemanticException e;
			e.message = "Bad array size";
			throw e;
		}
		return type;
	}



	//value.name == "variable"
	ASTVariable* parseVariable(Symbol& var, SymbolTable& table) {
		ASTVariable *val;
		ASTSimpleVar *base = new ASTSimpleVar(table);
		base->name = var.ntvalue[0].tvalue;
		if (table.getSymbolTableEntry(base->name) == NULL) {
			throwSE("Var " + base->name + " undeclared");
		}
		val = base;
		for (int i = 1; i < var.ntvalue.size();) {
			if (var.ntvalue[i].name == "$.") {
				ASTStrucMemberAccess *m = new ASTStrucMemberAccess(table);
				m->strucVar = val;
				m->member = var.ntvalue[i + 1].tvalue;
				val = m;
				i+=2;
			} else if (var.ntvalue[i].name == "$[") {
				ASTArrayEl *a = new ASTArrayEl(table);
				a->arrVar = val;
				a->index = parseValueNode(var.ntvalue[i+1], table);
				val = a;
				i+=3;
			} else  {
				throwSE("Failed to parse valuenode");
			}
		}
		return val;		
	}

	ASTValueNode* parseValueNode(Symbol& value, SymbolTable& table) {
		if (value.name == "value") {
			if (value.ntvalue.size() > 1) {
				ASTCastValue *cast = new ASTCastValue(table);
				cast->node = parseValueNode(value.ntvalue[0], table);
				cast->castType = parseVarType(value.ntvalue[2]);
				return cast;
			} else {
				return parseValueNode(value.ntvalue[0], table);
			}
		} else if (value.name == "castableValue" || value.name== "logicOperand" || value.name == "cmpOperand" || value.name == "addOperand") {
			if (value.ntvalue.size() > 1) {
				ASTValueNode* left = parseValueNode(value.ntvalue[0], table);
				for (int i = 1; i < value.ntvalue.size(); i+=2) {
					ASTBinaryOperator *bin = new ASTBinaryOperator(table);
					string opStr = value.ntvalue[i].tvalue;
#define AS_OP_SWITCH(_opStr, _op) if (opStr == #_opStr) {bin->op = ASTBinaryOperator::_op;} else
					AS_OP_SWITCH(||, OR)
					AS_OP_SWITCH(&&, AND)
					AS_OP_SWITCH(+, PLUS)
					AS_OP_SWITCH(-, MINUS)
					AS_OP_SWITCH(*, MUL)
					AS_OP_SWITCH(/, DIV)
					AS_OP_SWITCH(>, GT)
					AS_OP_SWITCH(>=, GTE)
					AS_OP_SWITCH(<, LT)
					AS_OP_SWITCH(<=, LTE)
					AS_OP_SWITCH(==, EQ)
					AS_OP_SWITCH(!=, NEQ)
					throwSE("Unknown binary operator " + opStr);
#undef AS_OP_SWITCH
					bin->left = left;
					bin->right = parseValueNode(value.ntvalue[i+1], table);
					left = bin;
				}
				return left;
			} else {
				return parseValueNode(value.ntvalue[0], table);
			}
		} else if (value.name == "mulOperand") {
			if (value.ntvalue[0].name == "$!") {
				ASTUnaryOperator *un = new ASTUnaryOperator(table);
				un->op = ASTUnaryOperator::NOT;
				un->node = parseValueNode(value.ntvalue[1], table);
				return un;
			} else {
				return parseValueNode(value.ntvalue[0], table);
			}
		} else if (value.name == "notOperand") {
			if (value.ntvalue[0].name == "$(") {
				return parseValueNode(value.ntvalue[1], table);
			} else if (value.ntvalue[0].name == "constant") {
				Symbol& cs = value.ntvalue[0];
				if (cs.ntvalue[0].name == "$char") {
					ASTCharConstValue *charVal = new ASTCharConstValue(table);
					charVal -> value = cs.ntvalue[0].tvalue.at(0);
				} else {
					Symbol& ns = cs.ntvalue[0].ntvalue[0];
					if (ns.name == "$float") {
						ASTFloatConstValue *fv = new ASTFloatConstValue(table);
						fv->value = stof(ns.tvalue);
						return fv;
					} else {
						ASTIntConstValue *iv = new ASTIntConstValue(table);
						iv->value = stoi(ns.tvalue);
						return iv;
					}
				}
			} else if (value.ntvalue[0].name == "notOperandWithVarStart") {
				ASTVariable *var = parseVariable(value.ntvalue[0].ntvalue[0], table);	
				Symbol& end = value.ntvalue[0].ntvalue[1];
				if (end.ntvalue.size() == 0) return var;
				if (end.ntvalue[0].name=="$?") {
					ASTUnaryOperator *un = new ASTUnaryOperator(table);
					un->op = ASTUnaryOperator::CHECK_NIL;
					un->node = var;
					return un;
				}
				if (end.ntvalue[0].name=="$[]") {
					ASTUnaryOperator *un = new ASTUnaryOperator(table);
					un->op = ASTUnaryOperator::ARR_SIZE;
					un->node = var;
					return un;
				}
				if (end.ntvalue[0].name=="funcCall") {
					Symbol& fcall = end.ntvalue[0];
					ASTFuncCallNode *call = new ASTFuncCallNode(table);
					ASTSimpleVar* simpleVar = dynamic_cast<ASTSimpleVar*>(var);
					if (simpleVar == NULL) {
						throwSE("Bad func name");
					}
					if (table.getSymbolTableEntry(simpleVar -> name) == NULL) {
						throwSE("no such func");
					}	
					call->name = simpleVar->name;
					//yra argumentu
					if (fcall.ntvalue.size() > 2) {
						Symbol& args = fcall.ntvalue[1];
						for (int i = 0; i < args.ntvalue.size(); i+=2) {
							call->args.push_back(parseValueNode(args.ntvalue[i], table));
						}	
					}
					return call;
				}
			}
		}
	}

	string parseValueString(Symbol& value) {
		string str;
		for (Symbol& s : value.ntvalue) {
			if (s.name == "$str") {
				str += s.tvalue;
			} else if (s.name == "$char") {
				str += s.tvalue;
			} else if (s.name == "$int") {
				str.push_back((char)stoi(s.tvalue));
			}
		}
		return str;
	}

	ASTNode* parseStatementWithVarStart(Symbol& value, SymbolTable& table) {
		Symbol& s = value.ntvalue[1].ntvalue[0];
		if (s.name == "varDeclStatement") {
			Symbol& var = value.ntvalue[0].ntvalue[0];
			if (var.name != "$id") {
				throwSE("Bad var declaration");
			}
			if (table.getSymbolTableEntry(var.tvalue) != NULL) {
				throwSE("Var " + var.tvalue + " already declared");
			}
			ASTVarDeclNode* varDecl = new ASTVarDeclNode(table);
			varDecl->varName = var.tvalue;
			varDecl->type = parseVarType(s.ntvalue[1]);
			SymbolTableEntry e(var.tvalue, new VarType(varDecl->type));
			table.addSymbolEntry(e);
			return varDecl;
		} else {
			ASTVariable* var = parseVariable(value.ntvalue[0], table);
			if (s.name == "funcCallStatement") {
				ASTProcCallNode *call = new ASTProcCallNode(table);
				Symbol& v = value.ntvalue[0].ntvalue[0];
				if (table.getSymbolTableEntry(v.tvalue) == NULL) {
					throwSE("no such func");
				}	
				call->name = v.name;
				Symbol& fcall = s.ntvalue[0];
				//yra argumentu
				if (fcall.ntvalue.size() > 2) {
					Symbol& args = fcall.ntvalue[1];
					for (int i = 0; i < args.ntvalue.size(); i+=2) {
						call->args.push_back(parseValueNode(args.ntvalue[i], table));
					}	
				}
				return call;
			} else if (s.name == "varAssignStatement") {
				ASTValueAssign *valAssign = new ASTValueAssign(table);
				valAssign->left = var;
				Symbol& right = s.ntvalue[1];
				if (right.name == "value") {
					valAssign->rightNode = parseValueNode(right, table);
					valAssign->type = ASTValueAssign::VAL;
				} else {
					valAssign->rightString = parseValueString(right);
					valAssign->type = ASTValueAssign::STRING;
				}
				return valAssign;
			} else if (s.name == "refAssignStatement") {
				ASTRefAssign *refAssign = new ASTRefAssign(table);
				refAssign->left = var;
				if (s.ntvalue[1].name == "$nil") {
					refAssign->right = NULL;
				} else {
					refAssign->right = parseVariable(s.ntvalue[1], table);
					if (s.ntvalue[2].name == "refAssignOffset") {
						Symbol& offset = s.ntvalue[2];
						refAssign->shiftRight = offset.ntvalue[0].tvalue == ">>";
						refAssign->shift = parseValueNode(offset.ntvalue[1], table);
					}
				}
				return refAssign;
			}
		}
		return NULL;	
	}

	ASTWhileNode* parseWhileStatement(Symbol& s, SymbolTable& table) {
		ASTWhileNode* whileNode = new ASTWhileNode(table);
		whileNode->condition = parseValueNode(s.ntvalue[2], table);
		whileNode->loopBeginLabel = "while-begin-" + irn.getLabel();
		whileNode->loopEndLabel = "while-end-" + irn.getLabel();
		SymbolTable* whileBlock = table.createNewScope("while-" + irn.getScope());
		for (Symbol& fImplStatement : s.ntvalue[5].ntvalue) {
			whileNode -> block.push_back(parseFuncImplStatement(fImplStatement, *whileBlock));
		}
		return whileNode;
	}

	ASTNode* parseIfStatement(Symbol& s, SymbolTable& table) {
		ASTIfNode *ifNode = new ASTIfNode(table);
		ifNode->table = &table;
		ifNode->condition = parseValueNode(s.ntvalue[2], table);
		SymbolTable* ifBlock = table.createNewScope("if-" + irn.getScope());
		for (Symbol& fImplStatement : s.ntvalue[5].ntvalue) {
			ifNode -> block.push_back(parseFuncImplStatement(fImplStatement, *ifBlock));
		}

		if (s.ntvalue.size() > 7) {
			Symbol& elseS = s.ntvalue[7];
			for (Symbol& elif : elseS.ntvalue[0].ntvalue) {
				ASTElseIfNode *elifNode = new ASTElseIfNode(table);
				elifNode->table = &table;
				elifNode->condition = parseValueNode(s.ntvalue[2], table);
				SymbolTable* elifBlock = table.createNewScope("elif-" + irn.getScope());
				for (Symbol& fImplStatement : elif.ntvalue[5].ntvalue) {
					elifNode -> block.push_back(parseFuncImplStatement(fImplStatement, *elifBlock));
				}
				ifNode->elseIfs.push_back(elifNode);
			}
			SymbolTable* elseBlock = table.createNewScope("else-" + irn.getScope());
			for (Symbol& fImplStatement : elseS.ntvalue[3].ntvalue) {
				ifNode -> elseBlock.push_back(parseFuncImplStatement(fImplStatement, *elseBlock));
			}
		}
		return ifNode;
	}

	ASTNode* parseControlStatement(Symbol& controlStatement, SymbolTable& table) {
		Symbol& s = controlStatement.ntvalue[0];
		if (s.name == "ifStatement") {
			return parseIfStatement(s, table);
		} else if (s.name == "whileStatement") {
			return parseWhileStatement(s, table);
		}
	}
	ASTNode* parseFuncImplStatement(Symbol& funcImplStatement, SymbolTable& table) {
		string name = funcImplStatement.ntvalue[0].name;
		Symbol& s = funcImplStatement.ntvalue[0];
		if (name == "$break") {
			ASTBreakNode *breakNode = new ASTBreakNode(table);
			return breakNode;
		} else if (name == "writeStatement") {
			ASTWriteNode *writeNode = new ASTWriteNode(table);
			writeNode -> table = &table;
			Symbol& write = s.ntvalue[1];
			if (write.name == "charString") {
				//+=2 - praleidziam ,
				for (int i = 0; i < write.ntvalue.size(); i += 2) {
					writeNode -> type = ASTWriteNode::STR;
					writeNode -> str = parseValueString(write);
				}
			} else {
				writeNode -> type = ASTWriteNode::VALUE;
				writeNode -> value = parseValueNode(write, table);
			}
			return writeNode;
		} else if (name == "readStatement") {
			ASTReadNode *readNode = new ASTReadNode(table);
			readNode -> table = &table;
			Symbol& read = s.ntvalue[1];
			readNode -> var = parseVariable(read, table);
			return readNode;
		} else if (name == "returnStatement") {
			ASTOutNode *outNode = new ASTOutNode(table);
			outNode -> table = &table;
			Symbol& out = s.ntvalue[1];
			cout << out.name << endl;
			outNode -> value = parseValueNode(out, table);
			return outNode;
		} else if (name == "controlStatement") {
			return parseControlStatement(s, table);
		} else if (name == "statementWithVariableStart") {
			return parseStatementWithVarStart(s, table);
		}
	}
	
	StrucDeclaration* createStrucDecl(Symbol& declStatement, SymbolTable& table) {
		StrucDeclaration *decl = new StrucDeclaration();
		decl->isPrivate = declStatement.ntvalue[0].ntvalue.size() > 0;
		Symbol& strucDecl = declStatement.ntvalue[1];
		Symbol& strucDeclHeader = strucDecl.ntvalue[0];
		decl->name = strucDeclHeader.ntvalue[1].tvalue;
		if (strucDecl.ntvalue.size() > 2) {
			decl->isExternallyDefined = false;
			Symbol& vars = strucDecl.ntvalue[2];
			for (int i = 0; i < vars.ntvalue.size(); i+= 2) {
				StrucDeclaration::StrucMember sm;
				sm.name = vars.ntvalue[i].tvalue;
				sm.type = parseVarType(vars.ntvalue[i+1].ntvalue[1]);
				decl->members.push_back(sm);
			}		
		} else {
			decl->isExternallyDefined = true;
		}
		return decl;
	}

	ASTFuncNode* createFuncNode(
			Symbol& declStatement, 
			SymbolTable& symbolTable) 
	{
		ASTFuncNode *func = new ASTFuncNode(symbolTable);
		func -> isPrivate = declStatement.ntvalue[0].ntvalue.size() > 0;
		Symbol& funcDecl = declStatement.ntvalue[1];
		Symbol& header = funcDecl.ntvalue[0];
		func -> name = header.ntvalue[0].tvalue;
		Symbol& argsDecl = header.ntvalue[2];

		SymbolTable* funcScope = symbolTable.createNewScope(func -> name);
		func -> table = funcScope;

		if (argsDecl.ntvalue.size() > 0) {
			Symbol& args = argsDecl.ntvalue[0];
			for (int i = 0; i < args.ntvalue.size(); i+=2) {
				Symbol& arg = args.ntvalue[i];
				string varName = arg.ntvalue[0].tvalue;
				VarType varType = parseVarType(arg.ntvalue[2]);
				funcScope -> addSymbolEntry(SymbolTableEntry(varName, new VarType(varType)));
				func -> args.push_back(varType);
			}
		}

		Symbol& returnType = header.ntvalue[5].ntvalue[0];
		if (returnType.type == Symbol::TERM && returnType.name == "$proc") {
			func -> isProc = true;
		} else {
			func -> isProc = false;
			func -> returnType = parseVarType(returnType);
		}
			FuncDeclaration *fd = new FuncDeclaration();
			fd->name = func->name;
			fd->isProc = func->isProc;
			fd->isPrivate = func-> isPrivate;
			fd->isExternallyDefined = func->isExternallyDefined;
			fd->arguments = func->args;
			fd->returnType = func->returnType;
			symbolTable.addSymbolEntry(SymbolTableEntry(fd->name, fd));
		for (Symbol& funcImplStatement : funcDecl.ntvalue[2].ntvalue) {
			func -> block.push_back(parseFuncImplStatement(funcImplStatement, *funcScope));
		}
		return func;
	}


	StrucDeclaration* getStruc(string name, SymbolTable& table) {
		SymbolTableEntry* e = table.getSymbolTableEntry(name);
		if (e == nullptr) return nullptr;
		if (e->type != SymbolTableEntry::STRUC) return nullptr;
		return e->strucDeclaration;
	}

	void calculateStrucSize(StrucDeclaration& sd, SymbolTable& root);

	int calculateVarTypeSize(VarType& vt, SymbolTable& root) {
		if (vt.mods[vt.mods.size()-1] == VarType::REF) {
			return 4;
		}
		int size = 0;
		//ar mod == ?[]
		if (vt.mods.size() == 2) {
			size = 4;
		} else {
			if (vt.base == VarType::INT || vt.base == VarType::FLOAT) {
				size = 4;
			} else if (vt.base == VarType::CHAR) {
				size = 1;
			} else if (vt.base == VarType::STRUC) {
				StrucDeclaration* sd = getStruc(vt.strucName, root);
				if (sd->size == 0) {
					calculateStrucSize(*sd, root);
				}
				size = sd->size;
			} else {
				throwSE("VarType base not set");
			}
		}
		if (vt.mods[vt.mods.size()-1] == VarType::ARR) {
			size = size * vt.arrSize + 4;
		}
		return size;
	}

	void calculateStrucSize(StrucDeclaration& sd, SymbolTable& root) {
		int size = 0;
		for (auto& m : sd.members) {
			m.offset = size;
			size += calculateVarTypeSize(m.type, root);
		}
		sd.size = size;
	}

	void calculateStrucDeclarationsSize(SymbolTable& rootTable) {
		for (SymbolTableEntry* e : rootTable.getSymbolTableEntries()) {
			if (e->type != SymbolTableEntry::STRUC) continue;
			StrucDeclaration* sd = e->strucDeclaration;
			calculateStrucSize(*sd, rootTable);
		}	
	}
}

AST ASCompiler::parseASTFromSymbol(Symbol& z, IRNameSupplier& irn) {
	AST ast;
	Symbol& program = z.ntvalue[1];
	SymbolTableContainer& stc = ast.getSTC();
	SymbolTable& symbolTable = *stc.getGlobalTable();
	for (Symbol& declStatement : program.ntvalue) {
		//ntvalue[0] == <privateMod>
		Symbol& statement = declStatement.ntvalue[1];
		if (statement.name == "funcDeclStatement") {
			ASTFuncNode *funcNode = createFuncNode(declStatement, symbolTable);
			if (funcNode != NULL) {
				ast.addFuncNode(funcNode);
			}
		} else if (statement.name == "strucDeclStatement") {
			auto *strucDecl = createStrucDecl(declStatement, symbolTable);
			symbolTable.addSymbolEntry(SymbolTableEntry(strucDecl->name, strucDecl));
		}
	}
	calculateStrucDeclarationsSize(symbolTable);
	return ast;
}
