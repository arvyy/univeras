#include "Declarations.hpp"

using namespace std;
using namespace ASCompiler;


VarType::VarType() {
	base = VarType::UNDEFINED;
	arrSize = -1;
}

bool VarType::operator==(const VarType& other) const {
	if (base != other.base) return false;
	if (base == VarType::STRUC) {
		if (strucName != other.strucName) {
			return false;
		}
	}
	if (mods.size() != other.mods.size()) return false;
	for (int i = 0; i < mods.size(); i++) {
		if (mods[i] != other.mods[i]) return false;
	}
	return true;
}

bool VarType::operator!=(const VarType& other) const {
	return !(*this == other);	
}

StrucDeclaration::StrucMember* StrucDeclaration::get(string name) {
	for (auto& m : members) {
		if (m.name == name) return &m;
	}
	return NULL;
}
