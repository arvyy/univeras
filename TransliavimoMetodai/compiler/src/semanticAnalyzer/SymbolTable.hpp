#ifndef SYMBOL_TABLE_HPP
#define SYMBOL_TABLE_HPP

#include "Declarations.hpp"
#include <map>
#include <vector>

namespace ASCompiler {
	
	struct SymbolTableEntry {
		SymbolTableEntry();
		SymbolTableEntry(std::string name, VarType* type);
		SymbolTableEntry(std::string name, FuncDeclaration* fdecl);
		SymbolTableEntry(std::string name, StrucDeclaration* sdecl);
		enum Type {
			FUNC,
			STRUC,
			VAR
		} type;
		std::string name;
		VarType* varType;
		FuncDeclaration* funcDeclaration;
		StrucDeclaration* strucDeclaration;
		int offset;
	};

	class SymbolTableContainer;

	class SymbolTable {
		public:
			SymbolTable();
			SymbolTable(SymbolTableContainer* stc, std::string name, SymbolTable* parent);
			SymbolTableEntry* getSymbolTableEntry(std::string name);
			SymbolTable* createNewScope(std::string name);
			void addSymbolEntry(SymbolTableEntry e);
			std::vector<SymbolTableEntry*> getSymbolTableEntries();
			std::string getName();
		private:
			std::string name;
			std::map<std::string, SymbolTableEntry> entries;
			SymbolTable *parentScope;
			SymbolTableContainer *stc;
	};

	class SymbolTableContainer {
		public:
			SymbolTableContainer();
			SymbolTable* getGlobalTable();
			SymbolTable* getScopeTable(std::string scopeName);
			void addScopeTable(SymbolTable* table);
		private:
			SymbolTable globalTable;
			std::map<std::string, SymbolTable*> scopeTables;
	};
}

#endif
