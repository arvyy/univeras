#include "AST.hpp"
#include <SemanticAnalyzer.hpp>

using namespace std;
using namespace ASCompiler;

namespace {
	void throwSE(string message) {
		SemanticException se;
		se.message = message;
		throw se;
	}
}

ASTNode::ASTNode(SymbolTable& t) : table(&t){}

void ASTNode::writeIR(IR& ir, IRNameSupplier& irn) {}

void ASTFuncNode::writeIR(IR& ir, IRNameSupplier& irn) {
	IREntry e;
	e.type = IREntry::LABEL;
	e.operand1 = name;
	ir.entries.push_back(e);
	for (auto node : block) {
		node->writeIR(ir, irn);
	}	
}

void ASTValueAssign::writeIR(IR& ir, IRNameSupplier& irn) {
	left->eval();
	rightNode->eval();
	left->writeIR(ir, irn);
	rightNode->writeIR(ir, irn);
	IREntry e;
	e.type = IREntry::ASSIGN;
	e.result = left->tempVarName;
	e.operand1 = rightNode->tempVarName;
	ir.entries.push_back(e);
}

void ASTFloatConstValue::writeIR( IR& ir, IRNameSupplier& irNameSupplier) {
	tempVarName = irNameSupplier.getTemp();
	IREntry e;
	e.type = IREntry::ASSIGN;
	e.operand1 = to_string(value);
	e.result = tempVarName;
	ir.entries.push_back(e);
}

void ASTFloatConstValue::eval() {
	valueType.base = VarType::FLOAT;
}


void ASTIntConstValue::writeIR( IR& ir, IRNameSupplier& irNameSupplier) {
	eval();
	tempVarName = irNameSupplier.getTemp();
	IREntry e;
	e.type = IREntry::ASSIGN;
	e.operand1 = to_string(value);
	e.result = tempVarName;
	ir.entries.push_back(e);
}

void ASTIntConstValue::eval() {
	valueType.base = VarType::INT;
}

void ASTBinaryOperator::writeIR( IR& ir, IRNameSupplier& irNameSupplier) {
	eval();
	left->writeIR(ir, irNameSupplier);
	right->writeIR(ir, irNameSupplier);
	tempVarName = irNameSupplier.getTemp();
	IREntry e;
	e.result = tempVarName;
	e.operand1 = left->tempVarName;
	e.operand2 = right->tempVarName;
	e.type = IREntry::BINARY_OP;
	string tmp;
	switch (op) {
		case PLUS:
			e.op = IREntry::PLUS; break;
		case MINUS:
			e.op = IREntry::MINUS; break;
		case MUL:
			e.op = IREntry::MUL; break;
		case DIV:
			e.op = IREntry::DIV; break;
		case AND:
			e.op = IREntry::AND; break;
		case OR:
			e.op = IREntry::OR; break;
		case EQ:
			e.op = IREntry::CMP_EQ; break;
		case LT:
			e.op = IREntry::CMP_LESS; break;
		case GT:
			e.op = IREntry::CMP_LESS;
			tmp = e.operand1;
			e.operand1 = e.operand2;
			e.operand2 = tmp;
			break;
		//TODO other cases
		default: break;
	}
	ir.entries.push_back(e);
}

void ASTBinaryOperator::eval() {
	left->eval();
	right->eval();
	if (left->valueType.mods.size() != 0) {
		throwSE("Expected plain type, got ref or array");
	}
	if (right->valueType != left->valueType) {
		throwSE("Unmatching types for binary op");
	}
	switch (op) {
		case PLUS:
		case MINUS:
		case MUL:
		case DIV:
			valueType.base = left->valueType.base;
			break;
		default:
			valueType.base = VarType::INT;
	}
}

void ASTUnaryOperator::writeIR(IR& ir, IRNameSupplier& irNameSupplier) {
	eval();
	node->writeIR(ir, irNameSupplier);
	tempVarName = irNameSupplier.getTemp();
	IREntry e;
	e.operand1 = node->tempVarName;
	e.result = tempVarName;
	switch (op) {
		case NOT:
			e.type = IREntry::UNARY_OP;
			e.op = IREntry::NOT;
			break;
		case CHECK_NIL:
			e.type = IREntry::BINARY_OP;
			e.op = IREntry::CMP_EQ;
			e.operand2 = "0";
			break;
		case ARR_SIZE:
			e.type = IREntry::DEREFERENCE_R;
			break;
	}
	ir.entries.push_back(e);
}

void ASTUnaryOperator::eval() {
	node->eval();
	switch (op) {
		case NOT:
			if (node->valueType.mods.size() > 0) {
				throwSE("Can only 'not' on simple types");
			}
			break;
		case CHECK_NIL:
			if (!node->valueType.isRef()) {
				throwSE("Cant check nil for non ref type");
			}
			break;
		case ARR_SIZE:
			if (!node->valueType.isArr()) {
				throwSE("can only check size for ref");
			}
			break;
	}
	valueType.base = VarType::INT;
}

void ASTSimpleVar::writeIR(IR& ir, IRNameSupplier& irNameSupplier) {
	eval();
	tempVarName = irNameSupplier.getTemp();
	if (valueType.mods.size() == 0 && valueType.base != VarType::STRUC) {
		IREntry e;
		e.result = tempVarName;
		e.type = IREntry::DEREFERENCE_R;
		e.operand1 = name;
		ir.entries.push_back(e);
	} else {
		IREntry e;
		e.result = tempVarName;
		e.type = IREntry::ASSIGN;
		e.operand1 = name;
		ir.entries.push_back(e);
	}
}

void ASTSimpleVar::eval() {
	SymbolTableEntry* e = table->getSymbolTableEntry(name);
	if (e->type != SymbolTableEntry::VAR) {
		throwSE("" + name + " was not resolved to variable type");
	}
	valueType = *(e->varType);
}

AST::AST(){}

AST::~AST(){}

void AST::addFuncNode(ASTFuncNode* node) {
	funcNodes.push_back(node);
}

SymbolTableContainer& AST::getSTC() {
	return stc;
}

void AST::writeIR(IR& ir, IRNameSupplier& irn) {
	for (auto& fn : funcNodes) {
		fn->writeIR(ir, irn);
	}	
}
