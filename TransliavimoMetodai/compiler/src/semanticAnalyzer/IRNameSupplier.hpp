#ifndef AS_IR_NAME_SUPPLIER_HPP
#define AS_IR_NAME_SUPPLIER_HPP

#include <string>
#include <vector>

namespace ASCompiler {

	class IRNameSupplier {
		public:
			std::string getLabel();
			std::string getTemp();
			std::string getScope();
		private:
			int label;
			int temp;
			int scope;
	};

}

#endif
