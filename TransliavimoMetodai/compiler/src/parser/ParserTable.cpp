#define AS_PARSER_DEBUG_OUTPUT ./test/parser/dbg
#define STRINGIFY2(X) #X
#define STRINGIFY(X) STRINGIFY2(X)

#include "ParserTable.hpp"
#include <map>
#include <set>
#include <iostream>
#include <Parser.hpp>

#ifdef AS_PARSER_DEBUG_OUTPUT
#include <fstream>
#endif

using namespace ASCompiler;
using namespace std;

namespace {
	bool containsEmpty(first_set& set, vector<string> key) {
		if (key.size() == 1 && key[0] == "") return true;
		for (string s : set[key]) {
			if (s.length() == 0) return true;
		}
		return false;
	}

#ifdef AS_PARSER_DEBUG_OUTPUT

	void encode(std::string& data) {
		std::string buffer;
		buffer.reserve(data.size());
		for(size_t pos = 0; pos != data.size(); ++pos) {
			switch(data[pos]) {
				case '&':  buffer.append("&amp;");       break;
				case '\"': buffer.append("&quot;");      break;
				case '\'': buffer.append("&apos;");      break;
				case '<':  buffer.append("&lt;");        break;
				case '>':  buffer.append("&gt;");        break;
				default:   buffer.append(&data[pos], 1); break;
			}
		}
		data.swap(buffer);
	}

	void writeRulesToFile(ofstream& out, vector<Rule>& rules) {
		out << "<h1>Rules</h1>";
		for (Rule r : rules) {
			string s = r.toString();
			encode(s);
			out << s << "<br>";
		}
	}

	void writeFirstSetToFile(ofstream& out, first_set& first) {
		out << "<h1>First set</h1>";
		out << "<table border=\"true\"><tr><th>Eilute</th><th>Pirmi terminalai</th></tr>";
		for (auto& entry : first) {
			out << "<tr><td>";
			for (auto s : entry.first) {
				if (s == "") s = "(e)";
				out << s << " ";
			}
			out << "</td><td>";
			for (auto s : entry.second) {
				if (s == "") s = "(e)";
				out << s << " ";
			}
			out << "</td></tr>";
		}
		out << "</table>" << endl;
	}

	void writeFollowSetToFile(ofstream& out, follow_set& follow) {
		out << "<h1>Follow set</h1>";
		out << "<table border=\"true\"><tr><th>Eilute</th><th>Pirmi terminalai</th></tr>";
		for (auto& entry : follow) {
			out << "<tr><td>";
			out << entry.first;
			out << "</td><td>";
			for (auto& s : entry.second) {
				out << s << " ";
			}
			out << "</td></tr>";
		}
		out << "</table>" << endl;
	}



	void writeRulesTableToFile(ofstream& out, map<string, map<string, Rule>>& table, set<string>& nonTermSet, set<string>& termSet) {
		out << "<h1>Rules table</h1>";
		out << "<table border=\"true\"><tr><th></th>";
		for (auto term : termSet) {
			out << "<th>" << term << "</th>";	
		}
		out << "</tr>";

		for (auto nonTerm : nonTermSet) {
			out << "<tr><th>" << nonTerm << "</th>";
			for (auto term: termSet) {
				if (table.find(nonTerm) == table.end() || table[nonTerm].find(term) == table[nonTerm].end()) {
					out << "<td></td>";
					continue;
				}
				Rule r = table[nonTerm][term];
				string val = (r.left.name == "" ? "" : r.toString());
				encode(val);
				out << "<td>" << val << "</td>";
			}
			out << "</tr>";
		}
		out << "</table>";
	}
#endif

	void calcFirstOfLine(first_set& firstSet, vector<string> line) {
		set<string> firstOfLine = firstSet[line];
		if (line.size() < 1) return;
		if (!containsEmpty(firstSet, {line[0]})) {
			for (auto& s : firstSet[{line[0]}]) {
				firstSet[line].insert(s);
			}
		} else {
			for (string s : firstSet[{line[0]}]) {
				if (s.length() > 0) {
					firstSet[line].insert(s);
				}
			}
			vector<string> lineWithoutFirst = line;
			lineWithoutFirst.erase(lineWithoutFirst.begin());
			calcFirstOfLine(firstSet, lineWithoutFirst);
			for (string s : firstSet[lineWithoutFirst]) {
				firstSet[line].insert(s);
			}

			// pridedam tuscia, jei jis yra visuose elementuose
			bool allContainEmpty = true;
			for (string s : lineWithoutFirst) {
				if (!containsEmpty(firstSet, {s})) {
					allContainEmpty = false;
					break;
				}
			}
			if (allContainEmpty) {
				firstSet[line].insert("");
			}
		}
	}

	first_set buildFirstSet(set<string>& termSet, set<string>& nonTermSet, map<string, vector<Rule>>& rulesMap) {
		//first sets
		//first(x) = {a, b, ...}, kur x yra simboliu seka - Y1Y2...
		first_set first;
		//prideti terminalu reiksmes i ju paciu aibe
		for (string term : termSet) {
			first[{term}] = {term};
		}
		//prideti tuscia simboli i reikiamas neterminalu aibes
		for (string nonTerm : nonTermSet) {
			for (auto& rule : rulesMap[nonTerm]) {
				if (rule.right.size() == 1 && rule.right[0].type == RuleEntrySymbol::EMPTY) {
					//first[{rule.left.name}] = {rule.right[0].name};
					first[{rule.left.name}] = {""};
				}
			}
		}	
		first_set firstBeforeIteration;
		do {
			firstBeforeIteration = first;
			for (string nonTerm : nonTermSet) {
				//kiekvienam neterminalui einam per jo taisykles
				for (auto& rule : rulesMap[nonTerm]) {
					vector<string> line;
					for (auto& res : rule.right) {
						if (res.type == RuleEntrySymbol::EMPTY) {
							//TODO ar tikrai sito reikia
							//line.push_back("");
						} else {
							line.push_back(res.name);
						}
					}
					calcFirstOfLine(first, line);
					for (string s : first[line]) {
						first[{nonTerm}].insert(s);
					}
				}
			}
		} while (firstBeforeIteration != first);

		return first;
	}

	follow_set buildFollowSet(first_set& first, set<string>& nonTermSet, map<string, vector<Rule>>& rulesMap) {
		follow_set follow;
		follow["Z"].insert("#");
		follow_set followBeforeIteration;
		do {
			followBeforeIteration = follow;
			for (string nonTerm : nonTermSet) {
				for (Rule rule : rulesMap[nonTerm]) {
					for (int i = 0; i < rule.right.size(); i++) {
						if (i == rule.right.size() - 1) {
							for (string s : follow[nonTerm]) {
								follow[rule.right[i].name].insert(s);
							}
						} else {
							for (string s : first[{rule.right[i + 1].name}]) {
								if (s.length() > 0) {
									follow[rule.right[i].name].insert(s);
								}
							}
							if (containsEmpty(first, {rule.right[i + 1].name})) {
								for (string s : follow[nonTerm]) {
									follow[rule.right[i].name].insert(s);
								}
							}
						}
					}
				}
			}
		} while (followBeforeIteration != follow);

		return follow;
	}

	map<string, map<string, Rule>> buildRuleTable(first_set& first, follow_set& follow, set<string>& termSet, set<string>& nonTermSet, map<string, vector<Rule>>& rulesMap) {
		map<string, map<string, Rule>> table;
		for (string nonTerm : nonTermSet) {
			for (string term : termSet) {
				bool found = false;
				for (Rule rule : rulesMap[nonTerm]) {
					vector<string> rightNames;
					for (RuleEntrySymbol res : rule.right) {
						if (res.type != RuleEntrySymbol::EMPTY) {
							rightNames.push_back(res.name);
						} else {
							rightNames.push_back("");
						}
					}
					calcFirstOfLine(first, rightNames);
					set<string>& firstOfRightNames = first[rightNames];
					bool accept = false;
					if (firstOfRightNames.find(term) != firstOfRightNames.end()) {
						accept = true;
					} else  {
						if (firstOfRightNames.find("") != firstOfRightNames.end()) {
							set<string>& followOfNonTerm = follow[nonTerm];
							if (followOfNonTerm.find(term) != followOfNonTerm.end()){
								accept = true;
							}
						}
					}

					if (accept) {
						if (found) {
							//TODO gramatika netinkama LL1 parseriui
							ParserException e;
							e.type = ParserException::BAD_GRAMMAR;
							e.message = "Not LL(1) compliant grammar, conflict at term " + term + " and nonterm " + nonTerm;
							throw e;
						}
						found = true;
						table[nonTerm][term] = rule;
					}
				}	
			}
		}
		return table;
	}		
}

void ParserTable::build(vector<Rule> rules)
{
	//create sets of term and nonTerm
	set<string> termSet;
	termSet.insert("#");
	set<string> nonTermSet;
	map<string, vector<Rule>> rulesMap;
	for (Rule& r : rules) {
		nonTermSet.insert(r.left.name);
		for (RuleEntrySymbol& res : r.right) {
			if (res.type == RuleEntrySymbol::TERM) {
				termSet.insert(res.name);
			} else if (res.type == RuleEntrySymbol::NON_TERM) {
				nonTermSet.insert(res.name);
			}
		}
		rulesMap[r.left.name].push_back(r);
	}

	first_set first = buildFirstSet(termSet, nonTermSet, rulesMap);

	follow_set follow = buildFollowSet(first, nonTermSet, rulesMap);

	mTable = buildRuleTable(first, follow, termSet, nonTermSet, rulesMap);

#ifdef AS_PARSER_DEBUG_OUTPUT
		string fileName = STRINGIFY(AS_PARSER_DEBUG_OUTPUT);
		fileName += ".html";
		ofstream out(fileName);
		out << "<!DOCTYPE html>" << endl;
		out << "<html><head><title>rules</title></head><body>";
		writeRulesToFile(out, rules);
		writeFirstSetToFile(out, first);
		writeFollowSetToFile(out, follow);
		writeRulesTableToFile(out, mTable, nonTermSet, termSet);
		out << "</body></html>";
		out.close();
#endif
}

Rule& ParserTable::getRule(string nonTermName, string termName) {
	if (nonTermName == "" || termName == "") {
		ParserException e;
		e.type = ParserException::NO_RULE;
		e.message = "No rule for " + nonTermName + " and term " + termName + ": cannot be empty";
		throw e;
	}
	if (mTable.find(nonTermName) == mTable.end() ||
			mTable[nonTermName].find(termName) == mTable[nonTermName].end())
	{
		ParserException e;
		e.type = ParserException::NO_RULE;
		e.message = "No rule for nonterm " + nonTermName + " and term " + termName;
		throw e;
	}
	return mTable[nonTermName][termName];
}


