#ifndef AS_PARSER_RULE_HPP
#define AS_PARSER_RULE_HPP

#include <string>
#include <vector>

namespace ASCompiler {
	struct RuleEntrySymbol {
		enum Type {
			TERM,
			NON_TERM,
			EMPTY
		} type;
		std::string name;
		std::string toString() {
			if (type == RuleEntrySymbol::EMPTY) {
				return "(e)";
			}
			if (type == RuleEntrySymbol::NON_TERM) {
				return "<" + name + ">";
			}
			return name;
		};
	};
	struct Rule {
		RuleEntrySymbol left;
		std::vector<RuleEntrySymbol> right;
		std::string toString() {
			std::string s = left.toString() + " ::=";
			for (auto& res : right) {
				s+=" " + res.toString();
			}
			return s;
		};
	};
}

#endif
