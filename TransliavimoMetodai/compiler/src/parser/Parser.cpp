#define AS_PARSER_UNFOLD_TEMP_RULES

#include <Parser.hpp>
#include <stack>
#include <fstream>
#include <regex>
#include "Rule.hpp"
#include "ParserTable.hpp"

using namespace ASCompiler;
using namespace std;

namespace {
	
	string buildErrorContext(vector<Lexeme>& input) {
		string context;
		for (int i = input.size() - 8; i < input.size(); i++) {
			if (i >= 0) {
				context += input[i].value;
			}
		}
		return context;
	}	

	vector<Rule> parseRules(string rulesFileName) {
		vector<Rule> rules;
		ifstream rulesFile(rulesFileName);
		string line;
		string nonTermRegexStr = "<[^\\s<>']+>";
		string termRegexStr = "[^\\s<>#]+";
		string emptyRegexStr = "\\(e\\)";
		string symbolRegexStr = "(" + 
			termRegexStr + "|" + 
			nonTermRegexStr + "|" + 
			emptyRegexStr + 
			")";
		regex ruleRegex("^(" + nonTermRegexStr + ")\\s*::=(?:\\s*" + symbolRegexStr + "\\s*)+$");
		while(getline(rulesFile, line)) {
			if (regex_match(line, ruleRegex)) {
				istringstream iss(line);
				Rule r;
				string symbol;
				iss >> symbol;
				//ismetam nuo term simbolio '<' ir '>'
				r.left.name = symbol.substr(1, symbol.length() - 2);
				r.left.type = RuleEntrySymbol::NON_TERM;

				//praleidziam `::='
				iss >> symbol;
				while (iss >> symbol) {
					RuleEntrySymbol res;
					if (symbol == "(e)") {
						res.type = RuleEntrySymbol::EMPTY;
					} else {
						if (symbol.at(0) == '<') {
							//non term
							res.name = symbol.substr(1, symbol.length() - 2);
							res.type = RuleEntrySymbol::NON_TERM;
						} else {
							//term
							res.name = symbol;
							res.type = RuleEntrySymbol::TERM;
						}
					}
					r.right.push_back(res);
				}
				//cout << "Created rule " << r.toString() << endl;
				rules.push_back(r);
			} else {
				//cout << line + " doesnt match regex" << endl;
			}
		}
		return rules;
	}

	// taisykliu kaires puses vienodos
	bool isLeftRecursive(vector<Rule>& nonTermRules) {
		for (Rule& rule : nonTermRules) {
			if (rule.right[0].type == RuleEntrySymbol::NON_TERM &&
					rule.right[0].name == rule.left.name) 
			{
				return true;	
			}
		}
		return false;
	}

	// argument taisykliu kaires puses vienodos
	// grazina naujas taisykles, tame tarpe ir naujo neterm simbolio
	vector<Rule> fixLeftRecursion(vector<Rule> nonTermRules) {
		vector<Rule> newRules;
		/*
		 * A ::= Aa1 | Aa2 | ... | b1 | b2 | ...
		 * 
		 * patampa
		 *
		 * A ::= b1A' | b2A' | ...
		 * A' ::= a1A' | a2A' | ... | (e)
		 */
		if (nonTermRules.size() == 0) {
			return newRules;
		}
		string nt = nonTermRules[0].left.name;
		for (Rule rule : nonTermRules) {
			if (nt == rule.right[0].name && 
					rule.right[0].type == RuleEntrySymbol::NON_TERM)
			{
				Rule newRule;
				newRule.left.name = nt + "'";	
				newRule.left.type = RuleEntrySymbol::NON_TERM;
				for (int i = 1; i < rule.right.size(); i++) {
					newRule.right.push_back(rule.right[i]);
				}
				RuleEntrySymbol res;
				res.type = RuleEntrySymbol::NON_TERM;
				res.name = nt + "'";
				newRule.right.push_back(res);
				newRules.push_back(newRule);
			} else {
				RuleEntrySymbol res;
				res.type = RuleEntrySymbol::NON_TERM;
				res.name = nt + "'";
				rule.right.push_back(res);
				newRules.push_back(rule);
			}
		}
		Rule rule;
		rule.left.name = nt + "'";
		rule.left.type = RuleEntrySymbol::NON_TERM;
		RuleEntrySymbol res;
		res.type = RuleEntrySymbol::EMPTY;
		rule.right.push_back(res);
		newRules.push_back(rule);

		return newRules;
	}

	// taisykliu kaires puses vienodos
	// konfliktas, kai keliu taisykliu pradzios vienodos
	bool hasFirstConflict(vector<Rule>& rules) {
		set<string> starts;
		for (Rule r : rules) {
			if (r.right[0].type != RuleEntrySymbol::EMPTY && starts.find(r.right[0].name) != starts.end()) {
				return true;
			}
			starts.insert(r.right[0].name);
		}
		return false;
	}

	//refactor bendras pradzias i nauja tarpini neterminala
	//rules kaires puses vienodos
	//turetu buti kvieciamas cikle, vieno kvietimo gali nepakakti
	// grazina naujas taisykles, tame tarpe ir naujo neterm simbolio
	vector<Rule> fixFirstConflict(vector<Rule> rules, set<string>& nonTermSet) {
		//randam pirma simboli, kuris kartojasi keliose taisyklese
		set<string> starts;
		string commonStart = "";
		bool needsFix = false;
		for (auto& rule : rules) {
			if (rule.right[0].type == RuleEntrySymbol::EMPTY) continue;
			string start = rule.right[0].name;
			if (starts.find(start) != starts.end()) {
				commonStart = start;
				needsFix = true;
				break;
			} else {
				starts.insert(start);
			}
		}
		if (!needsFix) return rules;
		vector<Rule> newRules;
		vector<Rule> commonStartRules;
		for (Rule r : rules) {
			if (r.right[0].type != RuleEntrySymbol::EMPTY && r.right[0].name == commonStart) {
				commonStartRules.push_back(r);
			} else {
				newRules.push_back(r);
			}
		}
		if (commonStartRules.size() == 0) return rules;
		int commonStartLength = 1;

		// didinam commonStartLength, kol pradzia nebesutaps tarp kuriu nors
		while (true) {
			int nextLength = commonStartLength + 1;
			if (commonStartRules[0].right.size() < nextLength) {
				break;
			}
			string nextName = commonStartRules[0].right[nextLength - 1].name;
			bool commonEnded = false;
			for (Rule commonRule : commonStartRules) {
				if (commonRule.right.size() < nextLength) {
					commonEnded = true;
					break;
				}
				if (commonRule.right[nextLength - 1].name != nextName) {
					commonEnded = true;
					break;
				}
			}
			if (commonEnded) break;
			commonStartLength = nextLength;
		}

		string newName = rules[0].left.name + "'";
		while (nonTermSet.find(newName) != nonTermSet.end()) {
			newName += "'";
		}
		nonTermSet.insert(newName);

		Rule refactored;
		refactored.left.type = RuleEntrySymbol::NON_TERM;
		refactored.left.name = rules[0].left.name;
		for (int i = 0; i < commonStartLength; i++) {
			refactored.right.push_back(commonStartRules[0].right[i]);
		}
		RuleEntrySymbol res;
		res.type = RuleEntrySymbol::NON_TERM;
		res.name = newName;
		refactored.right.push_back(res);
		newRules.push_back(refactored);

		for (Rule rule : commonStartRules) {
			rule.right.erase(rule.right.begin(), rule.right.begin() + (commonStartLength));
			if (rule.right.size() == 0) {
				RuleEntrySymbol res;
				res.type = RuleEntrySymbol::EMPTY;
				rule.right.push_back(res);
			}
			rule.left.type = RuleEntrySymbol::NON_TERM;
			rule.left.name = newName;
			newRules.push_back(rule);
		}
		return newRules;
	}

	vector<Rule> fixRules(vector<Rule>& rules) {
		map<string, vector<Rule>> rulesMap;
		set<string> nonTermSet;
		for (Rule r : rules) {
			nonTermSet.insert(r.left.name);
			rulesMap[r.left.name].push_back(r);
		}
		//Taisom kaire rekursija
		bool leftRecursionOccured;
		do {
			leftRecursionOccured = false;
			string leftRecursiveNonTerm;
			for (string nonTerm : nonTermSet) {
				if (isLeftRecursive(rulesMap[nonTerm])) {
					leftRecursiveNonTerm = nonTerm;
					leftRecursionOccured = true;
					break;
				}
			}
			if (leftRecursionOccured) {
				vector<Rule> newRules = fixLeftRecursion(rulesMap[leftRecursiveNonTerm]);
				rulesMap[leftRecursiveNonTerm].clear();
				for (Rule r : newRules) {
					nonTermSet.insert(r.left.name);
					rulesMap[r.left.name].push_back(r);
				}
			}
		} while (leftRecursionOccured);

		//taisom FIRST/FIRST
		bool firstConflictOccured;
		do {
			firstConflictOccured = false;
			string conflictNonTerm;
			for (string nonTerm : nonTermSet) {
				if (hasFirstConflict(rulesMap[nonTerm])) {
					firstConflictOccured = true;
					conflictNonTerm = nonTerm;
					break;
				}
			}
			if (firstConflictOccured) {
				vector<Rule> newRules = fixFirstConflict(rulesMap[conflictNonTerm], nonTermSet);
				rulesMap[conflictNonTerm].clear();
				for (Rule r : newRules) {
					nonTermSet.insert(r.left.name);
					rulesMap[r.left.name].push_back(r);
				}
			}
		} while (firstConflictOccured);

		vector<Rule> fixedRules;
		for (auto& entry : rulesMap) {
			fixedRules.insert(fixedRules.end(), entry.second.begin(), entry.second.end());
		}
		return fixedRules;
	}

#ifdef AS_PARSER_UNFOLD_TEMP_RULES
	void removeTempRules(Symbol& s) {
		if (s.type == Symbol::TERM) return;
		bool hadTempRules;
		do {
			hadTempRules = false;
			for (int i = 0; i < s.ntvalue.size(); i++) {
				Symbol& child = s.ntvalue[i];
				//is TEMP
				if (child.type == Symbol::NONTERM && child.name.length() > 0 && child.name.at(child.name.length() - 1) == '\'') {
					//get flat value
					vector<Symbol> tempValue = child.ntvalue;
					s.ntvalue.erase(s.ntvalue.begin() + i);
					s.ntvalue.insert(s.ntvalue.begin() + i, tempValue.begin(), tempValue.end());
					hadTempRules = true;
					break;
				}
			}
		} while (hadTempRules);
		for (Symbol& child : s.ntvalue) {
			removeTempRules(child);
		}
	}
#endif
}

class Parser::ParserImpl {
	public:
		ParserImpl(string rulesFile);
		void setLexer(Lexer& lexer);
		Symbol getRootSymbol();
	private:
		Lexer* lexer;
		ParserTable parserTable;

		//isparsina, ir grazina taisykles, kurios buvo panaudotos, bei gautas lexemas
		void parse(vector<Lexeme>& input, vector<Rule>& usedRules);
		// priskiria term lapams reiksmes
		// Lexemos link ivesties galo yra steko dugne
		void assignTermValues(Symbol& root, stack<Lexeme>& input);
};

Parser::Parser(string rulesFile)
	: _impl(new ParserImpl(rulesFile)) {}

	Parser::~Parser() {
		delete _impl;
	}

void Parser::setLexer(Lexer& lexer) {
	_impl->setLexer(lexer);
}

Symbol Parser::getRootSymbol() {
	return _impl->getRootSymbol();
}

Parser::ParserImpl::ParserImpl(string rulesFile) {
	vector<Rule> rules = parseRules(rulesFile);
	rules = fixRules(rules);
	parserTable.build(rules);
}

void Parser::ParserImpl::setLexer(Lexer& _lexer) {
	lexer = &_lexer;
}

Symbol Parser::ParserImpl::getRootSymbol() {
	vector<Rule> rulesUsed;
	vector<Lexeme> input;
	parse(input, rulesUsed);


	//neterm simboliu lapai, 
	//steko dugne yra lapai artimiausi ivesties pabaigai
	stack<Symbol*> leaves;

	//saknis
	Symbol root;
	root.type = Symbol::NONTERM;
	root.name = "Z";
	leaves.push(&root);

	for (Rule& rule : rulesUsed) {
		Symbol* leaf = leaves.top();
		for (RuleEntrySymbol res : rule.right) {
			if (res.type != RuleEntrySymbol::EMPTY) {
				Symbol s;
				s.name = res.name;
				s.type = (res.type == RuleEntrySymbol::TERM ? Symbol::TERM : Symbol::NONTERM);
				leaf -> ntvalue.push_back(s);
			}
		}
		vector<Symbol*> newLeaves;
		for (Symbol& s : leaf -> ntvalue) {
			if (s.type == Symbol::NONTERM) {
				newLeaves.push_back(&s);
			}
		}
		leaves.pop();
		for (int i = newLeaves.size() - 1; i >= 0; i--) {
			leaves.push(newLeaves[i]);
		}
	}

	stack<Lexeme> inputStack;
	for (int i = input.size() - 1; i >= 0; i--) {
		inputStack.push(input[i]);
	}
	assignTermValues(root, inputStack);

#ifdef AS_PARSER_UNFOLD_TEMP_RULES
	removeTempRules(root);
#endif

	return root;
}

//input stacke lexemos failo gale yra steko dugne
//root turi buti neterm
void Parser::ParserImpl::assignTermValues(Symbol& root, stack<Lexeme>& input) {
	for (Symbol& s : root.ntvalue) {
		if (s.type == Symbol::NONTERM) {
			assignTermValues(s, input);
		} else {
			s.tvalue = input.top().value;
			input.pop();
		}
	}
}



void Parser::ParserImpl::parse(vector<Lexeme>& input, vector<Rule>& rulesUsed) {
	vector<RuleEntrySymbol> parseStack;

	//init stack = [<Z>#] (steko dugnas)
	RuleEntrySymbol res;
	res.type = RuleEntrySymbol::TERM;
	res.name = "#";
	parseStack.push_back(res);
	res.type = RuleEntrySymbol::NON_TERM;
	res.name = "Z";
	parseStack.push_back(res);

	Lexeme lexeme;
	string nextTerm;
	bool readNextLexeme = true;
	do {
		if (readNextLexeme) {
			try {
				lexeme = lexer -> getNextLexeme();
				input.push_back(lexeme);
				nextTerm = lexeme.type;
			} catch (LexerException e) {
				if (e.type == LexerException::NO_MORE_LEXEMES) {
					nextTerm = "#";
				} else {
					ParserException pe;
					pe.type = ParserException::LEXER_EXCEPTION;
					pe.message = e.message;
					pe.context = buildErrorContext(input);
					throw pe;
				}
			}
		}
		readNextLexeme = false;

		if (parseStack.back().type == RuleEntrySymbol::NON_TERM) {
			RuleEntrySymbol& top = parseStack.back();
			Rule r;
			try {
				r = parserTable.getRule(top.name, nextTerm);
			} catch (ParserException& e) {
				e.context = buildErrorContext(input);
				throw;
			}
			rulesUsed.push_back(r);
			parseStack.pop_back();
			for (int i = r.right.size() - 1; i >= 0; i--) {
				parseStack.push_back(r.right[i]);
			}
		} else {
			RuleEntrySymbol& top = parseStack.back();
			if (top.type == RuleEntrySymbol::EMPTY) {
				parseStack.pop_back();
			} else {
				if (top.name == nextTerm) {
					parseStack.pop_back();
					readNextLexeme = true;
				} else {
					ParserException e;
					e.type = ParserException::UNEXPECTED_SYMBOL;
					e.context = buildErrorContext(input);
					e.message = "Expected " + top.name + ", got " + nextTerm + ".";  
					throw e;
				}
			}
		}
	} while(!parseStack.empty());
}
