#ifndef AS_PARSER_TABLE_HPP
#define AS_PARSER_TABLE_HPP

#include "Rule.hpp"
#include <vector>
#include <map>
#include <string>
#include <set>

namespace ASCompiler {

	typedef std::map<std::vector<std::string>, std::set<std::string>> first_set;
	typedef std::map<std::string, std::set<std::string>> follow_set;

	class ParserTable {
		public:
			void build(std::vector<Rule> rules);
			//<Z> - sakninis neterminalinis simbolis
			//# - ivesties pabaigos terminalinis simbolis
			Rule& getRule(std::string nonTermName, std::string termName);
		private:
			//std::map<std::string, std::vector<Rule>> mRulesMap;
			std::map<std::string, std::map<std::string, Rule>> mTable;
	};	

}

#endif
