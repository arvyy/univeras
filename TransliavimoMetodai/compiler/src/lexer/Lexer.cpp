#include <Lexer.hpp>
#include <fstream>
#include <iostream>
#include <string>
#include <cctype>
#include <vector>
#include <map>
#include <regex>
#include <sstream>

using namespace ASCompiler;
using namespace std;

namespace {
	bool isdelim(char c) {
		string delims = "+-*/=<>()[]{}\"\'\\!@#$%^&,.?:;";
		for (int i = 0; i < delims.length(); i++) {
			if (delims.at(i) == c) return true;
		}
		return false;
	}
}

class Lexer::LexerImpl {
	public:
		LexerImpl(string lexemesFileName, string statesFileName);
		~LexerImpl();
		Lexeme getNextLexeme();
		void setInputStream(istream& in);
	private:
		istream* inputStream;
		struct State;
		struct StateEdgeFunction {
			enum Name {
				INIT,
				NEXTCHAR,
				CONCAT,
				CLEAR,
				RETURN,
				RETURN_LOOKUP_DEFAULT,
				RETURN_LOOKUP_FAIL,
				FAIL
			} name;
			string argument;
		};
		struct StateEdge {
			State *to;
			enum Requirement {
				//bet kokia raide
				LETTER,
				//bet koks skaitmuo
				DIGIT,
				//spec simboliai, pvz +-/*><=(){}[]
				DELIM,
				//specifinis char, saugomas charValue
				CHAR,
				//jei neatitiko reikalavimo kitoms briaunoms. Max 1 kiekvienoje virsuneje
				OTHER
			} requirement;
			vector<StateEdgeFunction> edgeFunctions;
			char charValue;
		};
		struct State {
			string name;
			vector<StateEdge> edges;
			StateEdge& getNext(char value);
		};
		State* start;
		string lexemeVal;
		char c;
		vector<State*> states;
		map<string, string> lookup;
		bool isEof;
		//grazina false jei failo pabaiga
		void nextChar(bool ignoreWhiteSpace);
		void parseLookup(string lexemesFileName);
		void parseStates(string statesFileName);
		bool getLexeme(Lexeme& l, string& errorMessage);
};

Lexer::Lexer(std::string lexemesFileName, std::string statesFileName) :
	_impl(new LexerImpl(lexemesFileName, statesFileName)) {}

Lexer::~Lexer() {
	delete _impl;
}

void Lexer::setInputStream(istream& in) {
	_impl->setInputStream(in);
}

Lexeme Lexer::getNextLexeme() {
	return _impl -> getNextLexeme();
}

Lexer::LexerImpl::LexerImpl(string lexemesFileName, string statesFileName) {
	parseLookup(lexemesFileName);
	parseStates(statesFileName);
}

Lexer::LexerImpl::~LexerImpl() {
	for (State* state_ptr : states) {
		delete state_ptr;
	}
}

Lexer::LexerImpl::StateEdge& Lexer::LexerImpl::State::getNext(char value) {
	using State = Lexer::LexerImpl::State;
	using StateEdge = Lexer::LexerImpl::StateEdge;
	using StateEdgeFunction = Lexer::LexerImpl::StateEdgeFunction;

	for (StateEdge& edge : edges) {
		switch(edge.requirement) {
			case StateEdge::CHAR : 
				if (value == edge.charValue) return edge;
				break;
			case StateEdge::LETTER : 
				if (isalpha(value)) return edge;
				break;
			case StateEdge::DIGIT : 
				if (isdigit(value)) return edge;
				break;
			case StateEdge::DELIM :
				if (isdelim(value)) return edge;
				break;
			case StateEdge::OTHER : return edge;
		}
	}
	LexerException e;
	e.type = LexerException::NO_EDGE;
	e.message = "state=" + name + ", char=" + value;
	throw (e);
}

Lexeme Lexer::LexerImpl::getNextLexeme() {
	using State = Lexer::LexerImpl::State;
	using StateEdge = Lexer::LexerImpl::StateEdge;
	using StateEdgeFunction = Lexer::LexerImpl::StateEdgeFunction;

	State *current = start;
	LexerException e;
	Lexeme l;
	while(true) {
		StateEdge& selected = current->getNext(c);
		for (auto& func : selected.edgeFunctions) {
			switch(func.name) {
				case StateEdgeFunction::INIT :
					lexemeVal = "";
					if (isspace(c)) {
						nextChar(true);
					}
					if (isEof) {
						e.type = LexerException::NO_MORE_LEXEMES;
						throw(e);
					}
					break;
				case StateEdgeFunction::NEXTCHAR :
					if (isEof) {
						e.type = LexerException::UNEXPECTED_EOF;
						throw(e);
					}
					nextChar(false);
					break;
				case StateEdgeFunction::CONCAT :
					lexemeVal += c;
					break;
				case StateEdgeFunction::CLEAR : 
					lexemeVal = "";
					break;
				case StateEdgeFunction::RETURN : 
					l.type = func.argument;
					l.value = lexemeVal;
					return l;
				case StateEdgeFunction::FAIL :
					e.type = LexerException::FAIL;
					e.message = func.argument;
					throw(e);
				case StateEdgeFunction::RETURN_LOOKUP_DEFAULT :
					l.type = func.argument;
					l.value = lexemeVal;
					if (lookup.count(lexemeVal) > 0) {
						l.type = lookup[lexemeVal];
					}
					return l;
				case StateEdgeFunction::RETURN_LOOKUP_FAIL : 
					l.value = lexemeVal;
					if (lookup.count(lexemeVal) > 0) {
						l.type = lookup[lexemeVal];
						return l;
					} else {
						e.type = LexerException::FAIL;
						e.message = func.argument;
						throw(e);
					}
			}
		}
		current = selected.to; 
	}
}

void Lexer::LexerImpl::setInputStream(istream& in) {
	inputStream = &in;
	lexemeVal = "";
	isEof = false;
	nextChar(true);	
}

void Lexer::LexerImpl::nextChar(bool ignoreWhiteSpace) {
	char next;
	while ((next = inputStream->get()) != EOF) {
		if (!ignoreWhiteSpace || !isspace(next)) {
			c = next;
			return;
		}
	}
	c = next;
	isEof = true;
}

void Lexer::LexerImpl::parseLookup(string lexemesFileName) {
	ifstream lexemesFile(lexemesFileName);
	string line;
	while(getline(lexemesFile, line)) {
		istringstream iss(line);
		string name, lookupTerminal;
		iss >> name;
		while (iss >> lookupTerminal) {
			lookup[lookupTerminal] = name;
		}
	}
}

void Lexer::LexerImpl::parseStates(string statesFileName) {
	using State = Lexer::LexerImpl::State;
	using StateEdge = Lexer::LexerImpl::StateEdge;
	using StateEdgeFunction = Lexer::LexerImpl::StateEdgeFunction;
	map<string, State*> statesMap; //name -> state
	ifstream stateFile(statesFileName);
	string line;
	smatch matches;
	regex charRegex("^'(.)'$");
	regex digitRegex("^\\d+$");
	while(getline(stateFile, line)) {
		istringstream iss(line);
		string fromname, toname, req, edgefunc;
		StateEdge edge;
		iss >> fromname;
		if (statesMap.count(fromname) == 0) {
			State *state = new State();
			state -> name = fromname;
			statesMap[fromname] = state;
			states.push_back(state);
		}
		iss >> toname;
		if (statesMap.count(toname) == 0) {
			State *state = new State();
			state -> name = toname;
			statesMap[toname] = state;
			states.push_back(state);
		}
		edge.to = statesMap[toname];
		iss >> req;
		if (req == "DIGIT") edge.requirement = StateEdge::DIGIT;
		else if (req == "LETTER") edge.requirement = StateEdge::LETTER;
		else if (req == "DELIM") edge.requirement = StateEdge::DELIM;
		else if (req == "OTHER") edge.requirement = StateEdge::OTHER;
		else if (regex_search(req, matches, charRegex)) {
			edge.requirement = StateEdge::CHAR;
			edge.charValue = matches[1].str().at(0);
		} else if (regex_search(req, matches, digitRegex)) {
			edge.requirement = StateEdge::CHAR;
			edge.charValue = (char) stoi(req);
		} else {
			LexerException e;
			e.type = LexerException::BAD_STATES_FILE;
			e.message = "no such requirement: " + req;
			throw(e);
		}

		regex returnReg("^return\\((.*)\\)$");
		regex lookupReturnDefaultReg("^returnLookupDefault\\((.*)\\)$");
		regex lookupReturnFailReg("^returnLookupFail\\(\"(.*)\"\\)$");
		regex fail("^fail\\(\"(.*)\"\\)$");
		while (iss >> edgefunc) {
			StateEdgeFunction func;
			if (edgefunc == "concat()") func.name = StateEdgeFunction::CONCAT;
			else if (edgefunc == "clear()") func.name = StateEdgeFunction::CLEAR;
			else if (edgefunc == "nextchar()") func.name = StateEdgeFunction::NEXTCHAR;
			else if (edgefunc == "init()") func.name = StateEdgeFunction::INIT;
			else if (regex_search(edgefunc, matches, returnReg)) {
				func.name = StateEdgeFunction::RETURN;
				func.argument = matches[1].str();
			} else if (regex_search(edgefunc, matches, lookupReturnDefaultReg)) {
				func.name = StateEdgeFunction::RETURN_LOOKUP_DEFAULT;
				func.argument = matches[1].str();
			} else if (regex_search(edgefunc, matches, lookupReturnFailReg)) {
				func.name = StateEdgeFunction::RETURN_LOOKUP_FAIL;
				func.argument = matches[1].str();
			} else if (regex_search(edgefunc, matches, fail)) {
				func.name = StateEdgeFunction::FAIL;
				func.argument = matches[1].str();
			} else {
				continue;
			}
			edge.edgeFunctions.push_back(func);
		}
		statesMap[fromname]->edges.push_back(edge);
	}
	for (State* state : states) {
		std::sort(state -> edges.begin(), state -> edges.end(), [](StateEdge edge_a, StateEdge edge_b) {
				int a, b;
				switch (edge_a.requirement) {
				case StateEdge::CHAR : a = 0; break;
				case StateEdge::LETTER : a = 1; break;
				case StateEdge::DIGIT : a = 2; break;
				case StateEdge::DELIM : a = 3; break;
				case StateEdge::OTHER : a = 4; break;
				}
				switch (edge_b.requirement) {
				case StateEdge::CHAR : b = 0; break;
				case StateEdge::LETTER : b = 1; break;
				case StateEdge::DIGIT : b = 2; break;
				case StateEdge::DELIM : b = 3; break;
				case StateEdge::OTHER : b = 4; break;
				}
				return a < b;   
				});
		}
	start = statesMap["start"];
}
