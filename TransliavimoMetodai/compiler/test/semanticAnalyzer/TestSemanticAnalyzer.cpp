#include <SemanticAnalyzer.hpp>

#include <iostream>
#include <string>

using namespace std;
using namespace ASCompiler;

const string LEXEMES="./resources/Lexemes.txt";
const string STATES="./resources/LexerStates.txt";

const string INPUT_SOURCE="./test/pavyzdys2.as";
const string RULES="./resources/Rules.txt";


int main() {
	
	SemanticAnalyzer sa(LEXEMES, STATES, RULES);	
	IR ir;
	try {
		sa.buildIR(INPUT_SOURCE, ir);
		for (IREntry e : ir.entries) {
			e.toString();
			cout << e.toString() << endl;
		}
	} catch (SemanticException e) {
		cout << "Exception: " << e.message << endl;
	}
}
