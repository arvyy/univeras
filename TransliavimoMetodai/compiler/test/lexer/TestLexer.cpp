#include <Lexer.hpp>
#include <iostream>
#include <fstream>

using namespace ASCompiler;
using namespace std;

const string LEXEMES="./resources/Lexemes.txt";
const string STATES="./resources/LexerStates.txt";
const string INPUT_SOURCE="./test/pavyzdys.as";

int main() {
	Lexer lexer(LEXEMES, STATES);
	//lexer.setInputStream(cin);
	ifstream in(INPUT_SOURCE.c_str());
	lexer.setInputStream(in);
	while(true) {
		try {
			Lexeme l = lexer.getNextLexeme();
			cout << "lexeme: "<< l.type << " " << l.value << endl;
		} catch (LexerException& e) {
			if (e.type != LexerException::NO_MORE_LEXEMES) {
				std::string type;
				switch(e.type) {
					case LexerException::UNEXPECTED_EOF : 
						type = "unexpected eof";
						break;
					case LexerException::NO_EDGE : 
						type = "no edge";
						break;
					case LexerException::FAIL : 
						type = "fail";
						break;
				}
				cout << "error: " << type << " " << e.message << endl;
				return 1;
			} else {
				return 0;
			}	
		}
	}
}
