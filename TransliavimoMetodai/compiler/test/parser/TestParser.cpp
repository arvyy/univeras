#include <Parser.hpp>
#include <fstream>
#include <string>
#include <iostream>

using namespace ASCompiler;
using namespace std;

const string LEXEMES="./resources/Lexemes.txt";
const string STATES="./resources/LexerStates.txt";

const string INPUT_SOURCE="./test/pavyzdys.as";
const string RULES="./resources/Rules.txt";

const string OUT="./test/parser/out.xml";

//const string INPUT_SOURCE="./test/parser/test_input.as";
//const string RULES="./test/parser/test_rules.txt";

string symbolToXmlTag(Symbol& s);

void encode(std::string& data) {
    std::string buffer;
    buffer.reserve(data.size());
    for(size_t pos = 0; pos != data.size(); ++pos) {
        switch(data[pos]) {
            case '&':  buffer.append("&amp;");       break;
            case '\"': buffer.append("&quot;");      break;
            //case '\'': buffer.append("&apos;");      break;
            case '\'': buffer.append("1");      break;
            case '<':  buffer.append("&lt;");        break;
            case '>':  buffer.append("&gt;");        break;
            default:   buffer.append(&data[pos], 1); break;
        }
    }
    data.swap(buffer);
}

int main() {
	Lexer lexer(LEXEMES, STATES);
	ifstream in(INPUT_SOURCE.c_str());
	lexer.setInputStream(in);
	
	try {
		Parser parser(RULES);
		parser.setLexer(lexer);
		Symbol root = parser.getRootSymbol();
		ofstream out(OUT.c_str());
		out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		out << symbolToXmlTag(root);
		out.close();
		//cout << symbolToXmlTag(root) << endl;
	} catch (ParserException& e) {
		cout << "Exception: " << e.message << endl;
		cout << "Context: " << e.context << endl;
	}
}

string symbolToXmlTag(Symbol& s) {
	if (s.type == Symbol::TERM) {
		string val = s.tvalue;
		encode(val);
		return val;
	} else {
		string xml;
		string val = s.name;
		encode(val);
		xml = "<" + val + ">\n";
		for (auto& child : s.ntvalue) {
			xml += symbolToXmlTag(child);
		}
		xml += "</" + val + ">\n";
		return xml;
	}
}
