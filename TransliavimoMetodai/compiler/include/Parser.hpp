#ifndef AS_PARSER_HPP
#define AS_PARSER_HPP

#include "Lexer.hpp"
#include <string>
#include <vector>

namespace ASCompiler {

	struct Symbol {
		std::string name;
		enum Type {
			TERM,
			NONTERM
		} type;
		std::vector<Symbol> ntvalue; // kai term == false 0 simboliai sudarantys si neterminalini simboli
		std::string tvalue; // kai term == true - terminalinio simbolio reiksme
	};

	struct ParserException {
		enum Type {
			UNEXPECTED_SYMBOL,
			NO_RULE,
			LEXER_EXCEPTION,
			BAD_GRAMMAR
		} type;
		std::string message;
		std::string context;
	};

	class Parser {
		public:
			Parser(std::string rulesFile);
			~Parser();
			void setLexer(Lexer& lexer);
			Symbol getRootSymbol(); // meta ParserException
		private:
			class ParserImpl;
			ParserImpl *_impl;
	};


}

#endif


