#ifndef AS_LEXER_HPP
#define AS_LEXER_HPP

#include <string>
#include <iostream>

namespace ASCompiler {

	/*
	 * LEXER
	 *
	 * Naudojami failai:
	 * 	
	 * 	a) leksemu failas. Po viena leksema eiluteje, pavidalu `israiska id`, pvz `if $if`.
	 * 		Jei lexema neturi buti naudojama lookup'e, jos vardas tarp kabuciu `"simboliu eilute" 1`
	 * 	b) automato buseno failas. Po viena briana eiluteje, pavidalu `nuoVirsunes iVirsune reikalavimas vykdomosFunkcijos`
	 * 		Virsuniu vardai - vieno zodzio stringas. Pradines virsunes vardas yra 'start'. Reikalavimas vienas is: LETTER, DIGIT, DELIM, OTHER arba vienas simbolis.
	 * 		vykdomosFunkcijos - funkciju vardai atskirti tarpais. Galimos: init(), nextchar(), concat(), clear(), return(lexemeId), returnLookupDefault(defaultLexemeId), returnLookupFail(failMessage), fail(failMessage)
	 */

	struct Lexeme {
		std::string type;
		std::string value;
	};

	struct LexerException {
		enum Type {
			BAD_STATES_FILE,
			NO_MORE_LEXEMES,
			UNEXPECTED_EOF,
			NO_EDGE,
			FAIL
		} type;	
		std::string message;
	};

	class Lexer {
		public:
			Lexer(std::string lexemesFileName, std::string statesFileName);
			~Lexer();
			void setInputStream(std::istream& in);
			Lexeme getNextLexeme(); // meta LexerException
		private:
			class LexerImpl;
			LexerImpl *_impl;
	};

}

#endif
