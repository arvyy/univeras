#ifndef AS_SEMANTIC_HPP
#define AS_SEMANTIC_HPP

#include "Parser.hpp"
#include "IntermediateRepresentation.hpp"
#include <string>
#include <iostream>

namespace ASCompiler {

	struct SemanticException {
		enum Type {
			PARSER_EXCEPTION,
			BREAK_NOT_LOOP,
			BAD_RETURN,
			UNDECLARED_VAR
		} type;
		std::string message;
	};

	class SemanticAnalyzer {
		public:
			SemanticAnalyzer(std::string lexemesFile, std::string statesTile, std::string rulesFile);	
			void buildIR(std::string fileName, IR& out);
		private:
			Lexer lexer;
			Parser parser;
	};

}

#endif
