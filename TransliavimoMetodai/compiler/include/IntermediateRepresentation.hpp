#ifndef AS_INTERMEDIATE_REPRESENTATION_HPP
#define AS_INTERMEDIATE_REPRESENTATION_HPP

#include <vector>
#include <string>

namespace ASCompiler {
	
	struct IREntry {
		enum Type {
			LABEL,
			BINARY_OP,
			UNARY_OP,
			ASSIGN,
			DEREFERENCE_R, //x = *y
			DEREFERENCE_L, //*x = y
			ADDRESS_OF, //x = &y
			JUMP,
			JGTZ,
			FUNC_CALL,
			FUNC_RET
		} type;
		enum Op {
			CMP_EQ,
			CMP_LESS,
			AND,
			OR,
			PLUS,
			MINUS,
			DIV,
			MUL,
			NOT
		} op;
		std::string operand1;
		std::string operand2;
		std::string result;

		std::string toString() {
			if (type == IREntry::LABEL) {
				return "" + operand1 + ":";
			}
			if (type == IREntry::ASSIGN) {
				return result + " = " + operand1;
			}
			if (type == IREntry::DEREFERENCE_R) {
				return result + " = deref " + operand1;
			}
			if (type == IREntry::UNARY_OP) {
				return result + " = ! " + operand1;
			}
			if (type == IREntry::BINARY_OP) {
				std::string ops;
				switch (op) {
					case CMP_EQ: ops = "=="; break;
					case CMP_LESS: ops = "<"; break;
					case AND: ops = "&&"; break;
					case OR: ops = "||"; break;
					case PLUS: ops = "+"; break;
					case MINUS: ops = "-"; break;
					case MUL: ops = "*"; break;
					case DIV: ops = "/"; break;
					default: break;
				}
				return result + " = " + operand1 + " " + ops + " " + operand2; 
			}
			return "";	
		}	
	};

	struct IR {
		std::vector<IREntry> entries;
	};

}

#endif
