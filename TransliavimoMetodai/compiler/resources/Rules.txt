<Z> ::= <imports> <program>

<imports> ::= (e)
<imports> ::= <notEmptyImports>

<notEmptyImports> ::= <notEmptyImports> <import>
<notEmptyImports> ::= <import>

<import> ::= $using $str $;
<import> ::= $using $str $as $id $;

<program> ::= <program> <declStatement>
<program> ::= <declStatement>

<declStatement> ::= <privateMod> <funcDeclStatement>
<declStatement> ::= <privateMod> <strucDeclStatement>

<funcDeclStatement> ::= <funcDeclHeader> ${ <funcImplStatements> $}
<funcDeclStatement> ::= <funcDeclHeader> $;

<funcDeclHeader> ::= $id $( <funcArgsDecl> $) $: <funcReturnType>

<privateMod> ::= (e)
<privateMod> ::= $private

<funcArgsDecl> ::= (e)
<funcArgsDecl> ::= <notEmptyFuncArgsDecl>

<notEmptyFuncArgsDecl> ::= <funcArgDecl>
<notEmptyFuncArgsDecl> ::= <notEmptyFuncArgsDecl> $, <funcArgDecl>

<funcArgDecl> ::= $id $: <variableType>

<type> ::= $basetype
<type> ::= $id

<funcReturnType> ::= $proc 
<funcReturnType> ::= <variableType>

<funcImplStatements> ::= <funcImplStatement>
<funcImplStatements> ::= <funcImplStatements> <funcImplStatement>

<funcImplStatement> ::= <statementWithVariableStart>
<funcImplStatement> ::= <controlStatement>
<funcImplStatement> ::= <returnStatement>
<funcImplStatement> ::= <readStatement>
<funcImplStatement> ::= <writeStatement>
<funcImplStatement> ::= $break $;

<statementWithVariableStart> ::= <variable> <statementWithVariableStartEnding>

<statementWithVariableStartEnding> ::= <varDeclStatement>
<statementWithVariableStartEnding> ::= <funcCallStatement>
<statementWithVariableStartEnding> ::= <varAssignStatement>
<statementWithVariableStartEnding> ::= <refAssignStatement>

<varDeclStatement> ::= $: <variableType> $;

<variableType> ::= <type> 
<variableType> ::= <variableType> $[]
<variableType> ::= <variableType> $[ $int $]
<variableType> ::= <variableType> $?


<funcCallStatement> ::= <funcCall> $;

<funcCall> ::= $( $)
<funcCall> ::= $( <funcCallArgs> $)

<funcCallArgs> ::= <funcCallArg>
<funcCallArgs> ::= <funcCallArgs> $, <funcCallArg>

<funcCallArg> ::= <value>
<funcCallArg> ::= $nil

<value> ::= <castableValue>
<value> ::= <castableValue> $as <variableType>

<castableValue> ::= <logicOperand>
<castableValue> ::= <castableValue> $logicalOp <logicOperand>

<logicOperand> ::= <cmpOperand>
<logicOperand> ::= <cmpOperand> $compare <cmpOperand>

<cmpOperand> ::= <addOperand>
<cmpOperand> ::= <cmpOperand> $add <addOperand>

<addOperand> ::= <mulOperand>
<addOperand> ::= <addOperand> $mul <mulOperand>

<mulOperand> ::= <notOperand>
<mulOperand> ::= $! <notOperand>

<notOperand> ::= <notOperandWithVarStart>
<notOperand> ::= <constant>
<notOperand> ::= $( <value> $)

<notOperandWithVarStart> ::= <variable> <notOperandWithVarStartEnd>

<notOperandWithVarStartEnd> ::= (e)
<notOperandWithVarStartEnd> ::= $?
<notOperandWithVarStartEnd> ::= $[] $. $size
<notOperandWithVarStartEnd> ::= <funcCall>

<constant> ::= <number>
<constant> ::= $char

<number> ::= $int
<number> ::= $float

<variable> ::= $id 
<variable> ::= <variable> $. $id
<variable> ::= <variable> $[ <value> $]

<varAssignStatement> ::= $= <value> $;
<varAssignStatement> ::= $= <charString> $;

<charString> ::= $str
<charString> ::= <charString> $, $char
<charString> ::= <charString> $, $str
<charString> ::= <charString> $, $int

<returnStatement> ::= $out $;
<returnStatement> ::= $out <value> $;

<controlStatement> ::= <ifStatement>
<controlStatement> ::= <whileStatement>
<controlStatement> ::= <forStatement>

<ifStatement> ::= $if $( <value> $) ${ <funcImplStatements> $} <optionalElse>

<optionalElse> ::= (e)
<optionalElse> ::= <elseifStatements> $else ${ <funcImplStatements> $}

<elseifStatement> ::= $elseif $( <value> $) ${ <funcImplStatements> $}

<elseifStatements> ::= (e)
<elseifStatements> ::= <elseifStatements> <elseifStatement>

<whileStatement> ::= $while $( <value> $) ${ <funcImplStatements> $}


<forStatement> ::= $for $( $id $: <value> $) ${ <funcImplStatements> $}

<strucDeclStatement> ::= <strucDeclHeader> $;
<strucDeclStatement> ::= <strucDeclHeader> ${ <varDeclStatements> $}

<strucDeclHeader> ::= $struc $id

<varDeclStatements> ::= $id <varDeclStatement>
<varDeclStatements> ::= <varDeclStatements> $id <varDeclStatement>

<refAssignStatement> ::= $ptrAssign <variable> <refAssignOffset> $;
<refAssignStatement> ::= $ptrAssign $nil $;

<refAssignOffset> ::= (e)
<refAssignOffset> ::= $ptrShift <value>

<readStatement> ::= $read <variable> $;

<writeStatement> ::= $write <value> $;
<writeStatement> ::= $write <charString> $;
