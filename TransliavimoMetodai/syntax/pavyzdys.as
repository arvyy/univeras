# eilutes komentaras

#> 
	bloko komentaras
#<

gautiFibSkaiciu(n : int) : int;

uzpildytiFibSkaiciais(arr : int[]?) : proc {
	if (!arr?) {
		out;
	};
	for(i : arr[].size) {
		arr[i] = gautiFibSkaiciu(i);
	};
};

uzpildytiFibSkaiciais2(arr : int[]?) : proc {
	if (!arr?) {
		out;
	};
	if (arr[].size > 0) {
		arr[0] = 1;
	};
	if (arr[].size > 1) {
		arr[1] = 1;
	} else {
		out;
	};
	a : int?;
	b : int?;
	c : int;
	a -> arr[0];
	b -> arr[1];
	for (i : arr[].size - 2) {
		c = a + b;
		a -> b;
		b -> b>>1;
		b = c;
	};
};

main() : int {
	n : int;
	simbEilute : char[60];
	simbEilute = "Fibonaci skaiciuokle",10,13;
	write simbEilute;
	while(1) {
		write "iveskite n",10,13;
		read n;
		if (n < 0) {
			break;
		};
		arr : int[n];
		uzpildytiFibSkaiciais(arr);
		uzpildytiFibSkaiciais(nil);
		for (i : arr[].size) {
			write arr[i];
			write "",10,13;
		};
	}
};

gautiFibSkaiciu(n : int) : int {
	if (n == 0 || n == 1) {
		out 1;
	} else {
		out gautiFibSkaiciu(n - 1) + gautiFibSkaiciu(n - 2);
	};
};

vec : struc;

vec : struc {
	float x;
	float y;
};

addVec(a : vec, b : vec, mult : float, target : vec?) : proc {
	target.x = (a.x + b.x) * mult;
	target.y = (a.y + b.y) * mult;
};
