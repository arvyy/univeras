
var WIDTH = 800;
var HEIGHT = 600;	

function Robot3P(scene) {

	this.a = 0;
	this.b = 0;
	this.c = 0;

	var material,
		mesh;

	var base = new THREE.Object3D();
	material = new THREE.MeshLambertMaterial({color: 0xffaa00});
	mesh = new THREE.Mesh(new THREE.CubeGeometry(32, 32, 32), material);
	mesh.position.y = 16;
	mesh.castShadow = true;
	base.add(mesh);
	this.base = base;

	var shoulder = new THREE.Object3D();
	mesh = new THREE.Mesh(new THREE.CubeGeometry(24, 64, 24), material);
	mesh.position.y = 32;
	mesh.castShadow = true;
	shoulder.add(mesh);
	mesh = new THREE.Mesh(new THREE.CylinderGeometry(16, 16, 32), material);
	mesh.position.y = 0;
	mesh.rotation.z = Math.PI / 2;
	mesh.castShadow = true;
	shoulder.add(mesh);
	shoulder.position.y = 32;
	base.add(shoulder);
	this.shoulder = shoulder;

	var arm = new THREE.Object3D();
	mesh = new THREE.Mesh(new THREE.CubeGeometry(16, 64, 16), material);
	mesh.position.y = 32;
	mesh.position.x = 16;
	mesh.castShadow = true;
	arm.add(mesh);
	mesh = new THREE.Mesh(new THREE.CylinderGeometry(16, 16, 40), material);
	mesh.position.x = 8;
	mesh.rotation.z = Math.PI / 2;
	mesh.castShadow = true;
	arm.add(mesh);
	mesh = new THREE.Mesh(new THREE.SphereGeometry(8), material);
	mesh.position.x = 16;
	mesh.position.y = 64 + 6;
	mesh.castShadow = true;
	arm.add(mesh);
	arm.position.y = 64;
	shoulder.add(arm);
	this.arm = arm;
	this.held = null;
	this.updateA();
	this.updateB();
	this.updateC();
}

Robot3P.prototype.updateA = function() {
	this.base.rotation.y = this.a;
}

Robot3P.prototype.updateB = function() {
	this.shoulder.rotation.x = this.b;
}

Robot3P.prototype.updateC = function() {
	this.arm.rotation.x = this.c;
}

Robot3P.prototype.update = function() {
	this.updateA();
	this.updateB();
	this.updateC();

	if (this.held) {
		this.held.rotation.x = -this.b - this.c;
		this.held.rotation.y = -this.a;
	}
}
Robot3P.prototype.set = function(values) {
	this.a = values.a;
	this.b = values.b;
	this.c = values.c;
	this.update();
}

Robot3P.prototype.attach = function(obj) {
	obj.position.x = 16;
	obj.position.y = 64 + 6;
	this.arm.add(obj);
	this.held = obj;
}

Robot3P.prototype.remove = function() {
	var h = this.held;
	this.held = null;
	this.arm.remove(h);
	h.rotation.set(0, 0, 0);
	return h;
}

function createOven() {
	var shape = new THREE.Shape();
	var w = 64;
	var h = 64;
	var t = 8;
	shape.moveTo(0, 0);
	shape.lineTo(w, 0);
	shape.lineTo(w, 2*h);
	shape.lineTo(w - t, 2*h);
	shape.lineTo(w - t, h);
	shape.lineTo(t, h);
	shape.lineTo(t, 2*h);
	shape.lineTo(w, 2*h);
	shape.lineTo(w, 2*h + t);
	shape.lineTo(0, 2*h + t);
	shape.lineTo(0, 0);

	var extrudeSettings = {
		steps: 2,
		depth: 64,
		bevelEnabled: true,
		bevelThickness: 1,
		bevelSize: 1,
		bevelSegments: 1
	};

	var geometry = new THREE.ExtrudeBufferGeometry( shape, extrudeSettings );
	var material = new THREE.MeshLambertMaterial({color: 0x00aa00});
	var mesh = new THREE.Mesh( geometry, material ) ;

	mesh.castShadow = true;
	mesh.receiveShadow  = true;



	return mesh;
}

function createVase(){
	var points = [];
	for (var i = 0; i < 8; i++) {
		x = Math.sin(2 * Math.PI * 7 * i / 64) * 4 + 12;
		y = 6 * i - 40;
		points.push(new THREE.Vector2(x, y));
	}
	var geometry = new THREE.LatheGeometry(points);
	var material = new THREE.MeshLambertMaterial({color: 0x0000aa});
	var vase = new THREE.Mesh( geometry, material );
	vase.castShadow = true;
	return vase;
}

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, WIDTH / HEIGHT, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer();
renderer.shadowMap.enabled = true;
renderer.setClearColor(0xffffff,1);
renderer.setSize( WIDTH, HEIGHT);

camera.position.x = 256;
camera.position.z = 64;
camera.position.y = 256;
camera.lookAt(new THREE.Vector3(0, 0, 0));


var light = new THREE.AmbientLight(0xaaaaaa);
scene.add(light);

var pointLight = new THREE.PointLight();
pointLight.position.set(0, 200, 0);
scene.add( pointLight );

var spotLight = new THREE.SpotLight(0xffffff);
spotLight.position.set(0, 200, 0);
spotLight.castShadow = true;
scene.add( spotLight );

var controls = new THREE.OrbitControls(camera, renderer.domElement);

var vaseBase = (function(){
	var material = new THREE.MeshLambertMaterial({color: 0xff0000 });
	var mesh = new THREE.Mesh(new THREE.CubeGeometry(32, 32, 32), material);
	mesh.castShadow = true;
	return mesh;
})();

vaseBase.position.x = -128;
vaseBase.position.y = 16;
scene.add(vaseBase);

var robot = new Robot3P();
var oven = createOven();
var vase = createVase();

scene.add(robot.base);
spotLight.target = robot.base;

oven.position.x = -16;
oven.position.z = 64;
scene.add(oven);

vase.position.x = -128;
vase.position.y = 32 + 40;
scene.add(vase);


var plane = new THREE.Mesh(new THREE.PlaneGeometry(320, 320),new THREE.MeshLambertMaterial({color: 0x555555}));
plane.receiveShadow  = true;
plane.rotation.x=-0.5*Math.PI;
scene.add(plane);

var t = 0;

var pos1 = {a: 0, b: 0, c: 0};
var pos2 = {a: 0.55, b: -0.25, c: -0.25};
var pos3 = {a: 1.2, b: 0.25, c: -0.65};
var pos4 = {a: 1.1, b: -0.15, c: -0.28};

function interpolate(from, to, startTick, endTick, currentTick) {
	var elapsed = currentTick - startTick;
	var total = endTick - startTick;
	var t = elapsed / total;
	return {
		a: Math.PI * (from.a * (1 - t) + to.a * t),
		b: Math.PI * (from.b * (1 - t) + to.b * t),
		c: Math.PI * (from.c * (1 - t) + to.c * t)
	};
}

function animate() {
	if (t < 64) {
		robot.set(interpolate(pos1, pos2, 0, 63, t));
	}
	if (t == 64) {
		scene.remove(vase);
		robot.attach(vase);
	}
	if (t >= 64 && t < 128) {
		robot.set(interpolate(pos2, pos3, 64, 127, t));
	}
	if (t >= 128 && t < 192) {
		robot.set(interpolate(pos3, pos4, 128, 191, t));
	}
	if (t == 192) {
		robot.remove();
		scene.add(vase);
		vase.position.set(16, 64 + 40, 96);
	}
	if (t >= 192 && t < 256) {
		robot.set(interpolate(pos4, pos3, 192, 255, t));
	}
	if (t >= 256 && t < 320) {
		robot.set(interpolate(pos3, pos1, 256, 320, t));
	}
	if (t >= 320) {
		t = 0;	
		vase.position.x = -128;
		vase.position.y = 32 + 40;
		vase.position.z = 0;
	}
	t++;
	requestAnimationFrame( animate );
	renderer.render( scene, camera );
};

document.body.appendChild( renderer.domElement );
animate();


