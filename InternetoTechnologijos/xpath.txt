1. /descendant::folder[@name="Arvydas"]

 /descendant::folder[@name="Arvydas"]/ancestor::*
 /descendant::folder[@name="Arvydas"]/descendant::*
 /descendant::folder[@name="Arvydas"]/following-sibling::*
 /descendant::folder[@name="Arvydas"]/preceding-sibling::*
 /descendant::folder[@name="Arvydas"]/following::*
 /descendant::folder[@name="Arvydas"]/preceding::*
 /descendant::folder[@name="Arvydas"]/attribute::*

2. Rasti visus failus su tokiu pat mimetype kaip ir background.png failas
 /descendant::file[mime-type/@type = /descendant::file[@name="background.png"]/mime-type/@type]/@name

3. Vidutinis failo dydis
 sum(//file/size) div count(//file)

4. 
//file < 4
"string" + true()
false() = 3.3

5. Visi folderiai su failais dydziu > 10MB 
/descendant::file/size[text() > 10]/ancestor::folder[1]/@name

/descendant::file {<file name="imageViewer-6.3.0"/>, <file name="imageViewer"/>, <file name="tree.ico"/>, <file name="background.png"/>, <file name="001_photo.png"/>}

/size[text() > 10] {<size measure="MB">123.456</size>,<size measure="MB">45.4</size>,<size measure="MB">11</size>}

/ancestor::folder[1] {<folder name="bin"/>, <folder name="Arvydas"/>, <folder name="photos">}

/@name {"bin", "Arvydas", "photos"}


6.
aibe ir skaicius
//file = 4
aibe ir eilute
//file = "file"
aibe ir logine reiksme
//file = true()
aibe ir aibe
//file = //folder

7. //file > //folder
