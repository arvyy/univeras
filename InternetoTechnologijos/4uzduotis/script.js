filesCount = 0;

function pwCbHandler(cb) {
	var pw = $('#passwordFieldLabel');
	if (cb.checked) {
		pw.show();
		$(pw).find('input').attr('required', true);
	} else {
		$(pw).find('input').attr('required', false);
		pw.hide();
	}
}

function formSubmitHandler(form) {
	event.preventDefault();
	var d = $(form).find('input[name="until"]').val();
	var day = parseInt(d.substring(0, 2));
	var month = parseInt(d.substring(3, 5)) - 1;
	var year = parseInt(d.substring(6));
	if (month > 11) {
		alert('Metuose tėra 12 mėnesių');
		return;
	}
	var date = new Date(year, month, day);
	if (date.getTime() < new Date().getTime()) {
		alert('Data negali būt anksčiau nei šiandiena');
		return;
	}
	if (date.getDate() != day) {
		console.log(date.getDate());
		alert('Bloga data');
		return;
	}
	var data = {};
	var dataArray = $(form).serializeArray();
	for (var i = 0; i < dataArray.length; i++) {
		data[dataArray[i].name] = dataArray[i].value;
	}
	$.ajax({
		url : 'https://api.myjson.com/bins/',
		method : 'POST',
		data : JSON.stringify(data),
		contentType : 'application/json',
		dataType : 'json',
		success : function(resp) {
			$.ajax({
				url : resp.uri,
				method : 'GET',
				contentType : 'application/json',
				success : function(getResp) {
					onReceiveNewItem(getResp);
				}
			});
		}
	});
}

function onReceiveNewItem(item) {
	filesCount++;
	if ($('#tr-' + item.name).length > 0) {
		$('#tr-' + item.name).remove();
		filesCount--;
	}
	var str = 'Failai';
	if (filesCount > 0) str += '(' + filesCount + ')';
	$('#filesHeader').text(str);
	$('#files tbody').append(
		'<tr class="matching-item" id="tr-'+item.name+'">'+
			'<td>' + item.name + '</td>'+
			'<td><button onclick="onRemoveItem(this)">Trinti</button></td>'+
		'</tr>'
	);
}

function onRemoveItem(item) {
	filesCount--;
	$(item).parents('tr').remove();
	var str = 'Failai';
	if (filesCount > 0) str += '(' + filesCount + ')';
	$('#filesHeader').text(str);
}

function onNameInput(name) {
	$('.matching-item').removeClass('matching-item');
	$('#tr-' + name.value).addClass('matching-item');
}

$(document).ready(function() {
	$('#passwordFieldLabel').hide();
	$('input[name="until"]').attr('pattern', '[0-9]{2}-[0-9]{2}-[0-9]{4}');
	$('input[name="name"]').attr('pattern', '[a-zA-Z_]+');
});
