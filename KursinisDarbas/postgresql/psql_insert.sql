insert into Virsunes (id, tipas, detales)
values
	(1, 'Darbuotojas', 1),
	(2, 'Darbuotojas', 2),
	(3, 'Darbuotojas', 3),
	(4, 'Darbuotojas', 4),
	(5, 'Darbuotojas', 5),

	(6, 'Dokumentas', 1),
	(7, 'Dokumentas', 2),
	(8, 'Dokumentas', 3),
	(9, 'Dokumentas', 4),
	(10, 'Dokumentas', 5),

	(11, 'Tipas', 1),
	(12, 'Tipas', 2),
	(13, 'Tipas', 3),
	(14, 'Tipas', 4),

	(15, 'Role', 1),
	(16, 'Role', 2),
	(17, 'Role', 3);

insert into Briaunos (id, tipas, virsune_is, virsune_i)
values 
	(1, 'Vadovas', 1, 2),
	(2, 'Vadovas', 1, 3),
	(3, 'TuriRole', 1, 15),

	(4, 'Vadovas', 2, 4),
	(5, 'Vadovas', 2, 5),
	(6, 'TuriRole', 2, 17),

	(7, 'Vadovas', 3, 5),
	(8, 'TuriRole', 3, 17),

	(9, 'TuriRole', 4, 16),
	(10, 'Mato', 4, 12),

	(11, 'TuriRole', 5, 12),
	(12, 'Mato', 5, 8),

	(13, 'TuriTipa', 6, 11),
	(14, 'TuriTipa', 7, 11),
	(15, 'TuriTipa', 7, 12),
	(16, 'TuriTipa', 8, 13),
	(17, 'TuriTipa', 9, 14),
	(18, 'TuriTipa', 10, 12),
	(19, 'TuriTipa', 10, 13),

	(20, 'Mato', 15, 11),
	(21, 'Mato', 15, 12),
	(22, 'Mato', 15, 13),
	(23, 'Mato', 15, 14),
	(24, 'Mato', 16, 11),
	(25, 'Mato', 17, 11),
	(26, 'Mato', 17, 12),
	(27, 'Mato', 17, 13);

insert into Darbuotojas (id, vardas)
values
	(1, 'D1'),
	(2, 'D2'),
	(3, 'D3'),
	(4, 'D4'),
	(5, 'D5');

insert into Dokumentas (id, pavadinimas)
values
	(1, 'DOC1'),
	(2, 'DOC2'),
	(3, 'DOC3'),
	(4, 'DOC4'),
	(5, 'DOC5');

insert into Tipas (id, pavadinimas)
values
	(1, 'T1'),
	(2, 'T2'),
	(3, 'T3'),
	(4, 'T4');

insert into Role (id, pavadinimas)
values
	(1, 'R1'),
	(2, 'R2'),
	(3, 'R3');
