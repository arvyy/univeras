\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {lithuanian}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Įvadas}{2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.}Grafų duomenų bazės}{3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1.}Duomenų struktūra}{3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2.}Primytivių užklausų vykdymas}{5}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.}Grafų duomenų užtikrinimas RDBVS}{7}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1.}Modeliuojami duomenys}{7}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2.}Įgyvendinimas GDBVS (Neo4J)}{8}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.}Įgyvendinimas RDBVS (Postgresql)}{8}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.1.}Lankstus įgyvendinimas}{9}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.2.}Struktūrizuotas įgyvendinimas}{10}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.3.}Sintaksinis palengvinimas}{12}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{Rezultatai ir išvados}{15}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{Literatūra}{16}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{Santrumpos}{17}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\MakeUppercase {Priedai}}{17}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{1 priedas.\nobreakspace {}MATCHING transliatoriaus implementacija Racket kalba}{18}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{2 priedas.\nobreakspace {}Neo4j duomenų sukūrimo sakinys}{23}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{3 priedas.\nobreakspace {}SQL lankstaus formato įgyvendinimo duomenų sukūrimo sakiniai}{25}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{4 priedas.\nobreakspace {}SQL nelankstaus įgyvendinimo duomenų sukūrimo sakiniai}{28}% 
